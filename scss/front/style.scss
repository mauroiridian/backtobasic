@import "../admin/variables";
@import "../admin/mixins";


// Global Styles

@include font-face-new('caviardreams', 'caviardreams-webfont', 'serif');
@include font-face-new('caviardreams_bold', 'caviardreams_bold-webfont', 'serif');
@include font-face-new('caviardreams_bolditalic', 'caviardreams_bolditalic-webfont', 'serif');
@include font-face-new('caviardreams_italic', 'caviardreams_italic-webfont', 'serif');
@include font-face-new('helbablackdb_normal', 'helbablackdb_normal_0-webfont', 'serif');
@include font-face-new('helbalightdb', 'helbalightdb_0-webfont', 'serif');



@font-face {
  font-family:'Swiss721BT-Black';
  src: url('../fonts/Swiss721BT-Black_gdi.eot');
  src: url('../fonts/Swiss721BT-Black_gdi.eot?#iefix') format('embedded-opentype'),
  url('../fonts/Swiss721BT-Black_gdi.woff') format('woff'),
  url('../fonts/Swiss721BT-Black_gdi.ttf') format('truetype'),
  url('../fonts/Swiss721BT-Black_gdi.svg#Swiss721BT-Black') format('svg');
  font-weight: 400;
  font-style: normal;
  font-stretch: normal;
  unicode-range: U+0020-25CA;
}


/* test modulos  "borrable" */

.opacity_Back_50 {
  @include opacities($color1_rgb, $op_half);
}

/* end test modulos */


/* general classes */


h1, h2, h3, h4, h5, h6, p {
  margin: 0 5%;
  width: 90%;
  color: $black;
}

h1 {
  color: $white;
  text-transform: uppercase;
}

h2 {
  font-family:'Times New Roman';
}

h3 {
  font-family: 'Futura Bk BT';
}

p {
  letter-spacing: 7px;
}

body {
  font-size: 16px;
  font-family: 'Times New Roman';
}

.wrapper {
  width: $normal;
  margin: 0 auto;
  @media (max-width: 1024px) {
    float: left;
    width: $full;
  }
}


hr {
  border-top: 1px solid $black;
}

.float_l {
  float: left;
}

.float_r {
  float: right;
}

.button {
  display: inline-block;
  padding: 1% 3%;
  cursor: pointer;
  color: $white;
  background-color: $black;
  font: 1.200em 'Times New Roman';
}

.button2 {
  color: $black;
  background-color: transparent;
  background-image: url("../images/border_bottom.png");
  background-position: bottom;
  background-size: 100% 50%;
  background-repeat: no-repeat;
}

.button:hover {
  @include opacities($black_rgb, $op_half);
  color: $white;
}

.close {
  position: absolute;
  top: 5%;
  right: 5%;
  z-index: 10;
  font: 1.500em 'caviardreams_bold';
  color: $black;
}

.contenedor {
  width: $full;
  display: inline-block;
  position: relative;
  .rotador {
    width: $full;
    height: $full;
    display: block;
    z-index: 1;
    overflow: hidden;
  }
  .item {
    width: $full;
    height: $full;
    display: block;
    background-size: cover;
    background-position: center;
    background-repeat: no-repeat;

    .hover {
      width: $full;
      height: $full;
      position: relative;
      @include opacities($white, $op_80);
      opacity: 0;
    }

  }
  .col_50 {
    width: $half;
    display: block;
    height: $full;
    position: relative;
  }

}

.cont_center_v {
  width: $full;
  position: absolute;
  text-align: center;
  top: 50%;
  transform: translateY(-50%);
  z-index: 10;
}

.arrow{
  width: 3%;
  position: absolute;
  z-index:10;
  cursor: pointer;
  top: 50%;
  transform: translateY(-50%);
  img {
    width: $full;
    display: inline-block;
  }
  &.left{
    left: 2%;
  }
  &.right {
    right: 2%;
  }
}

.banner {
  height: 100vh;
  overflow: hidden;
  @extend .contenedor;
  background-size: cover;
  background-position: center;
  background-repeat: no-repeat;
}

.banner_v2 {
  height: 80vh;


  &.d_1 {
    .col_50:first-child{
      float: left;

      .cont_center_v {
        width: 88%;
        right: 0;
      }
    }
    .col_50 {
      float: right;
    }
  }
  &.d_2 {
    .col_50:first-child{
      float: right;

      .cont_center_v {
        width: 88%;
        left: 0;
      }
    }
    .col_50 {
      float: left;
    }
  }
}

.banner_v3 {
  height: 36vh;
}

.title_g {
  width: 30%;
  padding: 0% 2%;
  margin: 5%;
  @include opacities($white_rgb, $op_half);
  h2, h3 {
    text-transform: uppercase;
  }
  h2 {
    font-size: 72px;
  }
  h3 {
    font: 120px 'Futura Bk BT';
  }
  &.float_r {
    h2, h3 {
      text-align: right;
    }
  }
  &.float_l {
    h2, h3 {
      text-align: left;
    }
  }

  &.title_g2 {
    width: 90%;
    text-align: center;
  }

}

.a_txt {
  @extend .contenedor;
  text-align: center;
  padding: 50px 0;
  &.justify {
    text-align: justify;
    -moz-text-align-last: center; /* Code for Firefox */
    text-align-last: center;
  }
  p {
    color: $black;
    letter-spacing: 7px;
  }
  h2 {
    text-transform: uppercase;
    font-weight: bold;
    margin: 1% 5%;
  }
  h3 {
    text-transform: uppercase;
    font-weight: bold;
    margin: 0 5%;
    font-family: 'Times New Roman';
  }
}

/* end general classes */

.menu {
  text-align: center;
  margin-bottom: 2%;
  .logo {
    width: 10%;
    float: left;
    margin: 2%;
    img {
      @extend .contenedor;
    }
  }
  ul {
    width: 85%;
    float: right;
    display: table-cell;
    list-style: none;
    text-align: center;
    margin: 8% 0 0 0;
    text-align: right;
    li {
      width: 15%;
      display: inline-block;
      position: relative;
      a {
        @extend .contenedor;
        display: inline-block;
        vertical-align: bottom;
        padding: 0;
        text-align: center;
        span {
          @extend .contenedor;
          color: $black;
          font: 1.000em 'Times New Roman';
          text-transform: uppercase;
        }
        img {
          width: 80%;
          display: inline-block;
        }
      }
      ul {
        width: 100%;
        position: absolute;
        top: 100%;
        left: 0;
        border: 1px solid $black;
        background-color: $white;
        z-index: 100;
        padding: 0;
        margin: 0;
        display: none;
        li {
          width: 100%;
          padding: 2%;
          margin: 2% 0 0 0;
          a {
            background-position: center;
            background-size: cover;
            span {
              color: $gray-dark;
              padding: 10px 2%;
              @include opacities($white_rgb, $op_80);
            }
          }
        }
      }
    }
    li:hover{
      ul {
        display: block;
      }
    }
  }
}

.access {
  width: 30%;
  float: right;
  margin-right: 2%;
  margin-top: 2%;
  text-align: right;
  p {
    width: 100%;
    display: inline-block;
    font-size: 12px;
    color: $black;
    margin: 0 0;
    a {
      display: inline-block;
      font-size: inherit;
      color: $black;
    }
  }

  .searcher {
    width: 60%;
    height: 32px;
    display: inline-block;
    vertical-align: middle;
    border: 1px solid $gray-darker;
    input{
      border: 0;
    }
    input[type="text"]{
      width: 77%;
      height: 30px;
      float: left;
    }
    input[type="submit"]{
      width: 23%;
      height: 30px;
      float: right;
      color: transparent;
      background-image: url("../images/lupita.png");
      background-size: cover;
      background-position: center;
      background-color: $gray-darker;
      background-repeat: no-repeat;
    }
  }

  .social {
    width: 10%;
    display: inline-block;
    vertical-align: middle;
    margin: 0 5px;
    position: relative;
     img{
        width: $full;
        display: inline-block;
     }
  }
  .carrito {
    width: 7%;
    position: absolute;
    top: 2%;
    right: 2%;
    z-index: 1000000;
    text-align: center;
    a {
      width: 60%;
      display: inline-block;
      vertical-align: middle;
      margin: 5%;
      /*background-image: url("../images/carrito.png");
      background-size: cover;
      background-repeat: no-repeat;
      background-position: center;
      background-color: $gray-light;*/
      img {
        width: 100%;
        float: left;
        -webkit-border-radius: 50%;
        -moz-border-radius: 50%;
        border-radius: 50%;
        background-color: $gray-light;
        border: 2px solid $gray-light ;
      }
    }
    p {
      width: 30px;
      position: absolute;
      top: 0;
      right: 0;
      z-index:1;
      padding: 2px;
      background-color: $gray-light;
      color: $white;
      -webkit-border-radius: 10%;
      -moz-border-radius: 10%;
      border-radius: 10%;
      font-size: 14px;
      text-align: center;
      letter-spacing: 0;
    }
  }


}

.category {
  margin: 3% 0;
  text-align: center;
  .sec {
    width: 30%;
    height: 100vh;
    display: inline-block;
    margin: 0 1%;
    position: relative;

    .img1, .img2, a{
      width: $full;
      height: $full;
      position: absolute;
      top: 0;
      left: 0;
      background-size: cover;
      background-repeat: no-repeat;
      background-position: center;
    }

    .title {
      width: $full;
      position: absolute;
      top: 0;
      left: 0;
      z-index:2;
    }

    .t_g {
      opacity: 0;
    }
  }

  .sec:nth-child(even) .item .title {
    bottom: 0;
    top: inherit;
  }
}



.go_info {
  @extend .contenedor;
  width: 90%;
  margin: 2% 5%;
  /*height: 55vh;*/
  text-align: center;
  padding-bottom: 5%;

  .title {
    width: 90%;
    height: 30vh;
    display:  inline-block;
    background-size: cover;
    background-position: center;
    position: relative;
    z-index: 1;
    img {
      width: $half;
      display: inline-block;
    }
  }

  p {
    width: $half + $tq ;
    display: inline-block;
    position: relative;
    z-index:1;
    font-style: italic;
    font-size: 16px;
    margin-top: 4%;
  }

  .back_img {
    width: $full;
    height: 45vh;
    height: 80%;
    position: absolute;
    bottom: 2%;
    left: 0;
    background-size: 100% auto;
    background-position: center;
    margin-top: -25vh;
    .b_white {
      width: 96%;
      height: 90%;
      position: absolute;
      top: 5%;
      left: 2%;
      background-color: $white;
    }
  }

  .button {
    width: 20%;
    position: absolute;
    z-index: 2;
    font-size: 1.800em;
    bottom: 0;
    left: 40%;
    @include opacities($black_rgb, $op_80);
  }

}

.our_process {
  @extend .banner;
  @extend .banner_v2;
  @extend .d_1;
  height: 500px;
  margin: 4% 0;
  overflow: inherit;

  @media (max-width: 1520px) {
      height: 400px;
  }
  @media (max-width: 1200px) {
    height: 300px;
  }
  .col_50 {
    float: left;
    .item {
      position: relative;

      .cont_center_v {
        width: $full;
        left: 0;
      }

      &.video {
        overflow: hidden;
        video {
          width: 120%;
          margin-left: -10%;
        }
      }

      .col_50 {
        display: block;
        height: 100%;
      }
    }
  }



  .vid {
    width: $half;
    overflow: hidden;
  }

  .video-js .vjs-big-play-button {
    width: 200px;
    height: 200px;
    -webkit-border-radius: 50%;
    -moz-border-radius: 50%;
    border-radius: 50%;
    top:0;
    left: 0;
    right: 0;
    bottom: 0;
    margin: auto;
    border: 0;
  }

  .dual_title {
    @extend .item;
    h2, h3 {
      text-align: center;
    }

    .button {
      @extend .button2;
      float: right;
    }
  }

  .a_txt {
    padding: 2% 0;
  }
}

.follow_i {
  @extend .a_txt;
  h2 {
    letter-spacing: 7px;
    a {
      color: $black;
    }
  }
}

.footer {
  @extend .contenedor;

  .go_carrito {
    a {
      width: 25%;
      display: inline-block;
      img {
        @extend .contenedor;
      }
    }
  }

  .news {
    @extend .a_txt;
    h4 {
      margin: 2% 5%;
      text-transform: uppercase;
    }

    form {
      width: $half;
      display: inline-block;

      input[type="text"], input[type="email"]{
        @extend .contenedor;
        border: 1px solid $gray-light;
        padding: 1% 2%;
        margin-top: 2%;
      }

      input[type="submit"]{
        border: 0;
        padding: 1% 2%;
        text-transform: uppercase;
        margin: 2% 0;
      }
    }
  }

  .social {
    text-align: center;
    a {
      width: 20%;
      display: inline-block;
      margin: 0 2%;
      img {
        @extend .contenedor;
      }
    }

    a:nth-child(4){
      cursor: default;
    }
  }



  .sub_footer {
    width: 96%;
    text-align: center;
    padding: 1% 0;
    margin: 1% 2%;
    border-top: 1px solid $gray-light;

    .text {
      width: 80%;
      display: inline-block;
      a {
        width: auto;
        color: $gray-light;
      }
    }

    p {
      width: 100%;
      display: inline-block;
      font: 14px 'Arial';
      color: $gray-light;
      letter-spacing: inherit;
      vertical-align: middle;
      margin: 0 1%;
    }
    a {
      width: 6%;
      display: inline-block;
      img {
        width: $full;
      }
    }
    a.pay {
      width: 5%;
      display: inline-block;
      margin: 5px 1%;
    }
  }
}


.legal {
  h1, h2, h3, h4, h5, h6 {
    text-align: center;
  }

  h1 {
    color: $black;
    margin: 2% 0;
  }

  p {
    width: $full;
    display: inline-block;
    text-align: justify;
    color: $gray-darker;
    margin: 0;
  }

  ul{
    li {
      letter-spacing: 7px;
      margin: 2% 0;
    }
  }
}

.b_home {
  .go_shop {
    width: $half + $tq;
    display: inline-block;
    img {
      @extend .contenedor;
    }
  }
}


.b_we_are {
  @extend .banner;
  @extend .banner_v2;

  .title {
    width: $half;
    display: inline-block;
    @include opacities($white_rgb, $op_half);
    padding: 1% 2%;
    h2, h3, h4 {
      text-transform: uppercase;
      letter-spacing: 4px;
      color: $black;
    }

    h3 {
      font-family: 'Futura Bk BT';
    }

    h2:first-child {
      text-align: right;
    }

    h2, h4 {
      text-align: right;
    }

    h3 {
      text-align: left;
      letter-spacing: 10px;
    }
  }
}

.title_we_b_2 {
  padding-bottom: 0;
  h2, h3 {
    margin: 0 5%;
  }

  h2 {
    margin-top: 5%;
  }
}

.banner_blog {
  @extend .banner;
  @extend .banner_v2;
}

.blog {
  @extend .contenedor;
  margin: 1% 0;
  .wrapper {
    width: $half + $tq;
  }

  .filter {
    @extend .contenedor;
    border: 1px solid $gray-light;
    padding: 2% 2%;
    form {
      @extend .contenedor;
      border-bottom: 1px solid $gray-dark;
      button {
        border: 0;
        background-color: transparent;
      }
      input {
        width: 80%;
        border: 0;
        display: inline-block;
      }
    }

    h2 {
      width: auto;
      text-transform: uppercase;
      border-bottom: 1px solid $gray-dark;
      margin: 3% 0;
      font-size: 18px;
      font-weight: bold;
    }

    ul {
      list-style: none;
      display: inline-block;
      margin: 2% 0;
      li {
        @extend .contenedor;
        a {
          display: inline-block;
          color: $gray-dark;
        }
      }
    }
  }

  .news {
    background-color: $black;
    text-align: center;
    padding: 2% 0;
    margin-top: 2%;
    h4 {
      color: $white;
      font-size: 15px;
      margin: 2% 5%;
    }

    form {
      input[type="text"], input[type="email"]{
        width: 90%;
        background-color: $black;
        border: 1px solid $white;
        margin: 4% 0 ;
      }
      input[type="submit"]{
        background-color: $white;
        border: 0;
        color: $black;
        text-transform: uppercase;
        margin: 4% 0;
      }
    }
  }

  .outstanding_blog {
    width: 80%;
    height: 90vh;
    display: inline-block;
    margin: 0 10%;
    .img, .txt {
      width: $full;
      height: 60%;
      float: left;
      display: block;
    }
    .img {
      background-size: cover;
      background-position: center;
    }
    .txt{
      height: 39%;
      position: relative;
      margin-top: 1%;
      border: 1px solid $gray-darker;
      text-align: center;

      a {
        font: 18px 'Times New Roman';
        font-style: italic;
        color: $gray-darker;
        display: inline-block;
        margin-top: 10%;
        text-transform: uppercase;
      }

      h2 {
        font-size: 30px;
        font-weight: bold;
        letter-spacing: 10px;
        text-transform: uppercase;
      }

      .date {
        width: $full;
        position: absolute;
        bottom: 98%;
        left: 0;
        h3 {
          width: auto;
          display: inline-block;
          font: 18px 'Futura Bk BT';
          padding: 1% 2%;
          background-color: $black;
          color: $white;
        }
      }
    }
  }

  .rot_blog {
    @extend .contenedor;
    text-align: center;
    margin: 4% 0;
    position: relative;
    h2 {
      width: auto;
      display: inline-block;
      text-align: center;
      margin: 2% 5%;
      font-weight: bold;
      font-size: 30px;
      border-bottom: 1px solid $gray-darker;
    }
    .item {
      width: 15%;
      height: 30vh;
      display: inline-block;
      margin: 0 0.5%;
      .img, .txt {
        width: $full;
        height: 60%;
        float: left;
        display: block;
      }
      .img {
        background-size: cover;
        background-position: center;
      }
      .txt {
        position: relative;
        height: 39%;
        margin-top: 1%;
        border: 1px solid $gray-darker;

          h3 {
            font: 18px 'Times New Roman';
            font-weight: bold;
          }
          a {
            display: inline-block;
            font: 12px 'Times New Roman';
            font-style: italic;
            margin-top: 3%;
            color: $black;
          }
        }
    }

    .arrow {
      width: 2%;
      position: absolute;
      top: 64%;
      img {
        @extend .contenedor;
      }
      &.left{
        left: 0;
      }
      &.right {
        right: 0;
      }
    }
  }
}

.the_blog {
  width: 80%;
  display: inline-block;
  margin: 2% 10%;
  text-align: center;

  .txt, .item {
    width: 46%;
    display: inline-block;
    vertical-align: top;
    margin: 2% 1%;
  }

  .txt {

    margin-top: 0;

    text-align: left;

    h2, h3, p {
      @extend .contenedor;
      margin: 2% 0;
    }

    h2 {
      font-size: 40px;
      font-weight: bold;
      margin-top: 0;
    }

    h3 {
      font-size: 24px;
      font-style: italic;

    }

    p{
      /*font: 21px 'Futura Bk BT';*/
      display: inline-block;
    }

    .powered_by {
      text-align: right;
      font-style: italic;
      a {
        color: $gray-dark;
      }
    }
  }
  .item {
    height: 90vh;
    position: relative;
    background-position: center;
    background-size: cover;
  }
  .item:nth-child(2){
    height: 90vh;
    margin-top: 0;
  }
  .item:nth-child(3) {
    height: 44vh;
  }
  .item:nth-child(4) {
    height: 44vh;
  }
  .item:nth-child(5) {
    height: 90vh;
  }
  .item:nth-child(6) {
     height: 90vh;
   }
  .item:nth-child(7) {
    height: 44vh;
  }
  .item:nth-child(8) {
    height: 44vh;
  }
  .item:nth-child(9) {
    height: 90vh;
  }
  .item:nth-child(10) {
    height: 90vh;
  }
  .hover {
    width: $full;
    height: auto;
    display: inline-block;
    @include opacities($gray_rgb, $op_half);
    padding: 2% 0;
    opacity: 0;
    h2 {
      font: 30px 'Futura Bk BT';
      color: $white;
      margin: 2% 0;
      display: inline-block;
    }
    a {
      width: 7%;
      display: inline-block;
      margin: 0 2%;
      img {
        @extend .contenedor;
      }
    }
  }

  .social_feed {
    width: 20%;
    float: right;
    margin-left: 80%;

    h2 {
      width: 39%;
      display: inline-block;
      vertical-align: middle;
    }

    a {
      width: 15%;
      display: inline-block;
      img {
        @extend .contenedor;
      }
    }

    .control_blog {
      border-bottom: 1px solid $black;
      text-align: right;
      margin: 2% 0;
      a {
        width: auto;
        color: $black;
        text-transform: uppercase;
        cursor: pointer;
      }
    }

  }

}

.home_store {
  text-align: center;
  .link {
    width: 45%;
    height: 90vh;
    display: inline-block;
    position: relative;
    background-position: center;
    background-size: cover;
    .hover {
      width: $full;
      height: $full;
      position: absolute;
      top:0;
      left: 0;
      @include opacities($white_rgb, $op_half);
      opacity: 0;
    }
    .button {
      width: auto;
      border: 1px solid $white;
      background-color: transparent;
      color: $white;
      font-size: 72px;
    }

  }

  .link:hover{
    .button {
      border: 1px solid $gray-light;
      color: #767676;
    }
  }
}


.products {
  @extend .contenedor;
  .list {
    @extend .contenedor;
    height: 100vh;
    position: relative;
    text-align: center;
    .grid {
      width: 100%;
      display: inline-block;
      position: relative;
    }
    .item {
      width: 30%;
      height: 70vh;
      display: block;
      float: left;
      margin: 2% 1.5%;
      position: relative;

      .oos {
        width: 100%;
        position: absolute;
        top: 50%;
        left: 0;
        background-color: $black;
        padding: 2% 0;
        display: none;
        h3 {
          color: $white;
        }
      }
      &.agotado {
       .oos {
         display: block;
       }
      }

      .img {
        width: 100%;
        height: 90%;
        display: block;
        float: left;
        background-position: center;
        background-size: cover;
        &.img2{
          position: absolute;
          top: 0;
          left: 0;
        }
      }



      a {
        width: $full;
        height: $full;
        position: absolute;
        top: 0;
        left: 0;
      }

      .txt {
        width: 100%;
        float: left;
        display: block;
        height: 10%;
        position: relative;
      }

      /*
      &:nth-child(9n+1){
        height: 59vh;
      }
      &:nth-child(9n+2){
        height: 59vh;
      }
      &:nth-child(9n+3){
        height: 40vh;
      }
      &:nth-child(9n+4){
        height: 59vh;
      }
      &:nth-child(9n+5){
        height: 40vh;
      }
      &:nth-child(9n+6){
        height: 49vh;
      }
      &:nth-child(9n+7){
        height: 40vh;
      }
      &:nth-child(9n+8){
        height: 59vh;
      }
      &:nth-child(9n){
        height: 59vh;
      }
      */

      /*
      &:nth-child(9n+1){
        height: 59vh;
      }
      */
      /*
      &:nth-child(n+10){
        height: 59vh;
      }
      */
    }

    .item:hover {
      .img2 {
        opacity: 0;
      }
    }

    .grid-sizer,
    .grid-item { width: 30%; }
    .grid-item--width2 { width: 400px; }
  }

  .filter {
    @extend .contenedor;
    list-style:none;
    text-align: center;
    margin: 2% 0;
    li {
      width: 30%;
      display: inline-block;
      position: relative;
      a {
        width: 100%;
        display: inline-block;
        padding: 2% 0;
        border: 1px solid $gray-darker;
        color: $black;
        font-size: 24px;
        &.active{
          background: #000;
          color: #fff;
        }
      }
      ul {
        width: $full;
        position: absolute;
        top: 100%;
        left: 0;
        z-index: 10;
        padding: 0;
        margin: 0;
        background-color: $white;
        display: none;
        li {
          width: $full;
        }
      }
    }
    li:hover > ul {
      display: block;
    }
  }

  .search {
    width: $full;
    display: inline-block;
    text-align: right;
    input[type="submit"]{
      visibility: hidden;
    }
    input[type="search"]{
      border: 1px solid $gray-darker;
    }
  }
}

.search-form .form-group {
  float: right !important;
  transition: all 0.35s, border-radius 0s;
  width: 32px;
  height: 32px;
  background-color: #fff;
  box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
  border-radius: 25px;
  border: 1px solid #ccc;
}
.search-form .form-group input.form-control {
  padding-right: 20px;
  border: 0 none;
  background: transparent;
  box-shadow: none;
  display:block;
}
.search-form .form-group input.form-control::-webkit-input-placeholder {
  display: none;
}
.search-form .form-group input.form-control:-moz-placeholder {
  /* Firefox 18- */
  display: none;
}
.search-form .form-group input.form-control::-moz-placeholder {
  /* Firefox 19+ */
  display: none;
}
.search-form .form-group input.form-control:-ms-input-placeholder {
  display: none;
}
.search-form .form-group:hover,
.search-form .form-group.hover {
  width: 100%;
  border-radius: 4px 25px 25px 4px;
}
.search-form .form-group span.form-control-feedback {
  position: absolute;
  top: -1px;
  right: -2px;
  z-index: 2;
  display: block;
  width: 34px;
  height: 34px;
  line-height: 34px;
  text-align: center;
  color: #000;
  left: initial;
  font-size: 14px;
}


.the_produc {
  width: 80%;
  display: inline-block;
  text-align: center;
  margin: 0 10%;
  overflow: hidden;
  .txt, .gal{
    @extend .contenedor;
  }

  .txt {
    padding: 0 5%;
    h2, h3 {
      text-align: center;
      margin: 4% 5%;
    }

    h3 {
      font-family: 'Times New Roman';
    }

    p {
      @extend .contenedor;
      text-align: justify;
      letter-spacing: 2px;
      /*font-family: 'Futura Bk BT';*/
    }
    .desp {
      @extend .contenedor;
      .item {
        @extend .contenedor;
        display: none;
        margin: 2% 0;
        table {
          display: inline-block;
          text-align: center;
          tr {
            td:first-child {
              font-weight: bold;
            }
            td {
              padding: 0 2%;

            }
          }
        }
        .fotito {
          width: 40%;
          height: 10vh;
          display: inline-block;
          background-size: cover;
          background-position: center;
        }

      }

    }

    .size {
      @extend .contenedor;
      list-style:none;
      padding: 0;
      li {
        display: inline-block;
        a {
          color: $gray-darker;
          cursor: pointer;
          text-transform: uppercase;
          width: 40px;
          display: inline-block;
          border: 1px solid $gray-darker;
          text-align: center;
          font-size: 20px;
          padding: 10px 0;
          &.active{
            text-decoration: underline;
            background-color: $black;
            color: $white;
          }
        }
      }
    }
    .button {
      background-color: $white;
      border: 1px solid $gray-darker;
      color: $gray-darker;
      margin: 4% 20%;
      text-transform: uppercase;
      font-size: 20px;
      span:first-child {
        font-size: 14px;
        width: 100%;
      }
      span {
        display: inline-block;
      }
    }
    .button:hover {
      background-color: $black;
      color: $white;
    }

    .button2 {
      width: 60%;
      background-color: $black;
      color: $white;
    }

    .button3 {
      span{
        max-width: 70%;
        vertical-align: middle;
      }
      i {
        vertical-align: middle;
      }
    }

    .solcial_product {
      @extend .contenedor;

      a {
        width: 15%;
        display: inline-block;
        vertical-align: middle;
        margin: 2%;
        img {
          @extend .contenedor;
        }
      }
    }

  }

  .gal {
    height: 120vh;
    .gallery-top{
      width: $full;
      height: 80%;
      display: block;
      float: left;
      .item {
        img {
          width: $full;
          display: inline-block;
          position: relative;
          z-index: 0;
        }
      }
    }
    .gallery-thumbs {
      width: $full;
      height: 20%;
      display: block;
      float: left;
      margin-top: 4%;
    }
  }

  .add_wishlist {
    i {
      color: $white;
    }
  }

}

.zoomContainer {
  z-index: 10;
}

.zoomLens {
  z-index: 100000000000000000000000000;
}

.contacto_txt {
  p {
    margin: 4% 5%;
  }
}

.direccion2 {
  h4 {
    margin: 2% 0;
  }

  form {
    label {
      width: 40%;
      margin: 2% 0;
    }
    input, select, textarea {
      width: $full;
    }
  }

  .list-group {
    .list-group-item {
      span {
        width: 60%;
        display: inline-block;
        vertical-align: middle;
      }
    }

  }
}

.contacto {
  @extend .banner;
  @extend .banner_v2;

  .my-video-dimensions {
    width: 100%;
    height: 100%;
  }

  .b_contact_m {
   display: none;
  }

  #video-viewport {
    position: absolute;
    top: 0;
    overflow: hidden;
    z-index: -1; /* for accessing the video by click */
  }

  #debug {
    position: absolute;
    top: 0;
    z-index: 100;
    color: #fff;
    font-size: 12pt;
  }


  .item {
    .col_50 {
      position: absolute;
      top: 0;
      right: 0;
      z-index: 100;
      @include opacities($white_rgb, $op_80);
      h4 {
        text-align: left;
        color: $black;
        font-size: 30px;
      }
    }
    form {
      width: 96%;
      display: inline-block;
      padding: 0 2%;
      input[type='text'], select, textarea, input[type='email']{
        width: 100%;
        display: inline-block;
        margin: 0.5% 0;
        padding: 1% 2%;
        border: 1px solid $gray-darker;
      }
      textarea {
        height: 200px;
        @media (max-height: 768px) {
          height: 130px;
        }
        @media (max-width: 1280px) {
          height: 130px;
        }
      }

      input[type="submit"]{
        float: right;
        border: 0;
      }
    }
  }

}

.log {
  form {
    input[type="text"], input[type="email"], input[type="password"], input[type="date"]{
      width: 100%;
      display: inline-block;
      margin: 2% 0;
      padding: 1% 2%;
      border: 1px solid $gray-darker;
    }

    input[type="checkbox"]{
      display: inline-block;
    }

    input[type="submit"]{
      float: right;
      margin-top: 15px;
    }

    label {
      width: auto;
      display: inline-block;
      color: $gray-light;
    }
    #fos_user_registration_form_fecha {
      width: 70%;
      display: inline-block;
      select {
        width: 28%;
        display: inline-block;
        margin: 0 2%;
        border: 1px solid $gray-darker;
      }
    }


    .contenedor {
      p {
        width: auto;
        display: inline-block;
        vertical-align: middle;
      }
    }
  }
}

.forgot_pass {
  form {
    input[type="text"], input[type="email"], input[type="password"], input[type="date"]{
      width: 100%;
      display: inline-block;
      margin: 2% 0;
      padding: 1% 2%;
      border: 1px solid $gray-darker;
    }

    input[type="checkbox"]{
      display: inline-block;
    }

    input[type="submit"]{
      float: right;
      margin-top: 15px;
      border: 0;
    }

    .contenedor {
      p {
        width: auto;
        display: inline-block;
        vertical-align: middle;
      }
    }
  }
}

.carrito {
  table {
    border: 0;
    thead {
      tr {
        th {
          border: 0;
        }
      }
    }
    tbody {
      tr{
        td{
          border: 0;
        }
      }
    }
    img, h4, p {
      display: inline-block;
      vertical-align: middle;
    }
    h4, p {
      width: auto;
    }

    .desripcion {
      display: inline-block;
      vertical-align: middle;
    }
  }

  .total {
    margin: 3% 0;
    h6, h4 {
      width: $full;
      display: inline-block;
      padding: 0 0;
      margin: 0 0;
    }

    h6 {
      text-transform: uppercase;
      text-align: right;
    }

    h4 {
      text-align: right;
    }

    form {
      width: $full;
      display: inline-block;
      select {
        width: $full;
      }

      input, select{
        float: right;
      }
    }

    .button {
      text-transform: uppercase;
    }
  }

  .redimir {
    width: 100%;
    display: inline-block;
    margin: 3% 0;
    button {
      border: 0;
      text-transform: uppercase;
    }
    h6 {
      margin: 3% 0;
    }
  }

}

.direccion {
  form {
    input[type="text"], input[type="email"], input[type="password"], input[type="date"], select, textarea{
      width: 100%;
      display: inline-block;
      margin: 2% 0;
      padding: 1% 2%;
      border: 1px solid $gray-darker;
    }


    input[type="submit"]{
      float: right;
      margin-top: 15px;
    }

    .contenedor {
      p {
        width: auto;
        display: inline-block;
        vertical-align: middle;
      }
    }

    textarea {
      height: 200px;
    }
  }

  h4 {
    padding: 2% 5%;
    display: inline-block;
    width: 100%;
    margin: 2% 0;
    text-transform: uppercase;
    font-weight: bold;
    background-color: $black;
    color: $white;
  }
}

.banner_our {
  @extend .banner;
  @extend .banner_v2;
  height: 60vh;
  text-align: center;
}

.process {
  .select {
    list-style:none;
    width: $full;
    display: inline-block;
    border: 1px solid $gray-darker;
    text-align: center;
    margin: 0;
    padding: 4% 0;
    position: relative;
    background-image: url("../images/back_select_our.png");
    background-position: center;
    background-size: contain;
    li {
      width: 25%;
      display: inline-block;
      a {
        width: $full;
        display: inline-block;
        cursor: pointer;
        color: $black;
        vertical-align: top;
        margin: 12% 2%;
        img {
          width: auto;
          height: 80px;
          margin: 2% 25%;
          display: inline-block;
        }
        span {
          width: $full;
          display: inline-block;
        }
      }
    }

    .t_m {
      width: $full;
      position: absolute;
      top: 34%;
      left: 0;
      text-align: center;
      h2 {
        width: auto;
        color: $white;
        font-size: 16px;
        background-color: $black;
        padding: 1% 2%;
        display: inline-block;
      }
    }
  }

  .gal_top {
    overflow: hidden;
  }

  .item {
    width: $full;
    height: auto;
    float: left;
    display: block;
    .img, .a_txt {
      width: $full;
      float: left;
      display: block;
    }
    .img {
      height: 200px;
    }
    .a_txt {
      /*height: 55%;*/
      padding: 16px 0;
    }
    .img {
      background-position: center;
      background-size: cover;
      background-repeat: no-repeat;
    }
  }
}

.h_sustainable {
  width: 90%;
  display: inline-block;
  background-position: center;
  background-size: cover;
  padding: 5% 0;
  margin: 5% 5% 0 5%;
  text-align: center;
  img {
    width: 60%;
    display: inline-block;
  }
}

.sustainable {
  @extend .contenedor;
  .item {
    @extend .banner;
    @extend .banner_v2;
    height: auto;
    margin: 2% 0;
    overflow: inherit;
    display: inline-block;
    .a_txt {
      padding: 5% 0;
      h2 {
        text-align: left;
        text-transform: capitalize;
      }
      p {
        text-align: left;
      }
    }

    .img {
      @extend .contenedor;
      height: 400px;
      background-position: center;
      background-size: cover;
    }
  }
  
  .item:nth-child(odd){
     .col_50:first-child {
       float: left;
     }
     .col_50{
       float: right;
     }
   }

  .item:nth-child(even){
    .col_50:first-child {
      float: right;
    }
    .col_50{
      float: left;
    }
  }

}

.gift {
  margin-top: 2%;
  text-align: center;
  .txt {
    @extend .contenedor;
    h2, p {
      margin: 2% 5%;
      text-align: left;
    }
    form {
      @extend .contenedor;
      padding: 2% 5%;
      input, select, textarea {
        display: inline-block;
        width: $full;
        margin: 2% 0;
      }

      input[type="text"], select{
        height: 30px;
        padding: 0 5%;
      }

      textarea {
        height: 200px;
      }
    }
    .button {
      width: 40%;
      background-color: $black;
      color: $white;
      margin: 2% 0;
      span {
        @extend .contenedor;
      }
    }
  }
  .img {
    @extend .contenedor;
    img {
      @extend .contenedor;
    }
  }

  .solcial_product {
    @extend .contenedor;
    h4 {
      margin-top: 4%;
    }
    a {
      width: 13%;
      display: inline-block;
      margin: 2%;
      img {
        @extend .contenedor;
      }
    }
  }
}

.message_remodal {
  h2 {
    margin: 3% 5%;
  }
}

.resumen_compra {
  .contenedor {
    padding: 2% 0;
  }

  h5 {
    width: $full - 10%;
    display: inline-block;
    padding: 2% 5%;
    text-align: center;
    font-size: 22px;
    margin: 0;
  }

  h4 {
    text-transform: capitalize;
    display: inline-block;
  }


  p {
    display: inline-block;
  }
}

@import "../front/breakpoint";

/*commint voy bien*/
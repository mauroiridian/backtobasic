<?php
include("bd.php");
$filename = "productos";         //File Name
/*******YOU DO NOT NEED TO EDIT ANYTHING BELOW THIS LINE*******/
//create MySQL connection <img src='' />
//$sql = "SELECT co.nombre, co.email, co.celular, co.asunto, ci.nombre AS ciudad, s.nombre AS sede FROM  `fos_user` co LEFT JOIN ciudad ci ON ci.id = co.ciudad_id LEFT JOIN sede s ON s.id = co.sede_id GROUP BY co.email";
$sql = "SELECT prod.id as id, prod.nombre_es, concat(\"http://" . $_SERVER["HTTP_HOST"] . "/uploads/productos/\",prod.imagen) as imagen, concat(\"http://" . $_SERVER["HTTP_HOST"] . "/uploads/productos/\",prod.imagenaux) as imagen_aux, prod.precio, prod.sku, col_data.nombre_es as color,gen_data.nombre_es as genero FROM  `producto` prod LEFT JOIN producto_color col ON col.producto_id = prod.id LEFT JOIN color col_data ON col.color_id = col_data.id LEFT JOIN producto_genero gen ON gen.producto_id = prod.id LEFT JOIN genero gen_data ON gen.genero_id = gen_data.id GROUP BY prod.id";
$Connect = mysqli_connect($DB_Server, $DB_Username, $DB_Password) or die("Couldn't connect to MySQL:<br>" . mysql_error() . "<br>" . mysql_errno());
//select database
$Db = mysqli_select_db($Connect,$DB_DBName) or die("Couldn't select database:<br>" . mysql_error(). "<br>" . mysql_errno());
//execute query
$result = mysqli_query($Connect,$sql) or die("Couldn't execute query:<br>" . mysql_error(). "<br>" . mysql_errno());
$file_ending = "xls";
//header info for browser
header("Content-Type: application/xls;  charset=utf-8");
header("Content-Disposition: attachment; filename=$filename.xls");
header("Pragma: no-cache");
header("Expires: 0");
/*******Start of Formatting for Excel*******/
//define separator (defines columns in excel & tabs in word)
$sep = "\t"; //tabbed character
//start of printing column names as names of MySQL fields
for ($i = 0; $i < mysqli_num_fields($result); $i++) {
    echo mysqli_fetch_field($result)->name . "\t";
}
print("\n");
//end of printing column names
//start while loop to get data
while($row = mysqli_fetch_array($result))
{
    $schema_insert = "";
    for($j=0; $j<mysqli_num_fields($result);$j++)
    {
        if(!isset($row[$j]))
            $schema_insert .= "NULL".$sep;
        elseif ($row[$j] != "")
            $schema_insert .= "$row[$j]".$sep;
        else
            $schema_insert .= "".$sep;
    }
    $schema_insert = str_replace($sep."$", "", $schema_insert);
    $schema_insert = preg_replace("/\r\n|\n\r|\n|\r/", " ", $schema_insert);
    $schema_insert .= "\t";
    print(mb_convert_encoding(trim($schema_insert), 'UTF-16LE', 'UTF-8'));
    print "\n";
}
mysqli_close($Connect);
?>
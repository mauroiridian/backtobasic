<?php
include("bd.php");
$filename = "usuarios";         //File Name
/*******YOU DO NOT NEED TO EDIT ANYTHING BELOW THIS LINE*******/
//create MySQL connection
//$sql = "SELECT co.nombre, co.email, co.celular, co.asunto, ci.nombre AS ciudad, s.nombre AS sede FROM  `fos_user` co LEFT JOIN ciudad ci ON ci.id = co.ciudad_id LEFT JOIN sede s ON s.id = co.sede_id GROUP BY co.email";
$sql = "SELECT us.username as usuario, us.email as correo, us.nombre, us.apellidos, us.telefono, us.fecha as fecha_nacimiento FROM  `fos_user` us GROUP BY us.email";
$Connect = mysqli_connect($DB_Server, $DB_Username, $DB_Password) or die("Couldn't connect to MySQL:<br>" . mysql_error() . "<br>" . mysql_errno());
$Db = mysqli_select_db($Connect,$DB_DBName) or die("Couldn't select database:<br>" . mysql_error(). "<br>" . mysql_errno());
$result = mysqli_query($Connect,$sql) or die("Couldn't execute query:<br>" . mysql_error(). "<br>" . mysql_errno());
//header info for browser
header("Content-Type: application/vnd.ms-excel; charset=UTF-8");
header("Content-Disposition: attachment; filename=$filename.xls");
header("Pragma: no-cache");
header("Expires: 0");
/*******Start of Formatting for Excel*******/
//define separator (defines columns in excel & tabs in word)
$sep = "\t"; //tabbed character
//start of printing column names as names of MySQL fields
for ($i = 0; $i < mysqli_num_fields($result); $i++) {
    echo mysqli_fetch_field($result)->name . "\t";
}
print("\n");
//end of printing column names
//start while loop to get data
while($row = mysqli_fetch_array($result))
{
    $schema_insert = "";
    for($j=0; $j<mysqli_num_fields($result);$j++)
    {
        if(!isset($row[$j]))
            $schema_insert .= "NULL".$sep;
        elseif ($row[$j] != "")
            $schema_insert .= "$row[$j]".$sep;
        else
            $schema_insert .= "".$sep;
    }
    $schema_insert = str_replace($sep."$", "", $schema_insert);
    $schema_insert = preg_replace("/\r\n|\n\r|\n|\r/", " ", $schema_insert);
    $schema_insert .= "\t";
    print(mb_convert_encoding(trim($schema_insert), 'UTF-16LE', 'UTF-8'));
    print "\n";
}
mysqli_close($Connect);
?>
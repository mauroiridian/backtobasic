<?php
include("bd.php");
$filename = "compras";         //File Name
/*******YOU DO NOT NEED TO EDIT ANYTHING BELOW THIS LINE*******/
//create MySQL connection
//$sql = "SELECT co.nombre, co.email, co.celular, co.asunto, ci.nombre AS ciudad, s.nombre AS sede FROM  `fos_user` co LEFT JOIN ciudad ci ON ci.id = co.ciudad_id LEFT JOIN sede s ON s.id = co.sede_id GROUP BY co.email";
$sql = "SELECT co.created_at as fecha_compra, co.precio as precio, dep.nombre as departamento_envio, ci.nombre as ciudad_envio, user.username as usuario, user.email as usuario_email, dir.direccion as direccion_envio, co.descripcion as descripcion FROM `compra` co LEFT JOIN envio dir ON co.direccion_id = dir.id LEFT JOIN departamento dep ON dir.departamento_id = dep.id LEFT JOIN ciudad ci ON dir.ciudad_id = ci.id LEFT JOIN fos_user user ON dir.user_id = user.id";
$Connect = mysqli_connect($DB_Server, $DB_Username, $DB_Password) or die("Couldn't connect to MySQL:<br>" . mysql_error() . "<br>" . mysql_errno());
//select database
$Db = mysqli_select_db($Connect,$DB_DBName ) or die("Couldn't select database:<br>" . mysql_error(). "<br>" . mysql_errno());
//execute query
$result = mysqli_query($Connect,$sql) or die("Couldn't execute query:<br>" . mysql_error(). "<br>" . mysql_errno());
$file_ending = "xls";
//header info for browser
header("Content-Type: application/xls;  charset=utf-8");
header("Content-Disposition: attachment; filename=$filename.xls");
header("Pragma: no-cache");
header("Expires: 0");
/*******Start of Formatting for Excel*******/
//define separator (defines columns in excel & tabs in word)
$sep = "\t"; //tabbed character
//start of printing column names as names of MySQL fields
for ($i = 0; $i < mysqli_num_fields($result); $i++) {
    echo mysqli_fetch_field($result)->name . "\t";
}
print("\n");
//end of printing column names
//start while loop to get data
while($row = mysqli_fetch_array($result))
{
    $schema_insert = "";
    for($j=0; $j<mysqli_num_fields($result);$j++)
    {
        if(!isset($row[$j]))
            $schema_insert .= "NULL".$sep;
        elseif ($row[$j] != "")
            $schema_insert .= "$row[$j]".$sep;
        else
            $schema_insert .= "".$sep;
    }
    $schema_insert = str_replace($sep."$", "", $schema_insert);
    $schema_insert = preg_replace("/\r\n|\n\r|\n|\r/", " ", $schema_insert);
     $schema_insert .= "\t";
    utf8_decode($schema_insert);
    $print= implode(" | ", explode("<td>",str_replace("</tr><tr>", "", str_replace("</td>", "", str_replace("</tr></table>", "", str_replace("<table><tr>", "",trim($schema_insert)))))));
    print(mb_convert_encoding($print, 'UTF-16LE', 'UTF-8'));
    print "\n";
}

mysqli_close($Connect);
?>
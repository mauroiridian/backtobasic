var img_actual;
var link_Actual;
var texto;
var frameSrc;
var padre;
var isInIframe = (window.location != window.parent.location) ? true : false;
var base_path = '/uploads/images/general/';
var input_id ;
var input_name;
$raiz = '';
if(window.location.href.indexOf("app_dev.php") > -1) {
    $raiz = '/app_dev.php';
}

function embeddImagesForm() {
    $('#general_logo,#seo_imagen,#tipoevento_thumb_es,#tipoevento_thumb_en,#tipoevento_imagen_es' +
        ',#tipoevento_imagen_en,#taller_numero,#convenio_imagen,#sede_imagen,#testimonio_imagen' +
        ',#academia_thumb,#academia_mediana,#academia_grande,#campana_thumb,#campana_grande' +
        ',#noticia_imagen,#servicio_imagen').each(function(){
        img_actual = $(this).val();
        padre = $(this).parent();
        $id = $(this).attr('id');
        $name = $(this).attr('name');
        cleanContainer();
        if($.isNumeric(img_actual)){
            addBoton(padre,$raiz+'/admin/?action=edit&entity=Imagen&referer=modal&id='+img_actual,$id,$name,'editar imagen');
            texto = $(this).find(':selected').text();
            loadImagen(img_actual,base_path+texto,padre,$name);
        }else{
            addBoton(padre,$raiz+'/admin/?action=new&entity=Imagen&referer=modal',$id,$name,'crear imagen');
        }
    });
    clickBoton();
}

function fixImageSave(){
    try {
        $el = $('#imagen_alt,#imagengaleria_alt, #imagensede_alt, #mailing_alt, #genero_alt');
        $el = $("[id$=_alt]");
        $val = $el.val();
        var lastChar = $val.substr($val.length - 1);
        if (lastChar == '_')
            $el.val($val.substring(0, $val.length - 1));
        else
            $el.val($el.val() + '_');
    }catch(err){}
}

function loadImagen(id,ruta, papa,name){
    if(!papa)
        papa = padre;
    if(!name)
        name = input_name;
    console.log(name+' loadImagen');
    cleanContainer();
    addBoton(papa,$raiz+'/admin/?action=edit&entity=Imagen&referer=modal&id='+id,input_id,name,'editar imagen');
    $.ajax({
        url: "/ruta_imagen/"+id,
    }).done(function(html) {
        ruta = html;
        papa.append('<img src="'+ruta+'" class="thumb_img">');
        $('#imagen_modal').modal({show:false});
        $('#imagen_modal').addClass('hide');
        $('.modal-footer .btn').click();
        papa.append('<input type="hidden" id="'+input_id+'" name="'+name+'" value="'+id+'"/>');
        clickBoton();
    });

}

function clickBoton(){
    $('.img_form').unbind('click');
    $('.img_form').click(function(){
        padre = $(this).parent();
        frameSrc = $(this).data('link');
        input_id = $(this).data('id');
        input_name = $(this).data('name');
        $('#imagen_modal iframe').attr("src",frameSrc);
        $('#imagen_modal').modal({show:true});
        $('#imagen_modal').removeClass('hide');

    });
}

function cleanContainer(){
    padre.html('');
}

function addBoton(el,link,id,name,text){
    console.log(name + 'addBoton');
    $cad = '<a href="#" class="btn btn-primary img_form" data-id="'+id+'" data-name="'+name+'" data-link="'+link+'" role="button" style="clear: both">'+text+'</a>';
    el.append($cad);
}

function cleanDOM(){
    $wrapper = $('.content-wrapper').clone();
    if(isInIframe){
        $wrapper.css({'margin':0});
        //$wrapper.find('.action-delete,.action-list,.checkbox').remove();
        //$wrapper.find('.action-delete,.action-list').remove();
        $('body').html('');
        $('body').append($wrapper);
    }
}
function fixIframe(){
    $("#iri_iframe").load(function() {
        $(this).height( $(this).contents().find("body").height() + 100 );
        $("html, body").animate({ scrollTop: $('#iri_iframe').offset().top }, 1000);
    });
}

function clickEdit(){
    $('#contenedor_objetos, #new_objeto').find('a').click(function(e){
        e.preventDefault();
        var url = $(this).attr('href');
        $("#iri_iframe").attr('src',url);
        fixIframe();
        checkUrl();
    });
}

function limpiarIframe(){
    var frame = document.getElementById("iri_iframe"),
        frameDoc = frame.contentDocument || frame.contentWindow.document;
    frameDoc.documentElement.innerHTML = "";
}

function checkUrl(){
    var action = getUrlParameter('action');
    var entity = getUrlParameter('entity');
    console.log(action);
    console.log(entity);
    if(action == 'list'){
        if(entity == 'Imagengaleria' || entity == 'Imagensede' || entity == 'Galeriaproducto' || entity == 'GaleriaPost'){
            parent.limpiarIframe();
            parent.location.reload();
        }
    }else{
        $('.action-list').remove();
    }
    if(action == 'new'){
        if(entity == 'Imagengaleria' || entity == 'Imagensede' || entity == 'Galeriaproducto' || entity == 'GaleriaPost'){
            var select = $('#imagengaleria_galeria,#imagensede_sede,#galeriaproducto_producto,#galeriapost_post');
            var id = getUrlParameter('galeria');
            var llave = select.find('option[value="'+id+'"]').text();
            var padre = select.parent();
            padre.html(llave);
            if(entity == 'Imagengaleria')
                padre.append('<input type="hidden" id="imagengaleria_galeria" name="imagengaleria[galeria]" value="'+id+'"/>');
            else if(entity == 'Imagengaleria')
                padre.append('<input type="hidden" id="imagensede_sede" name="imagensede[sede]" value="'+id+'"/>');
            else if(entity == 'Galeriaproducto')
                padre.append('<input type="hidden" id="galeriaproducto_producto" name="galeriaproducto[producto]" value="'+id+'"/>');
            else if(entity == 'GaleriaPost')
                padre.append('<input type="hidden" id="galeriapost_post" name="galeriapost[post]" value="'+id+'"/>');
        }

    }
}

function sortable(){
    $( "#contenedor_objetos" ).sortable({
        revert: true,
        helper : 'clone',
        stop: function() {
            bindOrden();
        }
    });
}

function bindOrden(){
    $('#contenedor_objetos a').each(function (i){
        console.log('#objeto_'+$(this).data('id'));
        $('#objeto_'+$(this).data('id')).val(i);
    });
    var form = $('#orden_form');
    console.log(form.serialize());
    /**/
    $.ajax({
        url   : form.attr('action'),
        type  : form.attr('method'),
        data  : form.serialize(),
        success: function(data){
            //location.reload();
            console.log(data);
        }
    });
    /**/
}

function addModal(){
    var cad = '<div id="imagen_modal" class="modal hide fade" tabindex="-1" role="dialog" >'
        +'<div class="modal-header">'
        +'<button type="button" class="close" data-dismiss="modal">×</button>'
        +'<h3>Imagen</h3>'
        +'</div>'
        +'<div class="modal-body">'
        +'<iframe src="" style="zoom:0.60" width="99.6%" height="850" frameborder="0"></iframe>'
        +'</div>'
        +'<div class="modal-footer">'
        +'<button class="btn" data-dismiss="modal">OK</button>'
        +'</div>'
        +'</div>';
    $('body').append(cad);
}

function addLogout(){
    var cad = '<ul class="sidebar-menu">'
        +'<li class="  ">'
        +'<a href="'+$raiz+'/logout" title="logout">logout</a>'
        +'</li>'
        +'</ul>';
    $('.sidebar-menu').append(cad);
}

function loadHexas(){
    $(".hexa input[type=text]").each(function () {
        $val = $(this).val();
        if(!$val)
            $val = "#f00";
        $(this).spectrum({
            color: $val,
            preferredFormat: "hex",
            showInput: true
        });
    });
}

$(function(){
    addModal();
    addLogout();
    cleanDOM();
    embeddImagesForm();
    fixImageSave();
    loadHexas();
    if(isInIframe){
        checkUrl();
    }else{
        fixIframe();
        clickEdit();
        sortable();
    }
});


/**Mapa**/
var map;
function initMap(){
    if(!document.getElementById('map')){
        return;
    }
    $('form[name="ditribuidor"]').find('button[type="submit"]').parent().parent().parent().before('<div id="map"  class="col-md-12" style="height: 300px;margin-bottom: 20px"></div>');
    if($('form[name="ditribuidor"]').find('button[type="submit"]')){
        var lat;
        var lng;
        var myLatLng = {lat: 4.690714, lng: -74.061126};
        var lat_prev = $('#ditribuidor_lat').val();
        var lng_prev = $('#ditribuidor_lng').val();
        console.log(parseFloat(lat_prev));
        if(lat_prev && lng_prev)
            myLatLng = {lat: parseFloat(lat_prev), lng: parseFloat(lng_prev)};
        else{
            $('#ditribuidor_lat').val(myLatLng.lat);
            $('#ditribuidor_lng').val(myLatLng.lng);
        }
        map = new google.maps.Map(document.getElementById('map'), {
            center: myLatLng,
            zoom: 12
        });

        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            draggable:true,
            title: 'Sede'
        });

        marker.addListener('dragend', function() {
            myLatLng = marker.getPosition();
            map.setCenter(myLatLng);
            lat = myLatLng.lat();
            lng = myLatLng.lng();
            $('#ditribuidor_lat').val(lat);
            $('#ditribuidor_lng').val(lng);
        });
    }

}

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};
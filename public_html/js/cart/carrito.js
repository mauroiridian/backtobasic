$(function () {
    listado_productos();
    clickColores();
    agregar();
    departamentoChange();
    tallasgenero();
    getTallasgenero();
    getCiudadesDept();
    getCostoEnvio();
    hideUser();
    activeTalla();
});

function clickColores(){
    $('.colors a').click(function(){
        var check = $(this).find('input');
        if (check.is(':checked')) {
            check.prop('checked', false);
            $(this).removeClass('active');
        }else {
            check.prop('checked', true);
            $(this).addClass('active');
        }
        filtrar();
    });
}

function listado_productos(){
    var inputs = $('.filter input');
    inputs.click(function () {
        filtrar();
    });
}
function filtrar() {
    var arrs = [], arrs_class = [];
    $('.filter ul.grupo').each(function () {
        arrs.push($(this).find('input:checked'));
    });
    $.each( arrs, function( index, value ){
        var arr_temp = $.map( value, function( el ) {
            return ( '.'+$(el).data('class') );
        });
        arrs_class.push(arr_temp);
    });
    var cp = [];
    for(var i = 0; i < arrs_class.length - 1; i++){
        if(i == 0)
            cp = cartesianProductOf(arrs_class[i],arrs_class[i+1]);
        else
            cp = cartesianProductOf(cp,arrs_class[i+1]);
    }
    cp = cp.join( "," );
    $('.list .item').hide();
    if(cp)
        $(cp).show();
    else
        $('.list .item').show();
}

function cartesianProductOf(arr1,arr2) {
    var customerDebtorMatrix = [];
    for (var i = 0; i < arr1.length; i++) {
        for (var l = 0; l < arr2.length; l++) {
            customerDebtorMatrix.push(arr1[i]+arr2[l]);
        }
    }
    if(arr1.length == 0)
        return arr2;
    if(arr2.length == 0)
        return arr1;
    return customerDebtorMatrix;
};

function agregar() {
    $('.eliminar').click(function (e) {
        e.preventDefault();
        addCarritoTalla($(this).data('id'),$(this).data('cant'),$(this).data('id-talla'));
    });
    $('.add_talla').click(function(e){
        e.preventDefault();
        $.LoadingOverlay("show",{ zIndex: 9999, image: '/js/jquery-loading-overlay/src/loading.gif'});
        addCarritoTalla($(this).data('id'),$(this).data('cant'),$('#select-talla').val());
    })
    $('.select-cant').change(function () {
        $.LoadingOverlay("show",{ zIndex: 9999, image: '/js/jquery-loading-overlay/src/loading.gif'});
        setCarritoTalla($(this).parent().parent().data('id'),$(this).val(),$(this).parent().parent().data('id-talla'));
    });

    $('.eliminar_bono').click(function (e) {
        e.preventDefault();
        removeCarritoBono($(this).data('id'));
    });
}

function addCarrito(id,cant){
    $.ajax({
        url: $raiz+"/add-carrito/"+id+"/"+cant,
    })
    .done(function(data) {
        console.log(data.cantidad);
        $('#cantidad_'+id).html(data.cantidad);
        verGocarrito();
        if(window.location.href.indexOf("datos") > -1) {
            /*
             if(data.cantidad < 1){
             $('#fila_'+id).remove();
             }
             */
            window.location.reload();
        }
    })
    .fail(function() {
    })
    .always(function() {
    });

    function verGocarrito(){
        TweenMax.to($('.go_carrito'), 0.8, { 'opacity': 1 });
    }
}

function addCarritoTalla(id,cant,talla){

    $.ajax({
        url: $raiz+"/add-carrito-talla/"+id+"/"+cant+"/"+talla,
    })
    .done(function(data) {
        console.log(data.cantidad);
        if(window.location.href.indexOf("carrito-de-compras") > -1) {
            window.location.reload();
        }
        window.location = $raiz + '/carrito-de-compras';
    })
    .fail(function() {
        $.LoadingOverlay("hide");
    })
    .always(function() {
    });
}

function removeCarritoBono(id){

    $.ajax({
        url: $raiz+"/remove-carrito-bono/"+id,
    })
        .done(function(data) {
            console.log(data.cantidad);
            if(window.location.href.indexOf("carrito-de-compras") > -1) {
                window.location.reload();
            }
            window.location = $raiz + '/carrito-de-compras';
        })
        .fail(function() {
            $.LoadingOverlay("hide");
        })
        .always(function() {
        });
}

function setCarritoTalla(id,cant,talla){

    $.ajax({
        url: $raiz+"/set-carrito-talla/"+id+"/"+cant+"/"+talla,
    })
        .done(function(data) {
            console.log(data.cantidad);
            if(window.location.href.indexOf("carrito-de-compras") > -1) {
                window.location.reload();
            }
            window.location = $raiz + '/carrito-de-compras';
        })
        .fail(function() {
            $.LoadingOverlay("hide");
        })
        .always(function() {
        });
}

/****  Dirección ***/

function tallasgenero(){
$('#carroiridianbundle_preferenciasbox_genero').change(function () {
getTallasgenero();
})
}

function getTallasgenero(){
    $.LoadingOverlay("show",{ zIndex: 9999, image: '/js/jquery-loading-overlay/src/loading.gif'});
    $.ajax({
        url: $raiz+"/tallas-genero-box/"+$('#carroiridianbundle_preferenciasbox_genero').val()+"/1",
    })
        .done(function(html) {
            $('#carroiridianbundle_preferenciasbox_tallapantalon').html(html);
        })
        .fail(function() {
        })
        .always(function() {
            $.LoadingOverlay("hide");
        });

    $.ajax({
        url: $raiz+"/tallas-genero-box/"+$('#carroiridianbundle_preferenciasbox_genero').val()+"/2",
    })
        .done(function(html) {
            $('#carroiridianbundle_preferenciasbox_tallazapatos').html(html);
        })
        .fail(function() {
        })
        .always(function() {
            $.LoadingOverlay("hide");
        });

}

function departamentoChange(){
    $('#envio_departamento').change(function () {
        getCiudadesDept();
    });
}
function getCiudadesDept(){
    if(window.location.href.indexOf("carrito-de-compras") > -1){
        $.LoadingOverlay("show",{ zIndex: 9999, image: '/js/jquery-loading-overlay/src/loading.gif'});
        $.ajax({
            url: $raiz+"/ciudades-dept/"+$('#envio_departamento').val(),
        })
        .done(function(html) {
            $('#envio_ciudad').html(html);
            costoEnvio();
        })
        .fail(function() {
        })
        .always(function() {
            $.LoadingOverlay("hide");
        });
    }

}
function getCostoEnvio(){
    $('#envio_ciudad').change(function () {
        costoEnvio();
    });
}
function costoEnvio(){
    $("input[name=tipo_pago_check]").change(function () {
        $.LoadingOverlay("show",{ zIndex: 9999, image: '/js/jquery-loading-overlay/src/loading.gif'});
        $.ajax({
            url: $raiz+"/set-tipo-pago/" + $(this).val(),
        }).done(function(data) {
            console.log("ok ");
            $.LoadingOverlay("hide");
        })
    })
    $envio_ciudad = $('#envio_ciudad');
    $total_carrito = $('#total-carrito');
    $('#costo-envio-span').html($envio_ciudad.find(':selected').data('costo'));
    var goToDir = true;
    var result = 0;
    if($envio_ciudad.find(':selected').data('costo-noformato') != undefined){
        $('#costo-envio').val($envio_ciudad.find(':selected').data('costo'));
        result = parseInt($('#total-carrito').val()) + parseInt($envio_ciudad.find(':selected').data('costo-noformato'));
    }else{
        result = parseInt($('#total-carrito').val()) + parseInt($('#costo-envio').val());
        goToDir = false;
    }
    $("#total-resultado").html("$" + result.toLocaleString().replace(".", ","));
    var iva = Math.round($total_carrito.val() * 0.19/1.19);
    $('#carrito-iva').html("$" + iva.toLocaleString().replace(".", ","));
    $("#comprar").click(function(){
        //
        if(goToDir){
            $("#direccion_form").submit();
        }else{
            window.location = $raiz+"/pagar-payu"
        }

    })

}

function hideUser(){
    $('#envio_user').parent().hide();
}

function activeTalla(){
    $('.size a').click(function(){
        var value = $(this).data('value');
        $('#select-talla').val(value);
        $('.size a').removeClass('active');
        $(this).addClass('active');
    });
}
$raiz = '';
var galleryTop;
if(window.location.href.indexOf("app_dev.php") > -1) {
    $raiz = '/app_dev.php';
}
$(function(){
    $("img").error(function () {
        $(this).hide();
        // or $(this).css({visibility:"hidden"});
    });

    newsletter();
    galeriaOur();


    if (!isMobile()) {

        var swiper = new Swiper('.rot_blog', {
            nextButton: '.rot_blog .right',
            prevButton: '.rot_blog .left',
            slidesPerView: 6,
            paginationClickable: true,
            spaceBetween: 30
        });
    }

    if (isMobile()) {

        var swiper = new Swiper('.rot_blog', {
            slidesPerView: 1,
            paginationClickable: true,
            spaceBetween: 30,
            loop: true
        });
    }

    galleryTop = new Swiper('.process .gal_top', {
        spaceBetween: 10,
        slidesPerView: 1
    });


    $('.footer .social a:nth-child(4)').tooltipster({
        animation: 'grow',
        interactive: 'true'
    });

    $('.footer .social a:nth-child(4)').removeAttr( "href" );


    //$( ".datepicker" ).datepicker();


});

function FontSize(){

    if (!isMobile()) {

        $('.menu li a').flowtype({
            fontRatio: 5,
            minFont:14,
            maxFont: 24
        });

    }

    $('.a_txt h2').flowtype({
        fontRatio : 7,
        maxFont : 72
    });

    $('.a_txt h3').flowtype({
        fontRatio : 7,
        maxFont : 120
    });

    $('.a_txt p').flowtype({
        fontRatio : 20,
        maxFont : 20
    });

    if (isMobile()) {
        $('.a_txt p').flowtype({
            fontRatio : 20,
            maxFont : 16
        });
    }

    $('.sustainable .a_txt h2').flowtype({
        fontRatio : 7,
        maxFont : 25
    });

    $('.sustainable .a_txt p').flowtype({
        fontRatio : 20,
        maxFont : 18
    });


    $('.outstanding_blog h2').flowtype({
        fontRatio : 20,
        maxFont : 30
    });

    $('.the_blog .txt h2, .the_blog .item h2').flowtype({
        fontRatio : 10,
        maxFont : 40
    });


    $('.the_blog .social_feed h2').flowtype({
        fontRatio : 4,
        maxFont : 26
    });

    $('.the_blog h3').flowtype({
        fontRatio : 10,
        maxFont : 24
    });

    $('.the_blog p').flowtype({
        fontRatio : 10,
        maxFont : 16
    });

    $('.title_g h2').flowtype({
        fontRatio : 5,
        maxFont : 72
    });

    $('.title_g h3').flowtype({
        fontRatio : 3.5,
        maxFont : 120
    });

    $('.title_g2 h2').flowtype({
        fontRatio : 5,
        maxFont : 250
    });

    $('.title_g2 h3').flowtype({
        fontRatio : 3,
        maxFont : 120
    });

    if (isMobile()) {

        $('.title_g2 h3').flowtype({
            fontRatio : 3,
            maxFont : 74
        });
    }

    $('.follow_i h2').flowtype({
        fontRatio : 15,
        maxFont : 30
    });


    $('.dual_title h2').flowtype({
        fontRatio : 3.1,
        maxFont : 325
    });

    $('.dual_title h3').flowtype({
        fontRatio : 5.5,
        maxFont : 152
    });

    if (isMobile()) {
        $('.dual_title h2').flowtype({
            fontRatio : 4,
            maxFont : 325
        });

        $('.dual_title h3').flowtype({
            fontRatio : 6,
            maxFont : 152
        });

        $('.title_g2 h2').flowtype({
            fontRatio : 6,
            maxFont : 250
        });

        $('.title_g2 h3').flowtype({
            fontRatio : 5,
            maxFont : 120
        });

    }
}

function isMobile() {
    return(/Android|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) );
}

$( document ).ready(function() {
    hover();

    if (isMobile()) {
        $( "#v_menu" ).click(function() {
            $( ".menu ul, .menu .access .contenedor" ).slideToggle( "slow");
            //console.log('click');
        });
        $("div[data-imagen-mobil]").each(function(){
            $(this).css("background-image", "url(" + $(this).attr("data-imagen-mobil") + ")")
        })
        fixMenuProducts();
    }

    distribuidores();
    home();
    we_are();
    fixCarrito();
    FontSize();


    /*if (isMobile()) {
        var elem = $("#gal_top"),
            elemPos = elem.offset().top;

        $('.select li a').click(function () {
            TweenLite.to(window, 2, {scrollTo: elemPos});
        });

    }*/
    var let_submit = false;
    $("#register_form").submit(function(){
        var form = $(this);
        if(!let_submit){
            $('[data-remodal-id=message]').remodal().open();
            $('[data-remodal-id=message]').on('closed', function (e) {
                let_submit = true;
                form.submit();
            });
        }
        return let_submit;
    })


    /*$( "#go_legal" ).on( "click", function() {
		console.log('click legal');
	});
    $( "#go_legal" ).trigger( "click" );*/


});

$( window ).resize(function() {
    sizeVideo();
    fixCarrito();
});


$(window).on("load", function() {
    products();
    dimVideo();
    sizeVideo();
});

$(window).scroll(function(){

    fixCarrito();

});

var w_ini, h_ini, prop;

function dimVideo(){
    w_ini = $('.my-video').attr('width');
    h_ini = $('.my-video').attr('height');
    //w_ini = 1280;
    //h_ini = 720;
    //console.log(w_ini);
}
function sizeVideo(){
    var contacto = $('.my-video').parent();
    var wC = contacto.width();
    var hC = contacto.height();
    var scale_w = wC / w_ini;
    var scale_h = hC / h_ini;
    var scale = scale_w;
    if(scale_h > scale_w)
        scale = scale_h;
    //console.log(scale_w);
    //console.log(scale_h);

    contacto.find('video').attr('width', w_ini * scale);
    contacto.find('video').attr('height', h_ini * scale);
}


function home(){
    $('.category .sec').mouseenter(function(){
        var img_g = $(this).find('.img2');
        var t_g = $(this).find('.t_g');
        TweenMax.to(img_g, 0.5, {'opacity': '0', ease: Power2.easeOut});

        var t1 = new TimelineMax({repeat: 0});

        t1.to(t_g, 0.1, {opacity: 1})
            .to(t_g, 0.7, {left: 10, opacity: 0, ease: Power1.easeInOut})
            .to(t_g, 0.1, {left: 0});
    });

    $('.category .sec').mouseleave(function(){
        var img_g = $(this).find('.img2');
        TweenMax.to(img_g, 0.5, {'opacity': '1', ease: Power2.easeOut});
    });



}

function we_are(){
    $('.b_we_are .title h2').flowtype({
        fontRatio : 8.5
    });

    $('.b_we_are .title h3').flowtype({
        fontRatio : 5
    });

    $('.b_we_are .title h4').flowtype({
        fontRatio : 6
    });
}

function animaTopfxm(){
    if(scrollPosition <= TopCmenu ){
        //console.log('menor');
        TweenMax.to($('html, body'), 1.5, { scrollTop: TopCmenu });
    }
}

var form_news;
function newsletter(){
    $('#form_newsletter').submit(function (e) {
        $.LoadingOverlay("show",{ zIndex: 9999, image: '/js/jquery-loading-overlay/src/loading.gif'});
        e.preventDefault();
        data = $(this).serialize();
        $.ajax({
            url: $raiz+"/newsletter/"+$(this).find('input[type="email"]').val()
        }).done(function(data) {
            $.LoadingOverlay("hide");
            //console.log(data);
            $('#exitoso').remove();
            if(data.success == 1 || data.success == "1"){
                //console.log('entre');
                $('#form_newsletter').prepend('<p id="exitoso">Inscrito exitosamente</p>');
                $('#form_newsletter').find('input[type="email"]').val('');
            }else{
                $('#form_newsletter').prepend('<p id="exitoso" class="error">Email inscrito anteriormente</p>');
            }
        });
    });
}


var scrollPosition = 0;



function fixCarrito(){
    var hheader = $('.menu').height();
    var carrito = $('.access .carrito');


    $(carrito).css({'top': hheader + 25});

    var p = $(carrito).find('p');
    var img = $(carrito).find('img');

    $(carrito).mouseenter(function(){
        TweenMax.to(p, 1, {css:{top: '-10', backgroundColor: '#000000'}, ease: Elastic.easeOut,});
        TweenMax.to(img, 1, {css:{backgroundColor: '#000000'}, ease: Elastic.easeOut,});
    });
    $(carrito).mouseleave(function(){
        TweenMax.to(p, 1, {css:{top: '0', backgroundColor: '#999999'}, ease: Elastic.easeOut,});
        TweenMax.to(img, 1, {css:{backgroundColor: '#999999'}, ease: Elastic.easeOut,});
    });

    scrollPosition = $(window).scrollTop();

    if(scrollPosition >= hheader ){
        $(carrito).css({'position': 'fixed', 'top': 25});
    }else {
        $(carrito).css({'position': 'absolute', 'top': hheader + 25});
    }
}



function products(){
    if (isMobile()) {

        $( ".products" ).on( "click", ".v_filter", function() {
            TweenMax.to($('.cont_filter'), 0.5, {'left': '0%', ease: Power2.easeOut,});
            TweenMax.to($(this), 0.6, {'left': '80%', ease: Power2.easeOut,});
            $(this).removeClass('v_filter');
            $(this).addClass('n_filter');
            animaTopfxm();
        });

        $( ".products" ).on( "click", ".n_filter", function() {
            TweenMax.to($('.cont_filter'), 0.5, {'left': '-80%', ease: Power2.easeOut,});
            TweenMax.to($(this), 0.6, {'left': '0%', ease: Power2.easeOut,});
            $(this).addClass('v_filter');
            $(this).removeClass('n_filter');
        });


        $(".cont_filter").mCustomScrollbar();

    }
    var Wdocument = $(document).width();
    var qt = (Wdocument/3);

    $('.grid').masonry({
        // options
        itemSelector: '.grid-item',
        // use element for option
    });




    if (!isMobile()) {

        $('.products .list .txt h4').flowtype({
            fontRatio: 10,
            maxFont: 24
        });
    }

    $( ".the_produc .desp .button" ).click(function() {
        $(this).next('.item').slideToggle( "slow");
    });

    var galleryTop = new Swiper('.gallery-top', {
        onInit: function(){
            callZoom();
        },
        spaceBetween: 10,
        onSlideChangeEnd: function(){
            $('.zoomContainer').remove();
            callZoom();
        }
    });
    var galleryThumbs = new Swiper('.gallery-thumbs', {
        spaceBetween: 10,
        centeredSlides: true,
        slidesPerView: 4,
        touchRatio: 0.2,
        slideToClickedSlide: true
    });
    galleryTop.params.control = galleryThumbs;
    galleryThumbs.params.control = galleryTop;

    function callZoom(){
        $(".gal .gallery-top .item.swiper-slide-active").elevateZoom({
            constrainType: "height",
            constrainSize: 400,
            zoomType: "lens",
            containLensZoom: true,
            cursor: 'pointer'
        });
    }


}


var hCmenu = $('.contenedor.menu').height();
var hBannerp = $('.products').prev('.banner').height();
var TopCmenu = (hCmenu + hBannerp);

function fixMenuProducts(){
    scrollPosition = $(window).scrollTop();

    if(scrollPosition >= TopCmenu ){
        $('.cont_filter, .ico_filter').css({'position': 'fixed'});
    }else {
        $('.cont_filter, .ico_filter').css({'position': 'absolute'});
    }
}

function hover(){
    /*$(".item").mouseenter(function () {
        var hover = $(this).find('.hover');
        TweenMax.to(hover, 0.2, {opacity: 1});
    });

    $(".item").mouseleave(function () {
        var hover = $(this).find('.hover');
        TweenMax.to(hover, 0.2, {opacity: 0});
    });*/
}

function distribuidores(){

    if (!isMobile()) {
        $('#datos_distri').click(function () {
            TweenMax.to($('.datos'), 0.5, {'right': '-100%', 'opacity': 1, ease: Power2.easeOut,});
        });

        $('.datos .close').click(function () {
            TweenMax.to($('.datos'), 0.5, {'right': '0', 'opacity': 0, ease: Power2.easeOut,});
        });
    }else {
        $('#datos_distri').click(function () {
            TweenMax.to($('.datos'), 0.5, {'z-index': 100, 'opacity': 1, ease: Power2.easeOut,});
        });

        $('.datos .close').click(function () {
            TweenMax.to($('.datos'), 0.5, {'z-index': 1, 'opacity': 0, ease: Power2.easeOut,});
        });
    }
}
function galeriaOur(){
    $('#rot_our li').click(function(){
        galleryTop.slideTo($(this).index());
    });
}
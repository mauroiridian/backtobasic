<?php
/**
 * Created by PhpStorm.
 * User: Iridian 1
 * Date: 1/02/2016
 * Time: 12:29 PM
 */

namespace AppBundle\Service;

use CarroiridianBundle\Entity\PagoSuscripcion;
use CarroiridianBundle\Entity\Suscripcion;
use CarroiridianBundle\Entity\Tarjeta;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class QI
{
    protected $em;
    protected $request_stack;
    protected $locale;
    protected $container;

    protected $textos		= null;
    protected $textosDB		= null;
    protected $textosBigDB	= null;

    protected $settings		= null;
    protected $imagenes = null;
    protected $imagenesGen = null;


    public function __construct(EntityManager $em, RequestStack $request_stack, Container $container)
    {
        $this->em = $em;
        $this->request_stack = $request_stack;
        $this->container = $container;
        //$this->locale = $request_stack->getCurrentRequest()->getLocale();
    }

    public function qs($clase)
    {
        return $this->em->getRepository('AppBundle:Texto')->findAll();
    }

    private function getResults($entidad){
        $qb = $this->em->createQueryBuilder()
            ->select('s')
            ->from($entidad, 's', 's.llave')
            ->where('s.llave is not null')
            ->getQuery()
            ->useQueryCache(true)
            ->useResultCache(true)
            ->getArrayResult();
        return $qb;
    }

    /**
     * Retorna la variable con textos populada la primera vez que se llama esta función, los textos se traen de la base de datos
     * @return multitype: Arreglo con los textos intenacionalizados
     */
    private function getTextosDB() {
        if ($this->textosDB == null) {
            $this->textosDB = $this->getResults('AppBundle:Texto');
        }
        return $this->textosDB;
    }

    /**
     * Obtener uno de los texto fijos internacionalizados según el UserCulture actual y el nombre del texto solicitado. Los textos se traen de la base de datos
     * @param string $key Nombre (identificador) del texto solicitado
     * @return string El texto solicitado
     */
    public function getTextoDB($key) {
        $locale = $this->request_stack->getCurrentRequest()->getLocale();
        $arrTextos = $this->getTextosDB();

        if(isset($arrTextos[$key][$locale]) && $arrTextos[$key][$locale] != '')
            return $arrTextos[$key][$locale];
        else
            return " ";
    }

    private function getTextosBigDB() {
        if ($this->textosBigDB == null) {
            $this->textosBigDB = $this->getResults('AppBundle:TextoBig');
        }
        return $this->textosBigDB;
    }

    public function getTextoBigDB($key) {
        $locale = $this->request_stack->getCurrentRequest()->getLocale();
        $arrTextos = $this->getTextosBigDB();
        if(isset($arrTextos[$key][$locale]) && $arrTextos[$key][$locale] != '')
            return $arrTextos[$key][$locale];
        else
            return $key;
    }


    private function getImagenes() {
        if ($this->imagenes == null) {
            $this->imagenes = $this->getResults('AppBundle:Imagengaleria');
        }
        return $this->imagenes;
    }

    private function getImagenesGeneral() {
        if ($this->imagenesGen == null) {
            $this->imagenesGen = $this->getResults('AppBundle:Imagen');
        }
        return $this->imagenesGen;
    }

    public function getImagengen($key) {
        $locale = $this->request_stack->getCurrentRequest()->getLocale();
        $locale = ucfirst($locale);
        $imagenes = $this->getImagenesGeneral();
        $path = $this->container->getParameter('app.path.images');
        //exit(\Doctrine\Common\Util\Debug::dump($imagenes));
        if(isset($imagenes[$key])){
            return $path.'/'.$imagenes[$key]['image'];
        }
        else
            return $path.'/';
    }


    public function getImagen($key) {
        $locale = $this->request_stack->getCurrentRequest()->getLocale();
        $locale = ucfirst($locale);
        $imagenes = $this->getImagenes();
        $path = $this->container->getParameter('app.path.imagesgal');
        if(isset($imagenes[$key])){
            return $path.'/'.$imagenes[$key]['image'.$locale];
        }
        else
            return $path.'/';
    }

    public function getImagenMobil($key) {
        $locale = $this->request_stack->getCurrentRequest()->getLocale();
        $locale = ucfirst($locale);
        $imagenes = $this->getImagenes();
        $path = $this->container->getParameter('app.path.imagesgal');
        if(isset($imagenes[$key])){
            return $path.'/'.$imagenes[$key]['imagemobil'.$locale];
        }
        else
            return $path.'/';
    }

    public function getImagenAlt($key) {
        $imagenes = $this->getImagenes();
        if(isset($imagenes[$key])){
            return $imagenes[$key]['alt'];
        }
        else
            return '';
    }

    public function getImagenLink($key) {
        $imagenes = $this->getImagenes();
        if(isset($imagenes[$key])){
            return $imagenes[$key]['link'];
        }
        else
            return '';
    }


    /**
     * Retorna la variable con settings populada la primera vez que se llama esta función, los textos se traen de la base de datos
     * @return multitype: Arreglo con los settings
     */
    private function getSettingsDB() {
        if ($this->settings == null) {
            $this->settings = $this->getResults('AppBundle:Settings');
        }
        return $this->settings;
    }


    /**
     * Obtener uno de los texto fijos internacionalizados según el local actual y el nombre del texto solicitado. Los textos se traen de la base de datos
     * @param string $key Nombre (identificador) del texto solicitado
     * @return string El setting solicitado
     */
    public function getSettingDB($key) {
        $settings = $this->getSettingsDB();
        if(isset($settings[$key]) && $settings[$key]['valor'] != '')
            return $settings[$key]['valor'];
        else
            return $key;
    }

    public function getGaleria($llave){
        $locale = $this->request_stack->getCurrentRequest()->getLocale();
        $locale = ucfirst($locale);
        $path = $this->container->getParameter('app.path.imagesgal');
        $path_carta = $this->container->getParameter('app.path.carta');
        $qb = $this->em->createQueryBuilder();
        $qb
            ->select('a.id',"concat('".$path."/',a.image".$locale.") as imagen","concat('".$path."/',a.imagemobil".$locale.") as imagenmobil",'a.alt','a.link', "concat('".$path_carta."/',a.documento".$locale.") as doc","a.titulo".$locale." as titulo","a.resumen".$locale." as resumen")
            ->from('AppBundle:Imagengaleria', 'a')
            ->leftJoin('a.galeria', 'g')
            ->where('g.llave = :llave')
            ->andWhere('a.visible = 1')
            ->setParameter('llave', $llave)
            ->orderBy('a.orden', 'asc');

        $out = array();
        $results = $qb->getQuery()->getArrayResult();
        foreach($results as $item){
            $link = $item['link'];
            if(strpos($link,'http') !== false)
                $item['target'] = '_blank';
            else{
                $item['target'] = '_self';
                if(strpos($link,'#') === false && $link != null)
                    $item['link'] = $this->container->get('router')->generate($link);
            }
            array_push($out,$item);
        }
        return $out;
    }

    public function getGaleriaFull($llave){
        $locale = $this->request_stack->getCurrentRequest()->getLocale();
        $locale = ucfirst($locale);
        $path = $this->container->getParameter('app.path.imagesgal');
        $path_carta = $this->container->getParameter('app.path.carta');
        $qb = $this->em->createQueryBuilder();
        $qb
            ->select('a.id',"concat('".$path."/',a.image".$locale.") as imagen",'a.alt','a.link', "concat('".$path_carta."/',a.documento".$locale.") as doc","a.titulo".$locale." as titulo","a.resumen".$locale." as resumen")
            ->from('AppBundle:Imagengaleria', 'a')
            ->leftJoin('a.galeria', 'g')
            ->where('g.llave = :llave')
            ->setParameter('llave', $llave)
            ->orderBy('a.orden', 'asc');

        $out = array();
        $results = $qb->getQuery()->getArrayResult();
        foreach($results as $item){
            $link = $item['link'];
            if(strpos($link,'http') !== false)
                $item['target'] = '_blank';
            else{
                $item['target'] = '_self';
                if(strpos($link,'#') === false && $link != null)
                    $item['link'] = $this->container->get('router')->generate($link);
            }
            array_push($out,$item);
        }
        return $out;
    }

    public function getAniosBlog(){
        return $this->em->getRepository("BlogiridianBundle:Post")
            ->createQueryBuilder('post')
            ->select('post.id as id, MONTH(post.fecha) as mes, YEAR(post.fecha) as anio, post.fecha as fecha')
            ->where('post.visible = 1')
            ->groupBy('anio, mes')
            ->orderBy('post.fecha', 'asc')
            ->getQuery()
            ->getResult();
    }

    public function getResultadosBlog($term,$limit = null,$year = null, $month = null){
        $locale = $this->request_stack->getCurrentRequest()->getLocale();
        $locale = ucfirst($locale);
        $path = $this->container->getParameter('app.path.blogs');
        $qb = $this->em->createQueryBuilder();
        $qb
            ->select('p.id','p.titulo'.$locale.' as titulo',"concat('".$path."/',p.image) as imagen, p.fecha"
            )
            ->from('BlogiridianBundle:Post', 'p')
            ->andWhere('p.visible = 1')
            ->groupBy('p.id')
            ->orderBy('p.fecha', 'asc');
        if($limit)
            $qb->setMaxResults($limit);
        if($year && $month){
            $qb->andWhere('MONTH(p.fecha) = '.$month)
                ->andWhere('YEAR(p.fecha) = '.$year);
        }
        if($term){
            $cad = '';
            $or = '';
            $searches= explode(' ', $term);
            foreach ($searches as $param) {
                $cad .= $or."p.tituloEs like '%".$param."%'";
                $or = ' or ';
                $cad .= $or."p.tituloEn like '%".$param."%'";
                $cad .= $or."p.tags like '%".$param."%'";
            }
            $qb->andWhere($cad);
        }

        $results = $qb->getQuery()->getArrayResult();
        return $results;
    }

    public function getById($tabla,$campo,$id){
        $out = $this->em->getRepository($tabla)->findBy(array($campo=>$id),array('orden'=>'asc'));
        return $out;
    }

    public function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text))
        {
            return 'n-a';
        }

        return $text;
    }


    public function crearPlan($id){
        $plan = $this->em->getRepository("CarroiridianBundle:Boxplan")->find($id);
        $postdata = array(
            "accountId"=> $this->getSettingDB('payu_accountId'),
            "planCode"=> "BTBbox".$plan->getId(),
            "description"=> $plan->getNombre(),
            "interval"=>"MONTH",
            "intervalCount"=> "2",
            "maxPaymentsAllowed"=>"3",
            "paymentAttemptsDelay"=> "1",
            "additionalValues"=>array([
               "name"=>"PLAN_VALUE",
                "value"=> $plan->getPrecio(),
                "currency"=> "COP"
                ],
                [
                "name"=> "PLAN_TAX",
                "value"=> number_format($plan->getImpuesto(),1,'.',''),
                "currency"=> "COP"
             ],
                [
            "name"=> "PLAN_TAX_RETURN_BASE",
            "value"=> number_format($plan->getPrecioBase(),1,'.',''),
             "currency"=> "COP"
             ]
            )
        );

        $postdata = json_encode($postdata);
        $datos_payu = $this->em->getRepository('PagosPayuBundle:DatosPayu')->find(1);
        $url = $datos_payu->getUrlapiPayu().'plans';
        return $this->sendDataPayu($postdata,$url);

    }

     public function obtenertarjeta($token){
         $datos_payu = $this->em->getRepository('PagosPayuBundle:DatosPayu')->find(1);
         $url = $datos_payu->getUrlapiPayu().'creditCards/'.$token;
         $data= $this->getdatospayu($url);
         return $data->number;
     }

     public function crearTarjetaUsuario($usuario, $tarjeta){
         $planId = $tarjeta['plan_ref'];
         $plan = $this->em->getRepository("CarroiridianBundle:Boxplan")->findOneBy(array('idPayu'=>$planId));
         unset($tarjeta['plan_ref']);
         $user = $this->em->getRepository("AppBundle:User")->find($usuario);
         if( $user->getIdPayu()!=null ) {
             $cliente_id = $user->getIdPayu();
         }else{
             $cliente = $this->crearClientePayu(array('fullName'=>$user->getNombre().' '.$user->getApellidos(),'email'=>$user->getEmail()));
             $cliente_id = $cliente->id;
             $user->setIdPayu($cliente->id);
             $this->em->persist($user);


         }
         if($tarjeta['tokentarget']){
            $veriftarjeta = $this->em->getRepository("CarroiridianBundle:Tarjeta")->find($tarjeta["tokentarget"]);
            $token=$veriftarjeta->getToken();
         }else {
             unset($tarjeta['tokentarget']);
             $card = $this->crearTarjetaPayu($tarjeta,$cliente_id);
             if(property_exists($card,'type')){
                 return $card;
             }
             if(!$card->token){
                 return $card;
             }
             $veriftarjeta = $this->em->getRepository("CarroiridianBundle:Tarjeta")->findBy(array("token" => $card->token));
             if (!$veriftarjeta) {
                 $newtarj = new Tarjeta();
                 $newtarj->setUsuario($user);
                 $newtarj->setToken($card->token);
                 $this->em->persist($newtarj);
                 $token=$card->token;
             }else{
                 $token=$veriftarjeta->getToken();
             }

         }
         $cuotas = 1;
         $suscripcion=array('clienteId'=>$cliente_id,'tokenTarjeta'=>$token,'installments'=>$cuotas,'planId'=>$planId);
         $preferencia=$this->em->getRepository("CarroiridianBundle:Preferenciasbox")->findOneBy(array("user"=>$user,"box"=>$plan));
         $suscrip=$this->crearSuscripcionPayu($suscripcion);
         $newsus= new Suscripcion();
         $newsus->setDuracion(1);
         $valida = new \DateTime();
         $valida = new \DateTime(date('Y-m-d', strtotime("+2 days",strtotime("+".$newsus->getDuracion()." months", $valida->getTimestamp()) ) ) );
         $newsus->setValidaHasta($valida);
         $newsus->setUsuario($user);
         $newsus->setPreferencia($preferencia);
         $newsus->setPayerId($cliente_id);
         $newsus->setPlanId($planId);
         $newsus->setStatus("creada_payu");
         $newsus->setIdPayu($suscrip->id);
         $newsus->setPlanSuscripcion($plan);
         $this->em->persist($newsus);
         $this->em->flush();


         $resp = $this->getPagoSuscripcionPayu($suscrip->id);
         $activa= false;
         foreach ($resp->recurringBillList as $p) {
             $pe = $this->em->getRepository("CarroiridianBundle:PagoSuscripcion")->findBy(array('idPayu' => $p->id));
             if(count($pe) == 0) {
               $pago= new PagoSuscripcion();
               $pago->setIdPayu($p->id);
               $pago->setOrderId($p->subscriptionId);
               $pago->setMonto($p->amount);
               $pago->setMoneda($p->currency);
               $fecha = new \DateTime();
               $fecha->setTimestamp($p->dateCharge);
               $pago->setFecha($fecha);
               $pago->setState($p->state);
               $pago->setSuscripcion($newsus);
                 $this->em->persist($pago);
                 $this->em->flush();
             }
             if($p->state == 'completed'  || $p->state == 'PAID' ){
                 $activa = $activa || true;
             }

         }
         if($activa){
             $newsus->setActiva($activa);
             $this->em->persist($newsus);
             $this->em->flush();
         }

         return $newsus->getId();
     }

    public function crearSuscripcionPayu($suscripcion){

        $postdata = array(
            "quantity"=> "1",
            "installments"=> $suscripcion["installments"],
            "trialDays"=> "0",
            "immediatePayment"=> true,
            "customer"=> array(
                "id"=> $suscripcion["clienteId"],
                "creditCards"=> array(
                    array(
                        "token"=> $suscripcion["tokenTarjeta"]
                    )
                )
            ),
            'notifyUrl'=>$this->getSettingDB('urlRespuestaPayu'),
            "plan"=> array(
                "planCode"=> $suscripcion["planId"]
            ));
        $postdata = json_encode($postdata);
        $datos_payu = $this->em->getRepository('PagosPayuBundle:DatosPayu')->find(1);
        $url = $datos_payu->getUrlapiPayu().'subscriptions/';
        return $this->sendDataPayu($postdata,$url);

    }


    public function crearClientePayu($cliente){
        $datos_payu = $this->em->getRepository('PagosPayuBundle:DatosPayu')->find(1);
        $postdata = json_encode($cliente);
        $url =$datos_payu->getUrlapiPayu().'customers/';
        return $this->sendDataPayu($postdata,$url);

    }


    public function crearTarjetaPayu($tarjeta,$clienteId){
        $datos_payu = $this->em->getRepository('PagosPayuBundle:DatosPayu')->find(1);
        $postdata = json_encode($tarjeta);
        $url = $datos_payu->getUrlapiPayu().'customers/'.$clienteId.'/creditCards';
        return $this->sendDataPayu($postdata,$url);

    }


    private function getdatospayu($url){
        $datos_payu = $this->em->getRepository('PagosPayuBundle:DatosPayu')->find(1);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 1);
        //curl_setopt($ch, CURLOPT_HEADER, 1);
        $additional_headers = array(
            'Accept: application/json',
            'Content-Type: application/json; charset=utf-8',
            "Authorization: Basic ".base64_encode($datos_payu->getApiLogin().":".$datos_payu->getApiKey())
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $additional_headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);

        $data = curl_exec($ch);
        $data = json_decode($data);
        $information = curl_getinfo($ch);
        $curl_errno = curl_errno($ch);
        $curl_error = curl_error($ch);
        curl_close($ch);


        return $data;
    }

    public function cancelarSuscripcion(Suscripcion $suscripcion){
        $datos_payu = $this->em->getRepository('PagosPayuBundle:DatosPayu')->find(1);
        if($suscripcion->getIdPayu()) {

            $url = $datos_payu->getUrlapiPayu() . 'subscriptions/' . $suscripcion->getIdPayu();
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_POST, 1);
            //curl_setopt($ch, CURLOPT_HEADER, 1);
            $additional_headers = array(
                'Accept: application/json',
                'Content-Type: application/json; charset=utf-8',
                "Authorization: Basic " . base64_encode($datos_payu->getApiLogin().":".$datos_payu->getApiKey())
            );
            curl_setopt($ch, CURLOPT_HTTPHEADER, $additional_headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
            curl_setopt($ch, CURLINFO_HEADER_OUT, true);


            $data = curl_exec($ch);

            $data = json_decode($data);
            $information = curl_getinfo($ch);
            curl_close($ch);
            return $data;
        }
    }


    public function getPagoSuscripcionPayu($suscripcionId){
        $datos_payu = $this->em->getRepository('PagosPayuBundle:DatosPayu')->find(1);
        $url = $datos_payu->getUrlapiPayu().'recurringBill?subscriptionId='.$suscripcionId;

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 1);
        //curl_setopt($ch, CURLOPT_HEADER, 1);
        $additional_headers = array(
            'Accept: application/json',
            'Content-Type: application/json; charset=utf-8',
            "Authorization: Basic ".base64_encode($datos_payu->getApiLogin().":".$datos_payu->getApiKey())
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $additional_headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);


        $data = curl_exec($ch);
        $data = json_decode($data);
        $information = curl_getinfo($ch);

        curl_close($ch);
        return $data;


       }



    private function sendDataPayu($postdata,$url){
        $datos_payu = $this->em->getRepository('PagosPayuBundle:DatosPayu')->find(1);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 1);
        //curl_setopt($ch, CURLOPT_HEADER, 1);
        $additional_headers = array(
            'Accept: application/json',
            'Content-Type: application/json; charset=utf-8',
            "Authorization: Basic ".base64_encode($datos_payu->getApiLogin().":".$datos_payu->getApiKey())
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $additional_headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS,$postdata );
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);

        $data = curl_exec($ch);
        $data = json_decode($data);
        $information = curl_getinfo($ch);
        $curl_errno = curl_errno($ch);
        $curl_error = curl_error($ch);
        curl_close($ch);


        return $data;
    }



}
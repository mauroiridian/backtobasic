<?php
/**
 * Created by PhpStorm.
 * User: Iridian 4
 * Date: 26/07/2016
 * Time: 11:43 AM
 */

namespace AppBundle\Form\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //->add("matters")
            ->add('matters', EntityType::class, array(
                'required' => true,
                'class' => 'AppBundle\Entity\Matter',
                'choice_label' => 'nombre'.ucfirst($options["locale"]),
                'query_builder' => function (\AppBundle\Repository\MatterRepository $er) use ($options) {
                    return $er->createQueryBuilder('s')
                        ->orderBy("s.nombre".ucfirst($options["locale"]), "ASC");
                    //return $er->createQueryBuilder('d')
                      //  ->orderBy('d.id', 'ASC');
                }
            ))
            ->add('nombre')
            ->add('apellido')
            ->add('email', EmailType::class)
            ->add('ciudad')
            ->add('pais')
            ->add('mensaje');
        /*
         *
         * $builder->add("profileType",
"entity",
array( "class" => "NamespaceWebsiteBundle:ProfileType",
"query_builder" => function(EntityRepository $er) use ( $options ){
return $er->createQueryBuilder('s')
->orderBy("s.name_".$options["locale"], "ASC");
},
"property" => "name_".$options["locale"],
"error_bubbling" => true,
"label" => "label.account_type",
"translation_domain" => "profileform",
"invalid_message" => "userprofiletype.invalid",
"required" => false));
         */
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\ContactList',
            'locale' => 'en'
        ));
    }
}
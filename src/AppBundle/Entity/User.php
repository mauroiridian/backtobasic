<?php
namespace AppBundle\Entity;

use CarroiridianBundle\Entity\Compra;
use CarroiridianBundle\Entity\Descuento;
use CarroiridianBundle\Entity\Envio;
use CarroiridianBundle\Entity\Producto;
use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255, nullable=true)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="apellidos", type="string", length=255, nullable=true)
     */
    private $apellidos;

    /**
     * @var string
     *
     * @ORM\Column(name="telefono", type="string", length=255, nullable=true)
     */
    private $telefono;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="date")
     */
    private $fecha;

    /**
     * @var Compra[]
     *
     * @ORM\OneToMany(targetEntity="CarroiridianBundle\Entity\Compra", mappedBy="comprador", cascade={"remove"})
     */
    private $compras;

    /**
     * @var Envio[]
     *
     * @ORM\OneToMany(targetEntity="CarroiridianBundle\Entity\Envio", mappedBy="user", cascade={"remove"})
     */
    private $direcciones;
    
    /**
     * @var Descuento[]
     * @ORM\ManyToMany(targetEntity="CarroiridianBundle\Entity\Descuento", inversedBy="redimidopor")
     * @ORM\JoinTable(name="descuento_rel")
     */
    private $descuento;

    /**
     * @var string
     *
     * @ORM\Column(name="id_payu", type="string", length=255, nullable=true)
     */
    private $idpayu;

    /**
     * @ORM\OneToMany(targetEntity="CarroiridianBundle\Entity\Tarjeta", mappedBy="usuario")
     */
    private $tarjetas;




    public function __construct()
    {
        parent::__construct();
        $this->direcciones = new ArrayCollection();
        $this->compras = new ArrayCollection();
        $this->favoritos = new ArrayCollection();
    }

    public function __toString()
    {
        if(is_numeric($this->getId()))
            return $this->getId().' - ' . $this->getUsername() . " - " . $this->getEmail();
        return ' ';
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return User
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellidos
     *
     * @param string $apellidos
     *
     * @return User
     */
    public function setApellidos($apellidos)
    {
        $this->apellidos = $apellidos;
    
        return $this;
    }

    /**
     * Get apellidos
     *
     * @return string
     */
    public function getApellidos()
    {
        return $this->apellidos;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     *
     * @return User
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    
        return $this;
    }

    /**
     * Get telefono
     *
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Add compra
     *
     * @param \CarroiridianBundle\Entity\Compra $compra
     *
     * @return User
     */
    public function addCompra(\CarroiridianBundle\Entity\Compra $compra)
    {
        $this->compras[] = $compra;
    
        return $this;
    }

    /**
     * Remove compra
     *
     * @param \CarroiridianBundle\Entity\Compra $compra
     */
    public function removeCompra(\CarroiridianBundle\Entity\Compra $compra)
    {
        $this->compras->removeElement($compra);
    }

    /**
     * Get compras
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCompras()
    {
        return $this->compras;
    }

    /**
     * Add direccione
     *
     * @param \CarroiridianBundle\Entity\Envio $direccione
     *
     * @return User
     */
    public function addDireccione(\CarroiridianBundle\Entity\Envio $direccione)
    {
        $this->direcciones[] = $direccione;
    
        return $this;
    }

    /**
     * Remove direccione
     *
     * @param \CarroiridianBundle\Entity\Envio $direccione
     */
    public function removeDireccione(\CarroiridianBundle\Entity\Envio $direccione)
    {
        $this->direcciones->removeElement($direccione);
    }

    /**
     * Get direcciones
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDirecciones()
    {
        return $this->direcciones;
    }

    /**
     * Add favorito
     *
     * @param \CarroiridianBundle\Entity\Producto $favorito
     *
     * @return User
     */
    public function addFavorito(\CarroiridianBundle\Entity\Producto $favorito)
    {
        $this->favoritos[] = $favorito;
    
        return $this;
    }

    /**
     * Remove favorito
     *
     * @param \CarroiridianBundle\Entity\Producto $favorito
     */
    public function removeFavorito(\CarroiridianBundle\Entity\Producto $favorito)
    {
        $this->favoritos->removeElement($favorito);
    }

    /**
     * Get favoritos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFavoritos()
    {
        return $this->favoritos;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return User
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set descuento
     *
     * @param \CarroiridianBundle\Entity\Descuento $descuento
     *
     * @return User
     */
    public function setDescuento(\CarroiridianBundle\Entity\Descuento $descuento = null)
    {
        $this->descuento = $descuento;

        return $this;
    }

    /**
     * Get descuento
     *
     * @return \CarroiridianBundle\Entity\Descuento
     */
    public function getDescuento()
    {
        return $this->descuento;
    }

    /**
     * Add descuento
     *
     * @param \CarroiridianBundle\Entity\Descuento $descuento
     *
     * @return User
     */
    public function addDescuento(\CarroiridianBundle\Entity\Descuento $descuento)
    {
        $this->descuento[] = $descuento;

        return $this;
    }

    /**
     * Remove descuento
     *
     * @param \CarroiridianBundle\Entity\Descuento $descuento
     */
    public function removeDescuento(\CarroiridianBundle\Entity\Descuento $descuento)
    {
        $this->descuento->removeElement($descuento);
    }

    /**
     * Set idpayu
     *
     * @param string $idpayu
     *
     * @return User
     */
    public function setIdpayu($idpayu)
    {
        $this->idpayu = $idpayu;

        return $this;
    }

    /**
     * Get idpayu
     *
     * @return string
     */
    public function getIdpayu()
    {
        return $this->idpayu;
    }

    /**
     * Add tarjeta
     *
     * @param \CarroiridianBundle\Entity\Tarjeta $tarjeta
     *
     * @return User
     */
    public function addTarjeta(\CarroiridianBundle\Entity\Tarjeta $tarjeta)
    {
        $this->tarjetas[] = $tarjeta;

        return $this;
    }

    /**
     * Remove tarjeta
     *
     * @param \CarroiridianBundle\Entity\Tarjeta $tarjeta
     */
    public function removeTarjeta(\CarroiridianBundle\Entity\Tarjeta $tarjeta)
    {
        $this->tarjetas->removeElement($tarjeta);
    }

    /**
     * Get tarjetas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTarjetas()
    {
        return $this->tarjetas;
    }
}

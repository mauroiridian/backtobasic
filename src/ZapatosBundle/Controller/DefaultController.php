<?php

namespace ZapatosBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/zapatos", name="zapatos")
     */
    public function indexAction()
    {
        return $this->render('ZapatosBundle:Default:zapatos.html.twig', array('productos'=>array()));
    }
    /**
     * @Route("/productzapato/{id}/{nombre}", name="productzapato")
     */
    public function productAction($id)
    {
        $path = $this->container->getParameter('app.path.productos');
        $producto = $this->getDoctrine()->getRepository('CarroiridianBundle:Producto')->find($id);
        $imagenes = array();
        array_push($imagenes,$path.'/'.$producto->getImagen());
        foreach ($producto->getGalerias() as $galeria){
            array_push($imagenes,$path.'/'.$galeria->getImagen());
        }
        return $this->render('ZapatosBundle:Default:product.html.twig',array('producto'=>$producto,'imagenes'=>$imagenes));
    }

    /**
     * @Route("/buscador", name="buscador")
     */
    public function buscadorAction(Request $request)
    {
        $ci = $this->get('ci');
        $productos = $ci->getResultadosProductos($request->get('q'));
        return $this->render('ZapatosBundle:Default:zapatos.html.twig', array('productos'=>$productos));
    }

    /**
     * @Route("/masonry", name="masonry")
     */
    public function masonryAction(Request $request)
    {
        return $this->render('ZapatosBundle:Default:masonry.html.twig');
    }
}

<?php

namespace DistribuidoresBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/distribuidores", name="distri")
     */
    public function distriAction()
    {
        return $this->render('DistribuidoresBundle:Default:distribuidores.html.twig');
    }
}

<?php

namespace BaysideBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/bayside", name="bayside")
     */
    public function baysideAction()
    {
        return $this->render('BaysideBundle:Default:bayside.html.twig');
    }
    /**
     * @Route("/the_blog_old", name="the_blog_old")
     */
    public function the_blogAction()
    {
        return $this->render('BaysideBundle:Default:the_blog.html.twig');
    }
    /**
     * @Route("/our_process", name="our_process")
     */
    public function our_processAction()
    {
        return $this->render('BaysideBundle:Default:our_process.html.twig');
    }
    /**
     * @Route("/sustainable", name="sustainable")
     */
    public function sustainableAction()
    {
        return $this->render('BaysideBundle:Default:sustainable.html.twig');
    }
}

<?php

namespace HomeBundle\Controller;

use HomeBundle\Entity\Adicional;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use HomeBundle\Entity\Encuesta;
use HomeBundle\Form\Type\EncuestaType;
use HomeBundle\Form\Type\AdicionalType;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        $categorias= $this->getDoctrine()->getRepository('CarroiridianBundle:Categoria')->findBy(array('visible'=>true),array('orden'=>'asc'));
        return $this->render('HomeBundle:Default:index.html.twig', array('categorias'=>$categorias));
    }

    /**
     * @Route("/modulos", name="modulos")
     */
    public function modulosAction()
    {
        return $this->render('HomeBundle:Default:modulos.html.twig');
    }

    /**
     * @Route("/log", name="log")
     */
    public function logAction()
    {
        return $this->render('HomeBundle:Default:log.html.twig');
    }

    /**
     * @Route("/registry", name="registry")
     */
    public function registryAction()
    {
        return $this->render('HomeBundle:Default:registry.html.twig');
    }

    /**
     * @Route("/terms", name="terms")
     */
    public function termsAction()
    {
        return $this->render('HomeBundle:Default:terms.html.twig');
    }

    /**
     * @Route("/privacy_poli", name="privacy_poli")
     */
    public function privacy_poliAction()
    {
        return $this->render('HomeBundle:Default:privacy_poli.html.twig');
    }

    /**
     * @Route("/customer_service", name="customer_service")
     */
    public function customer_serviceAction()
    {
        return $this->render('HomeBundle:Default:customer_service.html.twig');
    }



}

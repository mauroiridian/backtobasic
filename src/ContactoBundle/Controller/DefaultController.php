<?php

namespace ContactoBundle\Controller;

use AppBundle\Entity\Contact;
use AppBundle\Entity\ContactList;
use AppBundle\Form\Type\ContactType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/contacto", name="contacto")
     */
    public function contactoAction(Request $request)
    {
        $contacto = new ContactList();
        $lc = $this->get('translator')->getLocale();
        $form = $this->createForm(ContactType::class, $contacto, array('locale' => $request->getLocale()));
        $form->handleRequest($request);
        if ($form->isValid()) {
            $data = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($contacto);
            $em->flush();
            //ENVIAR MENSAJE
            $sender  = $this->getDoctrine()->getRepository('AppBundle:Settings')->findBy(array('llave'=>"sender_mail"));
            $datos_envio  = $this->getDoctrine()->getRepository('AppBundle:Mailing')->findBy(["llave" => "contact"]);
            $this->SendMail($data->getMatters()->getLocalName($lc), $sender[0]->getValor(), $data->getMatters()->getDestinatario(), array("data" => $datos_envio[0], "email" => $contacto->getEmail(), "msj" => $contacto->getMensaje()), "AppBundle::contact_email.html.twig");
            $this->SendMail($datos_envio[0]->getAsunto()->getLocalName($lc), $sender[0]->getValor(), $contacto->getEmail(), array("data" => $datos_envio[0]), "AppBundle::gracias_email.html.twig");
            //\Doctrine\Common\Util\Debug::dump($data->getMatters()->getLocalName($lc));
            //return "ok";
            //return $this->render('AppBundle::contact_email.html.twig', array("data" => $datos_envio[0]));
            return $this->redirectToRoute('gracias');
        }
        $data  = $this->getDoctrine()->getRepository('AppBundle:Contact')->findBy(["llave" => "contact_info"]);
        return $this->render('ContactoBundle:Default:contacto.html.twig', array("data" => $data[0], "form" => $form->createView()));
    }

    public function SendMail($subject, $from, $to, $custom, $template){
        $message = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom($from)
            ->setTo($to)
            ->setBody(
                $this->renderView(
                    $template,
                    $custom
                ),
                'text/html'
            );
        $this->get('mailer')->send($message);
    }


    /**
     * @Route("/message", name="message")
     */
    public function messageAction()
    {
        return $this->render('ContactoBundle:Default:message.html.twig');
    }
}

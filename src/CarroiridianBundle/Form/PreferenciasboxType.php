<?php

namespace CarroiridianBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PreferenciasboxType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('colorcamiseta')
            ->add('productotbt')
            ->add('colorpantalon')
            ->add('tallacamiseta', EntityType::class, array(
                'required' => true,
                'class' => 'CarroiridianBundle\Entity\Talla',
                'query_builder' => function (\CarroiridianBundle\Repository\TallaRepository $er) use ($options) {
                    return $er->createQueryBuilder('s')
                        ->where("s.id <> 5")
                        ->orderBy("s.id", "ASC");
                }
            ))
            ->add('tallapantalon')
            ->add('tallazapatos')
            ->add('genero', EntityType::class, array(
                'required' => true,
                'class' => 'CarroiridianBundle\Entity\Genero',
                'query_builder' => function (\CarroiridianBundle\Repository\GeneroRepository $er) use ($options) {
                    return $er->createQueryBuilder('g')
                        ->where("g.id <> 3")
                        ->orderBy("g.id", "ASC");
                }
            ));
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CarroiridianBundle\Entity\Preferenciasbox'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'carroiridianbundle_preferenciasbox';
    }


}

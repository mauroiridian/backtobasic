<?php

namespace CarroiridianBundle\Controller;

use AppBundle\Entity\User;
use CarroiridianBundle\Entity\Envio;
use CarroiridianBundle\Entity\Bono;
use CarroiridianBundle\Entity\Preferenciasbox;
use CarroiridianBundle\Form\PreferenciasboxType;
use CarroiridianBundle\Form\Type\BonoType;
use CarroiridianBundle\Form\Type\EnvioType;
use PhpParser\Node\Expr;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{


    /**
     * @Route("/carrito-de-compras", name="carrito")
     */
    public function indexAction(Request $request)
    {
        $session = new Session();
        $carrito = $session->get('carrito', array());
        $bonos = $session->get('bonos', array());
        $descuento = $session->get('descuento', array());
        $descuento_porc = $session->get('descuento_porc', array());
        $tipo_pago = $session->get('tipo_pago', null);
        $direccion_id = $session->get('direccion_id', null);
        $ver_save_dir  = $this->getDoctrine()->getRepository('AppBundle:Settings')->findBy(["llave" => "dir_save_hidden"]);
        if(isset($ver_save_dir[0])){
            $ver_save_dir = ($ver_save_dir[0]->getValor() == "1") ? true:false;
        }else{
            $ver_save_dir = true;
        }
        $direccion = null;
        if($direccion_id != null){
            $direccion = $this->getDoctrine()->getRepository('CarroiridianBundle:Envio')->find($direccion_id);
        }
        //$session->set('descuento_porc', array());

        if($request->get("discount") && count($descuento) == 0){
            $bono = $this->getDoctrine()->getRepository('CarroiridianBundle:Bono')->findBy(array('codigo'=>$request->get("discount"), "reclama" => 0));
            if(count($bono) > 0){
                $valor = $bono[0]->getValorbono()->getValor() - $bono[0]->getReclama();
                $session->set('descuento', array("id" => $bono[0]->getId() , "codigo" => $bono[0]->getCodigo(), "de" => $bono[0]->getDe(), "valor" => $valor));
            }
        }
        if($request->get("discount_porc") && count($descuento_porc) == 0){
            $porcentaje = $this->getDoctrine()->getRepository('CarroiridianBundle:Descuento')->createQueryBuilder("d")
                ->leftJoin("d.redimidopor", "rp")
                ->leftJoin("d.solopara", "sp")
                ->andWhere("d.codigo = '" . $request->get("discount_porc") . "'")
                ->andWhere("d.fecha >= '" . date("Y-m-d") . "'")
                ->addSelect("rp")
                ->addSelect("sp")
                ->getQuery()
                ->getOneOrNullResult();
            //exit(dump($porcentaje->getRedimidoPor()->contains($this->getUser())));
            if(count($porcentaje) != null) {
                if($porcentaje->getSoloPara() != null && $porcentaje->getRedimidoPor()->contains($this->getUser()) == false){
                    if($porcentaje->getSoloPara()->getId() == $this->getUser()->getId()){
                        $session->set('descuento_porc', array("id" => $porcentaje->getId() , "codigo" => $porcentaje->getCodigo(), "valor" => $porcentaje->getPorcentaje()));
                    }
                }else{
                    if($porcentaje->getRedimidoPor()->contains($this->getUser()) == false){
                        $session->set('descuento_porc', array("id" => $porcentaje->getId() , "codigo" => $porcentaje->getCodigo(), "valor" => $porcentaje->getPorcentaje()));
                    }
                }
            }
        }
        $descuento = $session->get('descuento', array());
        $descuento_porc = $session->get('descuento_porc', array());
        $securityContext = $this->container->get('security.authorization_checker');
        if (!$securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirectToRoute('registro_login');
        }
        $user = $this->getUser();
        $direcciones = $user->getDirecciones();
        $envio = new Envio();
        if($request->isMethod('GET')) {
            $departamento = $this->getDoctrine()->getRepository('CarroiridianBundle:Departamento')->find(2);
            $envio->setUser($user);
            $envio->setDepartamento($departamento);
        }
        $form = $this->createForm(EnvioType::class, $envio);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $envio = $form->getData();
	        $envio->setUser($user);
            $em = $this->getDoctrine()->getManager();
            $em->persist($envio);
            $em->flush();

            $session = new Session();
            $session->set('direccion_id', $envio->getId());

            return $this->redirectToRoute('pagar_payu');
        }
        //exit(dump($ver_save_dir));
        return $this->render('CarroiridianBundle:Default:index.html.twig', array('carrito'=>$carrito,'bonos'=>$bonos, "descuento" => $descuento, "direcciones" => $direcciones, "descuento_porc" => $descuento_porc, "form" => $form->createView(), "tipo_pago" => $tipo_pago, "direccion" => $direccion, "show_dir" => $ver_save_dir));
    }
    /**
     * @Route("/the_store", name="home")
     */
    public function homeAction()
    {
        return $this->render('CarroiridianBundle:Default:home.html.twig');
    }
    /**
     * @Route("/datos", name="datos")
     */
    public function datosAction()
    {
        return $this->render('CarroiridianBundle:Default:datos.html.twig');
    }

    /**
     * @Route("/direccion", name="direccion")
     */
    public function direccionAction(Request $request)
    {
        return $this->redirectToRoute('homepage');
        $securityContext = $this->container->get('security.authorization_checker');
        if (!$securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $url = $this->generateUrl('homepage');
            $response = new RedirectResponse($url);
            return $response;
        }
        $user = $this->getUser();
        $direcciones = $user->getDirecciones();
        $envio = new Envio();
        if($request->isMethod('GET')) {
            $departamento = $this->getDoctrine()->getRepository('CarroiridianBundle:Departamento')->find(2);
            $envio->setUser($user);
            $envio->setDepartamento($departamento);
        }
        $form = $this->createForm(EnvioType::class, $envio);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $envio = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($envio);
            $em->flush();

            $session = new Session();
            $session->set('direccion_id', $envio->getId());

            return $this->redirectToRoute('pagar_payu');
        }
        return $this->render('CarroiridianBundle:Default:direccion.html.twig', array('form'=>$form->createView(),'direcciones'=>$direcciones));
    }

    /**
     * @Route("/direccion-sesion/{id}", name="direccion_sesion")
     */
    public function direccionSesionAction(Request $request,$id)
    {
        $envio = $this->getDoctrine()->getRepository('CarroiridianBundle:Envio')->find($id);
        $session = new Session();
        $session->set('direccion_id', $envio->getId());
        return $this->redirectToRoute('carrito');
    }

    /**
     * @Route("/set-tipo-pago/{value}", name="set_tipo_pago")
     */
    public function setTipoPagoAction($value)
    {
        $session = new Session();
        $session->set('tipo_pago', $value);
        return new JsonResponse("1");
    }

    /**
     * @Route("/mensaje", name="mensaje")
     */
    public function mensajeAction()
    {
        return $this->render('CarroiridianBundle:Default:mensaje.html.twig');
    }

    /**
     * @Route("/add-carrito/{id}/{cant}", name="add_carrito")
     */
    public function addCarritoAction($id,$cant)
    {
        $session = new Session();

        $carrito = $session->get('carrito', array());

        $repo = $this->getDoctrine()->getRepository('CarritoBundle:Producto');
        $producto = $repo->find($id);

        try {
            if(array_key_exists($producto->getId(),$carrito))
                $prod_carrito = $carrito[$producto->getId()]["cantidad"];
            else
                $prod_carrito = 0;
        } catch (Exception $e) {
            $prod_carrito = 0;
        }

        if(!is_numeric($prod_carrito))
            $prod_carrito = 0;
        $prod_carrito += $cant;
        if($prod_carrito < 0)
            $prod_carrito = 0;
        $carrito[$producto->getId()] = array("cantidad"=>$prod_carrito,"nombre"=>$producto->getNombreEs(),"precio"=>$producto->getPrecio());
        if($prod_carrito < 1){
            unset($carrito[$producto->getId()]);
        }


        $session->set('carrito',$carrito);

        return new JsonResponse(array("cantidad"=>$prod_carrito));
    }

    /**
     * @Route("/add-carrito-talla/{id}/{cant}/{id_talla}", name="add_carrito_talla")
     */
    public function addCarritoTallaAction($id,$cant,$id_talla)
    {
        $session = new Session();

        $carrito = $session->get('carrito', array());

        $repo = $this->getDoctrine()->getRepository('CarroiridianBundle:Producto');
        $producto = $repo->find($id);

        try {
            if(array_key_exists($producto->getId(),$carrito))
                if(array_key_exists($id_talla,$carrito[$producto->getId()]))
                    $prod_carrito = $carrito[$producto->getId()][$id_talla]["cantidad"];
                else
                    $prod_carrito = 0;
            else
                $prod_carrito = 0;
        } catch (Exception $e) {
            $prod_carrito = 0;
        }

        if(!is_numeric($prod_carrito))
            $prod_carrito = 0;
        $prod_carrito += $cant;
        if($prod_carrito < 0)
            $prod_carrito = 0;
        $carrito[$producto->getId()][$id_talla] = array("cantidad"=>$prod_carrito,"nombre"=>$producto->getNombreEs(),"precio"=>$producto->getPrecio());
        if($prod_carrito < 1){
            unset($carrito[$producto->getId()][$id_talla]);
        }


        $session->set('carrito',$carrito);

        return new JsonResponse(array("cantidad"=>$prod_carrito));
    }

    /**
     * @Route("/remove-carrito-bono/{id}", name="remove_carrito_bono")
     */
    public function removeCarritoBonoAction($id)
    {
        $session = new Session();

        $bonos = $session->get('bonos', array());
        if(array_key_exists($id,$bonos)){
            unset($bonos[$id]);
        }
        $session->set('bonos',$bonos);

        return new JsonResponse(array("cantidad"=>0));
    }


    /**
     * @Route("/set-carrito-talla/{id}/{cant}/{id_talla}", name="set_carrito_talla")
     */
    public function setCarritoTallaAction($id,$cant,$id_talla)
    {
        $session = new Session();

        $carrito = $session->get('carrito', array());

        $repo = $this->getDoctrine()->getRepository('CarroiridianBundle:Producto');
        $producto = $repo->find($id);

        try {
            if(array_key_exists($producto->getId(),$carrito))
                if(array_key_exists($id_talla,$carrito[$producto->getId()]))
                    $prod_carrito = $carrito[$producto->getId()][$id_talla]["cantidad"];
                else
                    $prod_carrito = 0;
            else
                $prod_carrito = 0;
        } catch (Exception $e) {
            $prod_carrito = 0;
        }

        if(!is_numeric($prod_carrito))
            $prod_carrito = 0;
        $prod_carrito = $cant;
        if($prod_carrito < 0)
            $prod_carrito = 0;
        $carrito[$producto->getId()][$id_talla] = array("cantidad"=>$prod_carrito,"nombre"=>$producto->getNombreEs(),"precio"=>$producto->getPrecio());
        if($prod_carrito < 1){
            unset($carrito[$producto->getId()][$id_talla]);
        }


        $session->set('carrito',$carrito);

        return new JsonResponse(array("cantidad"=>$prod_carrito));
    }

    /**
     * @Route("/ciudades-dept/{id}", name="ciudadesByDept")
     */
    public function ciudadesByDeptAction($id)
    {
        $ciudades = $this->getDoctrine()->getRepository('CarroiridianBundle:Ciudad')->findBy(array('departamento' => $id, "visible" => true), array('nombre' => 'asc'));
        return $this->render('CarroiridianBundle:Default:ciudades.html.twig', array('ciudades' => $ciudades));
    }

        /**
         * @Route("/tallas-genero-box/{id}/{tipo}", name="tallasporgenerobox")
         */
        public function TallasporgeneroBoxAction($id,$tipo)
    {

        $tallas = $this->getDoctrine()->getRepository('CarroiridianBundle:Tallaproducto')->findBy(array('genero'=>$id,'tipo'=>$tipo,"visible"=>true),array('nombreEs'=>'asc'));
        return $this->render('CarroiridianBundle:Default:tallas.html.twig', array('tallas'=>$tallas));
    }

    /**
     * @Route("/productos", name="productos")
     */
    public function productosAction()
    {
        return $this->render('CarroiridianBundle:Default:productos.html.twig', array('productos'=>array()));
    }

    /**
     * @Route("/mis-planes", name="mis-planes")
     */
    public function planexBoxAction()
    {
        $securityContext = $this->container->get('security.authorization_checker');
        if (!$securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirectToRoute('registro_login');
        }

       $path = $this->container->getParameter('app.path.productos');
       $boxes=$this->getDoctrine()->getRepository("CarroiridianBundle:Boxplan")->findBy(array("visible"=>true));
       $planes=[];
       foreach ($boxes as $box){
         $suscripcion = $this->getDoctrine()->getRepository('CarroiridianBundle:Suscripcion')->findOneBy(array('usuario'=>$this->getUser(),'plan_suscripcion'=>$box), array('activa'=>'desc','id'=>'desc'));
            if($suscripcion) {
                $planes[$suscripcion->getId()]["nombre"] =$box->getNombre();
                $planes[$suscripcion->getId()]["idplan"] =$box->getId();
                $planes[$suscripcion->getId()]["precio"] =$box->getPrecio();
                $planes[$suscripcion->getId()]["imagen"]= $path."/".$box->getImagen();
                $planes[$suscripcion->getId()]["fecha"] =$suscripcion->getCreatedat();
                $planes[$suscripcion->getId()]["estado"]=$this->checkSuscripcion($box->getId());
                $planes[$suscripcion->getId()]["hasta"]=$suscripcion->getValidaHasta();
                $planes[$suscripcion->getId()]["direccion"]=$suscripcion->getPreferencia()->getDireccionusuario();
            }
       }

        return $this->render('CarroiridianBundle:Default:misplanes.html.twig', array('productos'=>$planes));
    }

    /**
     * @Route("/make-cancelar/{id}", name="make-cancelar")
     */
    public function makecancelacionAction($id){
        $qi=$this->get('qi');
        if($this->getUser()) {
            $box=$this->getDoctrine()->getRepository("CarroiridianBundle:Boxplan")->find($id);
            $suscripcion = $this->getDoctrine()->getRepository('CarroiridianBundle:Suscripcion')->findOneBy(array('usuario'=>$this->getUser(), 'plan_suscripcion'=>$box), array('id'=>'desc'));
            $qi->cancelarSuscripcion($suscripcion);

        }
        print_r($qi->cancelarSuscripcion($suscripcion));
        return $this->redirectToRoute("mis-planes");
    }

    /**
     * @Route("/cancelar-suscripcion/{id}", name="cancelar-suscripcion")
     */
    public function cancelarSuscripcionAction($id)
    {
        $box=$this->getDoctrine()->getRepository("CarroiridianBundle:Boxplan")->find($id);
        return $this->render('CarroiridianBundle:Default:cancelacion.html.twig', array('producto'=>$box));
    }


    /**
     * @Route("/wishlist", name="wishlist")
     */
    public function wishlistAction()
    {
        $session = new Session();
        $productos = $session->get('wish', array());
        return $this->render('CarroiridianBundle:Default:wishlist.html.twig', array('productos'=>$productos));
    }

    /**
     * @Route("/add-wish/{id}", name="add_wish")
     */
    public function wishAction($id)
    {
        $session = new Session();
        $productos = $session->get('wish', array());
        $ci = $this->get('ci');
        $producto = $ci->getProductoById($id);
        if($producto != null){
            $productos[$id] = $producto[0];
            $session->set("wish", $productos);
        }
        return $this->redirectToRoute('wishlist');
    }


    /**
     * @Route("/productos_por_categoria/{categoria}/{nombre}", name="productos_por_categoria")
     */
    public function productosCategoriaAction($categoria)
    {
        $ci = $this->get('ci');
        $productos = $ci->getProductos($categoria);
        $planesbox=$this->getDoctrine()->getRepository("CarroiridianBundle:Boxplan")->findBy(array("visible"=>true),array("id"=>"asc"));
        return $this->render('CarroiridianBundle:Default:productos.html.twig', array('productos'=>$productos,'categoria'=>$categoria,'planes'=>$planesbox));
    }

    /**
     * @Route("/productos_por_genero/{genero}", name="productos_por_genero")
     */
    public function productosGenAction($genero)
    {
        $ci = $this->get('ci');
        $productos = $ci->getProductos(null, $genero);

        return $this->render('CarroiridianBundle:Default:productos.html.twig', array('productos'=>$productos, 'genero' => $genero));
    }

    /**
     * @Route("/productos_por_gen_cat/{genero}/{categoria}", name="productos_por_gen_cat")
     */
    public function productosCatGenAction($genero, $categoria)
    {
        $ci = $this->get('ci');
        $productos = $ci->getProductos($categoria, $genero);
        $planesbox=$this->getDoctrine()->getRepository("CarroiridianBundle:Boxplan")->findBy(array("visible"=>true),array("id"=>"asc"));

        return $this->render('CarroiridianBundle:Default:productos.html.twig', array('productos'=>$productos, 'categoria'=>$categoria,'genero' => $genero, 'planes'=>$planesbox));
    }


    public function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * @Route("/boxplan/{id}/{nombre}", name="boxplan")
     */
    public function BoxAction($id, $nombre, Request $request, Session $session)
    {

        $box = $this->getDoctrine()->getRepository('CarroiridianBundle:Boxplan')->find($id);
        $user=$this->getUser();
        if(!$user) {
            return $this->render('CarroiridianBundle:Default:box.html.twig',array('producto'=>$box));

        }


        $direccion="";
        $preferenciauser=$this->getDoctrine()->getRepository('CarroiridianBundle:Preferenciasbox')->findOneBy(array("user"=>$user,"box"=>$id),array('id'=>'desc'));
        if($preferenciauser){
            $preferencia=$preferenciauser;
           $direccion= $preferencia->getDireccion();
        }else {
            $preferencia = new Preferenciasbox();
        }
        $direcciones=$user->getDirecciones();
        $departamentos = $this->getDoctrine()->getRepository('CarroiridianBundle:Departamento')->findBy(array(),array("nombre"=>"asc"));
        $form = $this->createForm(PreferenciasboxType::class, $preferencia);
        $form->handleRequest($request);
        $estado = $this->checkSuscripcion($id);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $preferencia = $form->getData();
            $preferencia->setUser($user);
            $plan=$this->getDoctrine()->getRepository('CarroiridianBundle:Boxplan')->find($id);
            $preferencia->setBox($plan);
            $direccionid=$request->get("diruser");
            if($direccionid){
                $direccion=$this->getDoctrine()->getRepository('CarroiridianBundle:Envio')->find($direccionid);

            }else{
                $direccion =new Envio();
                $direccion->setUser($user);
                $departamentoid=$request->get("departamento");
                $ciudadid=$request->get("ciudad");
                $departamento=$this->getDoctrine()->getRepository('CarroiridianBundle:Departamento')->find($departamentoid);
                $ciudad=$this->getDoctrine()->getRepository('CarroiridianBundle:Ciudad')->find($ciudadid);
                $direccion->setDireccion($request->get("direccion"));
                $direccion->setDepartamento($departamento);
                $direccion->setCiudad($ciudad);
                $direccion->setAdicionales($request->get("adicionales"));
                $em->persist($direccion);
                $em->flush();
            }
            $preferencia->setDireccion($direccion);
            $em->persist($preferencia);
            $em->flush();
            return $this->redirectToRoute("pagobox", array("id"=> $plan->getId()));

        }

        return $this->render('CarroiridianBundle:Default:box.html.twig',array('producto'=>$box, "form"=>$form->createView(), "direcciones"=>$direcciones, "departamentos"=>$departamentos,"diruser"=>$direccion,"estado"=>$estado));
    }

    /**
     * @Route("/pago-plan-box/{id}", name="pagobox")
     */
    public function PagoplanBox(Request $request,$id)
    {
        $securityContext = $this->container->get('security.authorization_checker');
        if (!$securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirectToRoute('registro_login');
        }
        $plan="";
        $qi=$this->get('qi');
        $em = $this->getDoctrine()->getManager();
        if($id) {
            $plan = $this->getDoctrine()->getRepository('CarroiridianBundle:Boxplan')->find($id);
            if ($plan->getIdPayu() == null) {
                $data = $qi->crearPlan($plan->getId());
                if (property_exists($data, 'id')) {
                    $plan->setIdPayu($data->planCode);
                    $em->persist($plan);
                    $em->flush();
                }
            }
        }

        $tarjetas=$this->getDoctrine()->getRepository("CarroiridianBundle:Tarjeta")->findBy(array("usuario"=>$this->getUser()));
        $estado = $this->checkSuscripcion($id);


        if($request->isMethod('POST')) {
            $data = $request->request->all();
            $data['number']=str_replace("-","",$data['number']);

            $resp = $qi->crearTarjetaUsuario($this->getUser()->getId(),$data);
            $error = '';
            if(!is_numeric($resp)){
                $desc = $resp->description;
                if($this->startsWith($desc, 'number:')){
                    $error = 'Lo sentimos, hay un error con el número de la tarjeta';
                    //dd($error);
                }else{
                    if($desc == 'The credit card expiration date is not valid'){
                        $error = 'Lo sentimos, la fecha de expiración es inválida';
                    }else{
                        $error = $desc;
                    }
                }
             }
            if($error == ''){
                return $this->redirectToRoute('mis-planes');
            }else{
                return $this->render('CarroiridianBundle:Default:pagoplan.html.twig', [
                    "plan"=>$plan,
                    'datos' => $data,
                    'error' => $error,
                    "tarjetas"=>$tarjetas,
                    'estado'=>''
                    ]);
            }
        }

            $data=array("name"=> "",
                "document"=> "",
                "number"=> "",
                "expMonth"=> "",
                "expYear"=> "",
                "type"=> "",
                "address"=> array(
                    "line1"=> "",
                    "line2"=> "",
                    "line3"=> "",
                    "postalCode"=> "",
                    "city"=> "",
                    "state"=> "",
                    "country"=> "",
                    "phone"=> ""
                ));



       return $this->render('CarroiridianBundle:Default:pagoplan.html.twig',array("plan"=>$plan,"datos"=>$data,"error"=>"", "estado"=>$estado,"tarjetas"=>$tarjetas));
    }

    function startsWith ($string, $startString)
    {
        $len = strlen($startString);
        return (substr($string, 0, $len) === $startString);
    }


    function checkSuscripcion($id){
        $box = $this->getDoctrine()->getRepository('CarroiridianBundle:Boxplan')->find($id);
        $suscripcion = $this->getDoctrine()->getRepository('CarroiridianBundle:Suscripcion')->findOneBy(array('usuario'=>$this->getUser(),'plan_suscripcion'=>$box), array('activa'=>'desc','id'=>'desc'));

        if(!$suscripcion){
            return "";
        }
        $hoy = new \DateTime();
        if($suscripcion){
            $validaHasta = $suscripcion->getValidaHasta();
            if ($hoy > $validaHasta){
                $suscripcion->setActiva(0);
                $em = $this->getDoctrine()->getManager();
                $em->persist($suscripcion);
                $em->flush();
                return 'vencida';
            }
        }
        if($suscripcion && $suscripcion->getActiva()){
            return "activa";
        }else{
            $pagos_pendientes = $this->getDoctrine()->getRepository("CarroiridianBundle:PagoSuscripcion")->findBy(array('suscripcion'=>$suscripcion ));
            $esperando = true;
            foreach ($pagos_pendientes as $p){
                $esperando = $esperando && $p->getState() == 'PENDING';
            }

            if($suscripcion && $esperando){
                return "pendiente";
            }else{
                return 'vencida';
            }
        }
    }





    /**
     * @Route("/product/{id}/{nombre}", name="product")
     */
    public function productAction($id)
    {
        $path = $this->container->getParameter('app.path.productos');
        $producto = $this->getDoctrine()->getRepository('CarroiridianBundle:Producto')->find($id);
        $imagenes = array();
        array_push($imagenes,$path.'/'.$producto->getImagen());
        foreach ($producto->getGalerias() as $galeria){
            array_push($imagenes,$path.'/'.$galeria->getImagen());
        }
        $session = new Session();
        $wish_list = $session->get('wish', array());
        return $this->render('CarroiridianBundle:Default:producto.html.twig',array('producto'=>$producto,'imagenes'=>$imagenes, "in_wish" => isset($wish_list[$id])));
    }

    /**
     * @Route("/gift", name="gift")
     */
    public function giftAction(Request $request)
    {
        $bono = new Bono();

        $form = $this->createForm(BonoType::class, $bono);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $bono = $form->getData();
            $bono->setCodigo($this->generateRandomString());
            $em = $this->getDoctrine()->getManager();
            $em->persist($bono);
            $em->flush();

            $session = new Session();
            $bonos = $session->get('bonos',array());
            //$bono = new Bono();
            $bono_s = array('id'=>$bono->getId(),'de'=>$bono->getDe(),'para'=>$bono->getPara(),'valor'=>$bono->getValorbono()->getValor(),'mensaje'=>$bono->getMensaje());
            array_push($bonos,$bono_s);
            $session->set('bonos', $bonos);

            return $this->redirectToRoute('carrito');
        }
        return $this->render('HomeBundle:Default:gift.html.twig', array('form'=>$form->createView()));
    }


    /**
     * @Route("/boxes", name="boxes")
     */
    public function BoxesAction(){
       $boxes=$this->getDoctrine()->getRepository("CarroiridianBundle:Boxplan")->findAll();
         return $this->render('CarroiridianBundle:Default:boxes.html.twig', array('productos'=>$boxes ));

    }
    /**
     * @Route("/buscador-productos", name="busca_productos")
     */
    public function buscadorAction(Request $request)
    {
        $ci = $this->get('ci');
        $productos = $ci->getProductos(null,null,$request->query->get('search'));
        return $this->render('CarroiridianBundle:Default:productos.html.twig', array('productos'=>$productos, "search" => $request->query->get('search')));
    }
}

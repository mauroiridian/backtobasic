<?php

namespace CarroiridianBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Suscripcion
 *
 * @ORM\Table(name="suscripcion")
 * @ORM\Entity(repositoryClass="CarroiridianBundle\Repository\SuscripcionRepository")
 */
class Suscripcion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $usuario;


    /**
     * @ORM\OneToMany(targetEntity="CarroiridianBundle\Entity\PagoSuscripcion", mappedBy="suscripcion")
     */
    private $pagos;

    /**
     * @var string
     *
     * @ORM\Column(name="id_payu", type="string", length=255, nullable=true)
     */



    private $idPayu;

    /**
     * @var bool
     *
     * @ORM\Column(name="activa", type="boolean" , nullable=true)
     */
    private $activa;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdat", type="datetime")
     */
    private $createdat;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255, nullable=true)
     */
    private $status;

    /**
     * @var int
     *
     * @ORM\Column(name="plan_id", type="integer", nullable=true)
     */
    private $planId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $payer_id;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var bool
     *
     * @ORM\Column(name="reembolso", type="boolean", nullable=true)
     */
    private $reembolso;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechareembolso", type="datetime", nullable=true)
     */
    private $fechareembolso;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="valida_hasta", type="date", nullable=true)
     */
    private $validaHasta;

    /**
     * @ORM\ManyToOne(targetEntity="CarroiridianBundle\Entity\Preferenciasbox")
     */
    private $preferencia;

    /**
     * @ORM\ManyToOne(targetEntity="CarroiridianBundle\Entity\Boxplan")
     */
    private $plan_suscripcion;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $duracion;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idPayu
     *
     * @param string $idPayu
     *
     * @return Suscripcion
     */
    public function setIdPayu($idPayu)
    {
        $this->idPayu = $idPayu;

        return $this;
    }

    /**
     * Get idPayu
     *
     * @return string
     */
    public function getIdPayu()
    {
        return $this->idPayu;
    }

    /**
     * Set activa
     *
     * @param boolean $activa
     *
     * @return Suscripcion
     */
    public function setActiva($activa)
    {
        $this->activa = $activa;

        return $this;
    }

    /**
     * Get activa
     *
     * @return bool
     */
    public function getActiva()
    {
        return $this->activa;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     *
     * @return Suscripcion
     */
    public function setCreatedat($createdat)
    {
        $this->createdat = $createdat;

        return $this;
    }

    /**
     * Get createdat
     *
     * @return \DateTime
     */
    public function getCreatedat()
    {
        return $this->createdat;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Suscripcion
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set planId
     *
     * @param integer $planId
     *
     * @return Suscripcion
     */
    public function setPlanId($planId)
    {
        $this->planId = $planId;

        return $this;
    }

    /**
     * Get planId
     *
     * @return int
     */
    public function getPlanId()
    {
        return $this->planId;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Suscripcion
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set reembolso
     *
     * @param boolean $reembolso
     *
     * @return Suscripcion
     */
    public function setReembolso($reembolso)
    {
        $this->reembolso = $reembolso;

        return $this;
    }

    /**
     * Get reembolso
     *
     * @return bool
     */
    public function getReembolso()
    {
        return $this->reembolso;
    }

    /**
     * Set fechareembolso
     *
     * @param \DateTime $fechareembolso
     *
     * @return Suscripcion
     */
    public function setFechareembolso($fechareembolso)
    {
        $this->fechareembolso = $fechareembolso;

        return $this;
    }

    /**
     * Get fechareembolso
     *
     * @return \DateTime
     */
    public function getFechareembolso()
    {
        return $this->fechareembolso;
    }

    /**
     * Set validaHasta
     *
     * @param \DateTime $validaHasta
     *
     * @return Suscripcion
     */
    public function setValidaHasta($validaHasta)
    {
        $this->validaHasta = $validaHasta;

        return $this;
    }

    /**
     * Get validaHasta
     *
     * @return \DateTime
     */
    public function getValidaHasta()
    {
        return $this->validaHasta;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->pagos = new ArrayCollection();
        $this->createdat= new \DateTime();
    }

    /**
     * Set usuario
     *
     * @param \AppBundle\Entity\User $usuario
     *
     * @return Suscripcion
     */
    public function setUsuario(\AppBundle\Entity\User $usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \AppBundle\Entity\User
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Add pago
     *
     * @param \CarroiridianBundle\Entity\PagoSuscripcion $pago
     *
     * @return Suscripcion
     */
    public function addPago(\CarroiridianBundle\Entity\PagoSuscripcion $pago)
    {
        $this->pagos[] = $pago;

        return $this;
    }

    /**
     * Remove pago
     *
     * @param \CarroiridianBundle\Entity\PagoSuscripcion $pago
     */
    public function removePago(\CarroiridianBundle\Entity\PagoSuscripcion $pago)
    {
        $this->pagos->removeElement($pago);
    }

    /**
     * Get pagos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPagos()
    {
        return $this->pagos;
    }

    /**
     * Set planSuscripcion
     *
     * @param \CarroiridianBundle\Entity\Boxplan $planSuscripcion
     *
     * @return Suscripcion
     */
    public function setPlanSuscripcion(\CarroiridianBundle\Entity\Boxplan $planSuscripcion = null)
    {
        $this->plan_suscripcion = $planSuscripcion;

        return $this;
    }

    /**
     * Get planSuscripcion
     *
     * @return \CarroiridianBundle\Entity\Boxplan
     */
    public function getPlanSuscripcion()
    {
        return $this->plan_suscripcion;
    }

    /**
     * Set duracion
     *
     * @param integer $duracion
     *
     * @return Suscripcion
     */
    public function setDuracion($duracion)
    {
        $this->duracion = $duracion;

        return $this;
    }

    /**
     * Get duracion
     *
     * @return integer
     */
    public function getDuracion()
    {
        return $this->duracion;
    }

    /**
     * Set payerId
     *
     * @param string $payerId
     *
     * @return Suscripcion
     */
    public function setPayerId($payerId)
    {
        $this->payer_id = $payerId;

        return $this;
    }

    /**
     * Get payerId
     *
     * @return string
     */
    public function getPayerId()
    {
        return $this->payer_id;
    }

    public function getInfousuario()
    {
        return "<p>" . $this->getUsuario()->getNombre() . " " . $this->getUsuario()->getApellidos() . " (" . $this->getUsuario()->getEmail() . ")</p>";
    }


    /**
     * Set preferencia
     *
     * @param \CarroiridianBundle\Entity\Preferenciasbox $preferencia
     *
     * @return Suscripcion
     */
    public function setPreferencia(\CarroiridianBundle\Entity\Preferenciasbox $preferencia = null)
    {
        $this->preferencia = $preferencia;

        return $this;
    }

    /**
     * Get preferencia
     *
     * @return \CarroiridianBundle\Entity\Preferenciasbox
     */
    public function getPreferencia()
    {
        return $this->preferencia;
    }

    /**
     * Get Valorespreferencias
     *
     * @return string
     */
    public function getValorespreferencias(){
        $html="";
        if($this->getPreferencia()) {
            $html='<table class="table table-bordered"><tr><td>Genero</td><td>Tiene producto</td><td> Talla camiseta</td><td>Color NO camiseta</td><td> Talla pantalon</td><td> Color NO pantalon</td><td>Talla zapatos</td><td>Direccion</td></tr>';
            $html .= "<tr><td>". $this->getPreferencia()->getGenero() . "</td>";
            $html .= "<td>" . $this->getPreferencia()->getProductotbt() . "</td>";
            $html .= "<td>". $this->getPreferencia()->getTallacamiseta() . "</td>";
            $html .= "<td>" . $this->getPreferencia()->getColorcamiseta() . "</td>";
            $html .= "<td>" . $this->getPreferencia()->getTallapantalon() . "</td>";
            $html .= "<td>" . $this->getPreferencia()->getColorpantalon() . "</td>";
            $html .= "<td>" . $this->getPreferencia()->getTallazapatos() . "</td>";
            $html .= "<td>" . $this->getPreferencia()->getDireccionusuario()  . "</td></tr></table>";


        }
        return $html;
    }
}

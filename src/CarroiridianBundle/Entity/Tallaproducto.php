<?php

namespace CarroiridianBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\PropertyAccess\PropertyAccess;

/**
 * Tallaproducto
 *
 * @ORM\Table(name="tallaproducto")
 * @ORM\Entity(repositoryClass="CarroiridianBundle\Repository\TallaproductoRepository")
 */
class Tallaproducto
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre_es", type="string", length=255)
     */
    private $nombreEs;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre_en", type="string", length=255, nullable=true)
     */
    private $nombreEn;

    /**
     * @ORM\ManyToOne(targetEntity="CarroiridianBundle\Entity\Tipoproducto")
     * @ORM\JoinColumn(name="tipo_id", referencedColumnName="id")
     */
    private $tipo;

    /**
     * @ORM\ManyToOne(targetEntity="CarroiridianBundle\Entity\Genero")
     * @ORM\JoinColumn(name="genero_id", referencedColumnName="id")
     */
    private $genero;



    /**
     * @var boolean
     *
     * @ORM\Column(name="visible", type="boolean")
     */
    private $visible = true;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    public function gen($campo,$locale){
        $accessor = PropertyAccess::createPropertyAccessor();
        return $accessor->getValue($this,$campo.'_'.$locale);
    }




    public function __toString()
    {
     return $this->nombreEs;
    }
    /**
     * Set nombreEs
     *
     * @param string $nombreEs
     *
     * @return Tallaproducto
     */
    public function setNombreEs($nombreEs)
    {
        $this->nombreEs = $nombreEs;

        return $this;
    }

    /**
     * Get nombreEs
     *
     * @return string
     */
    public function getNombreEs()
    {
        return $this->nombreEs;
    }

    /**
     * Set nombreEn
     *
     * @param string $nombreEn
     *
     * @return Tallaproducto
     */
    public function setNombreEn($nombreEn)
    {
        $this->nombreEn = $nombreEn;

        return $this;
    }

    /**
     * Get nombreEn
     *
     * @return string
     */
    public function getNombreEn()
    {
        return $this->nombreEn;
    }

    /**
     * Set tipo
     *
     * @param \CarroiridianBundle\Entity\Tipoproducto $tipo
     *
     * @return Tallaproducto
     */
    public function setTipo(\CarroiridianBundle\Entity\Tipoproducto $tipo = null)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return \CarroiridianBundle\Entity\Tipoproducto
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set genero
     *
     * @param \CarroiridianBundle\Entity\Genero $genero
     *
     * @return Tallaproducto
     */
    public function setGenero(\CarroiridianBundle\Entity\Genero $genero = null)
    {
        $this->genero = $genero;

        return $this;
    }

    /**
     * Get genero
     *
     * @return \CarroiridianBundle\Entity\Genero
     */
    public function getGenero()
    {
        return $this->genero;
    }

    /**
     * Set visible
     *
     * @param boolean $visible
     *
     * @return Tallaproducto
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * Get visible
     *
     * @return boolean
     */
    public function getVisible()
    {
        return $this->visible;
    }
}

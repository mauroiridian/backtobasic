<?php

namespace CarroiridianBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Producto
 *
 * @ORM\Table(name="producto")
 * @ORM\Entity(repositoryClass="CarroiridianBundle\Repository\ProductoRepository")
 * @Vich\Uploadable
 */
class Producto
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="CarroiridianBundle\Entity\Galeriaproducto", mappedBy="producto", cascade={"remove"}, orphanRemoval=true)
     */
    private $galerias;

    /**
     * @ORM\OneToMany(targetEntity="CarroiridianBundle\Entity\Inventario", mappedBy="producto",cascade={"remove"}, orphanRemoval=true)
     */
    private $inventarios;

    /**
     * @var Genero[]
     * @ORM\ManyToMany(targetEntity="CarroiridianBundle\Entity\Genero")
     */
    protected $generos;

    /**
     * @var Color[]
     * @ORM\ManyToMany(targetEntity="CarroiridianBundle\Entity\Color")
     */
    protected $colores;

    /**
     * @var Producto[]
     * @ORM\ManyToMany(targetEntity="CarroiridianBundle\Entity\Producto")
     */
    protected $coloresRelacionados;

    /**
     * @ORM\ManyToOne(targetEntity="CarroiridianBundle\Entity\Categoria")
     * @ORM\JoinColumn(name="categoria_id", referencedColumnName="id")
     */
    private $categoria;

    /**
     * @ORM\ManyToOne(targetEntity="CarroiridianBundle\Entity\Modelo")
     * @ORM\JoinColumn(name="modelo_id", referencedColumnName="id")
     */
    private $modelo;

    /**
     * @ORM\ManyToOne(targetEntity="CarroiridianBundle\Entity\Color")
     * @ORM\JoinColumn(name="color_id", referencedColumnName="id")
     */
    private $color;

    /**
     * @var string
     *
     * @ORM\Column(name="sku", type="string", length=255)
     */
    private $sku;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre_es", type="string", length=255)
     */
    private $nombreEs;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre_en", type="string", length=255, nullable=true)
     */
    private $nombreEn;

    /**
     * @var string
     *
     * @ORM\Column(name="imagen", type="string", length=255)
     */
    private $imagen;

    /**
     * @Vich\UploadableField(mapping="productos", fileNameProperty="imagen")
     * @var File
     */
    private $imageFile;

    /**
     * @var string
     *
     * @ORM\Column(name="imagenaux", type="string", length=255, nullable=true)
     */
    private $imagenaux;

    /**
     * @Vich\UploadableField(mapping="productos", fileNameProperty="imagenaux")
     * @var File
     */
    private $imageauxFile;

    /**
     * @var float
     *
     * @ORM\Column(name="precio", type="float")
     */
    private $precio;

    /**
     * @var \float
     *
     * @ORM\Column(name="precio_fecha", type="float", nullable=true)
     */
    private $precioFecha;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_ini", type="datetime", nullable=true)
     */
    private $fechaIni;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_fin", type="datetime", nullable=true)
     */
    private $fechaFin;

    /**
     * @var bool
     *
     * @ORM\Column(name="destacado", type="boolean", nullable=true)
     */
    private $destacado;

    /**
     * @var int
     *
     * @ORM\Column(name="unidades", type="integer", nullable=true)
     */
    private $unidades;

    /**
     * @var int
     *
     * @ORM\Column(name="min_unidades", type="integer", nullable=true)
     */
    private $minUnidades;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion_es", type="text")
     */
    private $descripcionEs;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion_en", type="text", nullable=true)
     */
    private $descripcionEn;

    /**
     * @var Estilo[]
     * @ORM\ManyToMany(targetEntity="CarroiridianBundle\Entity\Estilo")
     */
    protected $estilos;

    /**
     *
     * @var array
     * @ORM\Column(name="caracteristicas",type="array")
     */
    private $caracteristicas = array();

    /**
     * List of tags associated to the product.
     *
     * @var array
     * @ORM\Column(name="tags", type="array")
     */
    private $tags = array();


    /**
     * @var string
     *
     * @ORM\Column(name="alt", type="string", length=255)
     */
    private $alt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="nuevo", type="boolean")
     */
    private $nuevo = false;


    /**
     * @var integer
     *
     * @ORM\Column(name="orden", type="integer")
     */
    private $orden = 1;

    /**
     * @var boolean
     *
     * @ORM\Column(name="visible", type="boolean")
     */
    private $visible = true;

    /**
     * @var string
     *
     * @ORM\Column(name="fit_es", type="text")
     */
    private $fitEs;

    /**
     * @var string
     *
     * @ORM\Column(name="fit_en", type="text")
     */
    private $fitEn;

    /**
     * @var boolean
     *
     * @ORM\Column(name="visible_medidas", type="boolean")
     */
    private $visible_medidas = true;



    public function __toString()
    {
        $str = "";
        if(count($this->colores) > 0){
            foreach ($this->colores as &$item){
                $str .= " | " . $item->getNombreEs();
            }
        }else{
            $str = " | Sin color";
        }
        return $this->nombreEs . $str;
    }

    public function gen($campo,$locale){
        $accessor = PropertyAccess::createPropertyAccessor();
        return $accessor->getValue($this,$campo.'_'.$locale);
    }


    public function __construct()
    {
        $this->estilos = new ArrayCollection();
        $this->galerias = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sku
     *
     * @param string $sku
     *
     * @return Producto
     */
    public function setSku($sku)
    {
        $this->sku = $sku;

        return $this;
    }

    /**
     * Get sku
     *
     * @return string
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * Set nombreEs
     *
     * @param string $nombreEs
     *
     * @return Producto
     */
    public function setNombreEs($nombreEs)
    {
        $this->nombreEs = $nombreEs;

        return $this;
    }

    /**
     * Get nombreEs
     *
     * @return string
     */
    public function getNombreEs()
    {
        return $this->nombreEs;
    }

    /**
     * Set nombreEn
     *
     * @param string $nombreEn
     *
     * @return Producto
     */
    public function setNombreEn($nombreEn)
    {
        $this->nombreEn = $nombreEn;

        return $this;
    }

    /**
     * Get nombreEn
     *
     * @return string
     */
    public function getNombreEn()
    {
        return $this->nombreEn;
    }

    /**
     * Set imagen
     *
     * @param string $imagen
     *
     * @return Producto
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;

        return $this;
    }

    /**
     * Get imagen
     *
     * @return string
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    /**
     * Set precio
     *
     * @param float $precio
     *
     * @return Producto
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get precio
     *
     * @return float
     */
    public function getPrecio()
    {
        return $this->precio;
    }


    /**
     * Set fechaIni
     *
     * @param \DateTime $fechaIni
     *
     * @return Producto
     */
    public function setFechaIni($fechaIni)
    {
        $this->fechaIni = $fechaIni;

        return $this;
    }

    /**
     * Get fechaIni
     *
     * @return \DateTime
     */
    public function getFechaIni()
    {
        return $this->fechaIni;
    }

    /**
     * Set fechaFin
     *
     * @param \DateTime $fechaFin
     *
     * @return Producto
     */
    public function setFechaFin($fechaFin)
    {
        $this->fechaFin = $fechaFin;

        return $this;
    }

    /**
     * Get fechaFin
     *
     * @return \DateTime
     */
    public function getFechaFin()
    {
        return $this->fechaFin;
    }

    /**
     * Set destacado
     *
     * @param boolean $destacado
     *
     * @return Producto
     */
    public function setDestacado($destacado)
    {
        $this->destacado = $destacado;

        return $this;
    }

    /**
     * Get destacado
     *
     * @return bool
     */
    public function getDestacado()
    {
        return $this->destacado;
    }

    /**
     * Set unidades
     *
     * @param integer $unidades
     *
     * @return Producto
     */
    public function setUnidades($unidades)
    {
        $this->unidades = $unidades;

        return $this;
    }

    /**
     * Get unidades
     *
     * @return int
     */
    public function getUnidades()
    {
        return $this->unidades;
    }

    /**
     * Set minUnidades
     *
     * @param integer $minUnidades
     *
     * @return Producto
     */
    public function setMinUnidades($minUnidades)
    {
        $this->minUnidades = $minUnidades;

        return $this;
    }

    /**
     * Get minUnidades
     *
     * @return int
     */
    public function getMinUnidades()
    {
        return $this->minUnidades;
    }

    /**
     * Set descripcionEs
     *
     * @param string $descripcionEs
     *
     * @return Producto
     */
    public function setDescripcionEs($descripcionEs)
    {
        $this->descripcionEs = $descripcionEs;

        return $this;
    }

    /**
     * Get descripcionEs
     *
     * @return string
     */
    public function getDescripcionEs()
    {
        return $this->descripcionEs;
    }

    /**
     * Set descripcionEn
     *
     * @param string $descripcionEn
     *
     * @return Producto
     */
    public function setDescripcionEn($descripcionEn)
    {
        $this->descripcionEn = $descripcionEn;

        return $this;
    }

    /**
     * Get descripcionEn
     *
     * @return string
     */
    public function getDescripcionEn()
    {
        return $this->descripcionEn;
    }

    /**
     * Set precioFecha
     *
     * @param float $precioFecha
     *
     * @return Producto
     */
    public function setPrecioFecha($precioFecha)
    {
        $this->precioFecha = $precioFecha;

        return $this;
    }

    /**
     * Get precioFecha
     *
     * @return float
     */
    public function getPrecioFecha()
    {
        return $this->precioFecha;
    }

    /**
     * Add estilo
     *
     * @param \CarroiridianBundle\Entity\Estilo $estilo
     *
     * @return Producto
     */
    public function addEstilo(\CarroiridianBundle\Entity\Estilo $estilo)
    {
        $this->estilos[] = $estilo;

        return $this;
    }

    /**
     * Remove estilo
     *
     * @param \CarroiridianBundle\Entity\Estilo $estilo
     */
    public function removeEstilo(\CarroiridianBundle\Entity\Estilo $estilo)
    {
        $this->estilos->removeElement($estilo);
    }

    /**
     * Get estilos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEstilos()
    {
        return $this->estilos;
    }


    /**
     * Set alt
     *
     * @param string $alt
     *
     * @return Producto
     */
    public function setAlt($alt)
    {
        $this->alt = $alt;
    
        return $this;
    }

    /**
     * Get alt
     *
     * @return string
     */
    public function getAlt()
    {
        return $this->alt;
    }

    /**
     * Set orden
     *
     * @param integer $orden
     *
     * @return Producto
     */
    public function setOrden($orden)
    {
        $this->orden = $orden;
    
        return $this;
    }

    /**
     * Get orden
     *
     * @return integer
     */
    public function getOrden()
    {
        return $this->orden;
    }

    /**
     * Set visible
     *
     * @param boolean $visible
     *
     * @return Producto
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;
    
        return $this;
    }

    /**
     * Get visible
     *
     * @return boolean
     */
    public function getVisible()
    {
        return $this->visible;
    }

    /**
     * Add galeria
     *
     * @param \CarroiridianBundle\Entity\Galeriaproducto $galeria
     *
     * @return Producto
     */
    public function addGaleria(\CarroiridianBundle\Entity\Galeriaproducto $galeria)
    {
        $this->galerias[] = $galeria;
    
        return $this;
    }

    /**
     * Remove galeria
     *
     * @param \CarroiridianBundle\Entity\Galeriaproducto $galeria
     */
    public function removeGaleria(\CarroiridianBundle\Entity\Galeriaproducto $galeria)
    {
        $this->galerias->removeElement($galeria);
    }

    /**
     * Get galerias
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGalerias()
    {
        return $this->galerias;
    }

    /**
     * Set categoria
     *
     * @param \CarroiridianBundle\Entity\Categoria $categoria
     *
     * @return Producto
     */
    public function setCategoria(\CarroiridianBundle\Entity\Categoria $categoria = null)
    {
        $this->categoria = $categoria;
    
        return $this;
    }

    /**
     * Get categoria
     *
     * @return \CarroiridianBundle\Entity\Categoria
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * @param File $image
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;
    }

    /**
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * Add inventario
     *
     * @param \CarroiridianBundle\Entity\Inventario $inventario
     *
     * @return Producto
     */
    public function addInventario(\CarroiridianBundle\Entity\Inventario $inventario)
    {
        $this->inventarios[] = $inventario;
    
        return $this;
    }

    /**
     * Remove inventario
     *
     * @param \CarroiridianBundle\Entity\Inventario $inventario
     */
    public function removeInventario(\CarroiridianBundle\Entity\Inventario $inventario)
    {
        $this->inventarios->removeElement($inventario);
    }

    /**
     * Get inventarios
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInventarios()
    {
        return $this->inventarios;
    }

    /**
     * Set color
     *
     * @param \CarroiridianBundle\Entity\Color $color
     *
     * @return Producto
     */
    public function setColor(\CarroiridianBundle\Entity\Color $color = null)
    {
        $this->color = $color;
    
        return $this;
    }

    /**
     * Get color
     *
     * @return \CarroiridianBundle\Entity\Color
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set caracteristicas
     *
     * @param array $caracteristicas
     *
     * @return Producto
     */
    public function setCaracteristicas($caracteristicas)
    {
        $this->caracteristicas = $caracteristicas;
    
        return $this;
    }

    /**
     * Get caracteristicas
     *
     * @return array
     */
    public function getCaracteristicas()
    {
        return $this->caracteristicas;
    }

    /**
     * Set tags
     *
     * @param array $tags
     *
     * @return Producto
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
    
        return $this;
    }

    /**
     * Get tags
     *
     * @return array
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Add colore
     *
     * @param \CarroiridianBundle\Entity\Color $colore
     *
     * @return Producto
     */
    public function addColore(\CarroiridianBundle\Entity\Color $colore)
    {
        $this->colores[] = $colore;
    
        return $this;
    }

    /**
     * Remove colore
     *
     * @param \CarroiridianBundle\Entity\Color $colore
     */
    public function removeColore(\CarroiridianBundle\Entity\Color $colore)
    {
        $this->colores->removeElement($colore);
    }

    /**
     * Get colores
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getColores()
    {
        return $this->colores;
    }

    /**
     * Set nuevo
     *
     * @param boolean $nuevo
     *
     * @return Producto
     */
    public function setNuevo($nuevo)
    {
        $this->nuevo = $nuevo;
    
        return $this;
    }

    /**
     * Get nuevo
     *
     * @return boolean
     */
    public function getNuevo()
    {
        return $this->nuevo;
    }

    /**
     * Add coloresRelacionado
     *
     * @param \CarroiridianBundle\Entity\Producto $coloresRelacionado
     *
     * @return Producto
     */
    public function addColoresRelacionado(\CarroiridianBundle\Entity\Producto $coloresRelacionado)
    {
        $this->coloresRelacionados[] = $coloresRelacionado;
    
        return $this;
    }

    /**
     * Remove coloresRelacionado
     *
     * @param \CarroiridianBundle\Entity\Producto $coloresRelacionado
     */
    public function removeColoresRelacionado(\CarroiridianBundle\Entity\Producto $coloresRelacionado)
    {
        $this->coloresRelacionados->removeElement($coloresRelacionado);
    }

    /**
     * Get coloresRelacionados
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getColoresRelacionados()
    {
        return $this->coloresRelacionados;
    }

    /**
     * Set imagenaux
     *
     * @param string $imagenaux
     *
     * @return Producto
     */
    public function setImagenaux($imagenaux)
    {
        $this->imagenaux = $imagenaux;
    
        return $this;
    }

    /**
     * Get imagenaux
     *
     * @return string
     */
    public function getImagenaux()
    {
        return $this->imagenaux;
    }

    /**
     * @param File $imageaux
     */
    public function setImageauxFile(File $imageaux = null)
    {
        $this->imageauxFile = $imageaux;
    }

    /**
     * @return File
     */
    public function getImageauxFile()
    {
        return $this->imageauxFile;
    }

    

    /**
     * Set modelo
     *
     * @param \CarroiridianBundle\Entity\Modelo $modelo
     *
     * @return Producto
     */
    public function setModelo(\CarroiridianBundle\Entity\Modelo $modelo = null)
    {
        $this->modelo = $modelo;

        return $this;
    }

    /**
     * Get modelo
     *
     * @return \CarroiridianBundle\Entity\Modelo
     */
    public function getModelo()
    {
        return $this->modelo;
    }

    /**
     * Set fitEs
     *
     * @param string $fitEs
     *
     * @return Producto
     */
    public function setFitEs($fitEs)
    {
        $this->fitEs = $fitEs;

        return $this;
    }

    /**
     * Get fitEs
     *
     * @return string
     */
    public function getFitEs()
    {
        return $this->fitEs;
    }

    /**
     * Set fitEn
     *
     * @param string $fitEn
     *
     * @return Producto
     */
    public function setFitEn($fitEn)
    {
        $this->fitEn = $fitEn;

        return $this;
    }

    /**
     * Get fitEn
     *
     * @return string
     */
    public function getFitEn()
    {
        return $this->fitEn;
    }

    /**
     * Add genero
     *
     * @param \CarroiridianBundle\Entity\Genero $genero
     *
     * @return Producto
     */
    public function addGenero(\CarroiridianBundle\Entity\Genero $genero)
    {
        $this->generos[] = $genero;

        return $this;
    }

    /**
     * Remove genero
     *
     * @param \CarroiridianBundle\Entity\Genero $genero
     */
    public function removeGenero(\CarroiridianBundle\Entity\Genero $genero)
    {
        $this->generos->removeElement($genero);
    }

    /**
     * Get generos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGeneros()
    {
        return $this->generos;
    }

    /**
     * Set visibleMedidas
     *
     * @param boolean $visibleMedidas
     *
     * @return Producto
     */
    public function setVisibleMedidas($visibleMedidas)
    {
        $this->visible_medidas = $visibleMedidas;

        return $this;
    }

    /**
     * Get visibleMedidas
     *
     * @return boolean
     */
    public function getVisibleMedidas()
    {
        return $this->visible_medidas;
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: Iridian 4
 * Date: 10/08/2016
 * Time: 4:37 PM
 */

namespace CarroiridianBundle\Entity;

use AppBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * Descuento
 *
 * @ORM\Table(name="descuento")
 * @ORM\Entity(repositoryClass="CarroiridianBundle\Repository\DescuentoRepository")
 */
class Descuento
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(name="descuento_id", referencedColumnName="id")
     */
    protected $solopara;

    /**
     * @var User[]
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\User", mappedBy="descuento")
     */
    protected $redimidopor;

    /**
     * @var integer
     *
     * @ORM\Column(name="porcentaje", type="integer")
     */
    private $porcentaje = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="string", length=255, unique=true)
     */
    private $codigo;

    /**
     * @var date
     *
     * @ORM\Column(name="fecha", type="date", nullable=false)
     */
    private $fecha;

    public function __toString()
    {
        return $this->id . " - " . $this->codigo;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set porcentaje
     *
     * @param integer $porcentaje
     *
     * @return Descuento
     */
    public function setPorcentaje($porcentaje)
    {
        $this->porcentaje = $porcentaje;

        return $this;
    }

    /**
     * Get porcentaje
     *
     * @return integer
     */
    public function getPorcentaje()
    {
        return $this->porcentaje;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     *
     * @return Descuento
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set solopara
     *
     * @param \AppBundle\Entity\User $solopara
     *
     * @return Descuento
     */
    public function setSolopara(\AppBundle\Entity\User $solopara = null)
    {
        $this->solopara = $solopara;

        return $this;
    }

    /**
     * Get solopara
     *
     * @return \AppBundle\Entity\User
     */
    public function getSolopara()
    {
        return $this->solopara;
    }

    /**
     * Set redimidopor
     *
     * @param \AppBundle\Entity\User $redimidopor
     *
     * @return Descuento
     */
    public function setRedimidopor(\AppBundle\Entity\User $redimidopor = null)
    {
        $this->redimidopor = $redimidopor;

        return $this;
    }

    /**
     * Get redimidopor
     *
     * @return \AppBundle\Entity\User
     */
    public function getRedimidopor()
    {
        return $this->redimidopor;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return Descuento
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->redimidopor = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add redimidopor
     *
     * @param \AppBundle\Entity\User $redimidopor
     *
     * @return Descuento
     */
    public function addRedimidopor(\AppBundle\Entity\User $redimidopor)
    {
        $this->redimidopor[] = $redimidopor;

        return $this;
    }

    /**
     * Remove redimidopor
     *
     * @param \AppBundle\Entity\User $redimidopor
     */
    public function removeRedimidopor(\AppBundle\Entity\User $redimidopor)
    {
        $this->redimidopor->removeElement($redimidopor);
    }
}

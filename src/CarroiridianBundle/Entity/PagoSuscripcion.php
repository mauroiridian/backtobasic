<?php

namespace CarroiridianBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PagoSuscripcion
 *
 * @ORM\Table(name="pago_suscripcion")
 * @ORM\Entity(repositoryClass="CarroiridianBundle\Repository\PagoSuscripcionRepository")
 */
class PagoSuscripcion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="state", type="string", length=255, nullable=true)
     */
    private $state;



    /**
     * @ORM\ManyToOne(targetEntity="CarroiridianBundle\Entity\Suscripcion", inversedBy="pagos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $suscripcion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     */
    private $createdAt;

    /**
     * @var string
     *
     * @ORM\Column(name="id_payu", type="string", length=255, nullable=true)
     */
    private $idPayu;

    /**
     * @var string
     *
     * @ORM\Column(name="orderId", type="string", length=255, nullable=true)
     */
    private $orderId;

    /**
     * @var string
     *
     * @ORM\Column(name="monto", type="string", length=255, nullable=true)
     */
    private $monto;

    /**
     * @var string
     *
     * @ORM\Column(name="moneda", type="string", length=255, nullable=true)
     */
    private $moneda;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="date")
     */
    private $fecha;

    /**
     * @var string
     *
     * @ORM\Column(name="cus", type="string", length=255, nullable=true)
     */
    private $cus;

    /**
     * @var string
     *
     * @ORM\Column(name="reference_pol", type="string", length=255, nullable=true)
     */
    private $referencePol;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set state
     *
     * @param string $state
     *
     * @return PagoSuscripcion
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return PagoSuscripcion
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    public function __toString()
    {
        if($this)
            return $this->getId().' ';
        return '';
    }


    /**
     * Set idPayu
     *
     * @param string $idPayu
     *
     * @return PagoSuscripcion
     */
    public function setIdPayu($idPayu)
    {
        $this->idPayu = $idPayu;

        return $this;
    }

    /**
     * Get idPayu
     *
     * @return string
     */
    public function getIdPayu()
    {
        return $this->idPayu;
    }

    /**
     * Set orderId
     *
     * @param string $orderId
     *
     * @return PagoSuscripcion
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;

        return $this;
    }

    /**
     * Get orderId
     *
     * @return string
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * Set monto
     *
     * @param string $monto
     *
     * @return PagoSuscripcion
     */
    public function setMonto($monto)
    {
        $this->monto = $monto;

        return $this;
    }

    /**
     * Get monto
     *
     * @return string
     */
    public function getMonto()
    {
        return $this->monto;
    }

    /**
     * Set moneda
     *
     * @param string $moneda
     *
     * @return PagoSuscripcion
     */
    public function setMoneda($moneda)
    {
        $this->moneda = $moneda;

        return $this;
    }

    /**
     * Get moneda
     *
     * @return string
     */
    public function getMoneda()
    {
        return $this->moneda;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return PagoSuscripcion
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set cus
     *
     * @param string $cus
     *
     * @return PagoSuscripcion
     */
    public function setCus($cus)
    {
        $this->cus = $cus;

        return $this;
    }

    /**
     * Get cus
     *
     * @return string
     */
    public function getCus()
    {
        return $this->cus;
    }

    /**
     * Set referencePol
     *
     * @param string $referencePol
     *
     * @return PagoSuscripcion
     */
    public function setReferencePol($referencePol)
    {
        $this->referencePol = $referencePol;

        return $this;
    }

    /**
     * Get referencePol
     *
     * @return string
     */
    public function getReferencePol()
    {
        return $this->referencePol;
    }

    /**
     * Set suscripcion
     *
     * @param \CarroiridianBundle\Entity\Suscripcion $suscripcion
     *
     * @return PagoSuscripcion
     */
    public function setSuscripcion(\CarroiridianBundle\Entity\Suscripcion $suscripcion)
    {
        $this->suscripcion = $suscripcion;

        return $this;
    }

    /**
     * Get suscripcion
     *
     * @return \CarroiridianBundle\Entity\Suscripcion
     */
    public function getSuscripcion()
    {
        return $this->suscripcion;
    }
}

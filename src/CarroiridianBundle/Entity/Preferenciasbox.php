<?php

namespace CarroiridianBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Preferenciasbox
 *
 * @ORM\Table(name="preferenciasbox")
 * @ORM\Entity(repositoryClass="CarroiridianBundle\Repository\PreferenciasboxRepository")
 */
class Preferenciasbox
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $user;


    /**
     * @ORM\ManyToOne(targetEntity="CarroiridianBundle\Entity\Boxplan")
     * @ORM\JoinColumn(name="box_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $box;



    /**
     * @ORM\ManyToOne(targetEntity="CarroiridianBundle\Entity\Talla")
     * @ORM\JoinColumn(name="tallacamiseta_id", referencedColumnName="id")
     */
    private $tallacamiseta;

    /**
     * @var string
     *
     * @ORM\Column(name="colorcamiseta", type="string", length=255)
     */
    private $colorcamiseta;

    /**
     * @var string
     *
     * @ORM\Column(name="productotbt", type="string", length=255)
     */
    private $productotbt;

    /**
     * @var string
     *
     * @ORM\Column(name="colorpantalon", type="string", length=255)
     */
    private $colorpantalon;

    /**
     * @ORM\ManyToOne(targetEntity="CarroiridianBundle\Entity\Tallaproducto")
     * @ORM\JoinColumn(name="tallapantalon_id", referencedColumnName="id")
     */
    private $tallapantalon;

    /**
     * @ORM\ManyToOne(targetEntity="CarroiridianBundle\Entity\Tallaproducto")
     * @ORM\JoinColumn(name="tallazapatos_id", referencedColumnName="id")
     */
    private $tallazapatos;

    /**
     * @ORM\ManyToOne(targetEntity="CarroiridianBundle\Entity\Genero")
     * @ORM\JoinColumn(name="genero_id", referencedColumnName="id")
     */
    private $genero;

    /**
     * @ORM\ManyToOne(targetEntity="CarroiridianBundle\Entity\Envio")
     * @ORM\JoinColumn(name="direccion_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $direccion;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set colorcamiseta
     *
     * @param string $colorcamiseta
     *
     * @return Preferenciasbox
     */
    public function setColorcamiseta($colorcamiseta)
    {
        $this->colorcamiseta = $colorcamiseta;

        return $this;
    }

    /**
     * Get colorcamiseta
     *
     * @return string
     */
    public function getColorcamiseta()
    {
        return $this->colorcamiseta;
    }

    /**
     * Set productotbt
     *
     * @param string $productotbt
     *
     * @return Preferenciasbox
     */
    public function setProductotbt($productotbt)
    {
        $this->productotbt = $productotbt;

        return $this;
    }

    /**
     * Get productotbt
     *
     * @return string
     */
    public function getProductotbt()
    {
        return $this->productotbt;
    }

    /**
     * Set colorpantalon
     *
     * @param string $colorpantalon
     *
     * @return Preferenciasbox
     */
    public function setColorpantalon($colorpantalon)
    {
        $this->colorpantalon = $colorpantalon;

        return $this;
    }

    /**
     * Get colorpantalon
     *
     * @return string
     */
    public function getColorpantalon()
    {
        return $this->colorpantalon;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Preferenciasbox
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set tallacamiseta
     *
     * @param \CarroiridianBundle\Entity\Talla $tallacamiseta
     *
     * @return Preferenciasbox
     */
    public function setTallacamiseta(\CarroiridianBundle\Entity\Talla $tallacamiseta = null)
    {
        $this->tallacamiseta = $tallacamiseta;

        return $this;
    }

    /**
     * Get tallacamiseta
     *
     * @return \CarroiridianBundle\Entity\Talla
     */
    public function getTallacamiseta()
    {
        return $this->tallacamiseta;
    }

    /**
     * Set tallapantalon
     *
     * @param \CarroiridianBundle\Entity\Tallaproducto $tallapantalon
     *
     * @return Preferenciasbox
     */
    public function setTallapantalon(\CarroiridianBundle\Entity\Tallaproducto $tallapantalon = null)
    {
        $this->tallapantalon = $tallapantalon;

        return $this;
    }

    /**
     * Get tallapantalon
     *
     * @return \CarroiridianBundle\Entity\Tallaproducto
     */
    public function getTallapantalon()
    {
        return $this->tallapantalon;
    }

    /**
     * Set tallazapatos
     *
     * @param \CarroiridianBundle\Entity\Tallaproducto $tallazapatos
     *
     * @return Preferenciasbox
     */
    public function setTallazapatos(\CarroiridianBundle\Entity\Tallaproducto $tallazapatos = null)
    {
        $this->tallazapatos = $tallazapatos;

        return $this;
    }

    /**
     * Get tallazapatos
     *
     * @return \CarroiridianBundle\Entity\Tallaproducto
     */
    public function getTallazapatos()
    {
        return $this->tallazapatos;
    }

    /**
     * Set genero
     *
     * @param \CarroiridianBundle\Entity\Genero $genero
     *
     * @return Preferenciasbox
     */
    public function setGenero(\CarroiridianBundle\Entity\Genero $genero = null)
    {
        $this->genero = $genero;

        return $this;
    }

    /**
     * Get genero
     *
     * @return \CarroiridianBundle\Entity\Genero
     */
    public function getGenero()
    {
        return $this->genero;
    }

    /**
     * Set direccion
     *
     * @param \CarroiridianBundle\Entity\Envio $direccion
     *
     * @return Preferenciasbox
     */
    public function setDireccion(\CarroiridianBundle\Entity\Envio $direccion = null)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return \CarroiridianBundle\Entity\Envio
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set box
     *
     * @param \CarroiridianBundle\Entity\Boxplan $box
     *
     * @return Preferenciasbox
     */
    public function setBox(\CarroiridianBundle\Entity\Boxplan $box = null)
    {
        $this->box = $box;

        return $this;
    }

    /**
     * Get box
     *
     * @return \CarroiridianBundle\Entity\Boxplan
     */
    public function getBox()
    {
        return $this->box;
    }

    public function getDireccionusuario()
    {
        $html="<p>".$this->getDireccion()->getDepartamento()."-".$this->getDireccion()->getCiudad()."<br/>".
           $this->getDireccion()->getDireccion().
            "</p>";
        return $html;
    }
}

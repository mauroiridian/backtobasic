<?php

namespace ShoppingBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/shopping")
     */
    public function indexAction()
    {
        return $this->render('ShoppingBundle:Default:index.html.twig');
    }
}

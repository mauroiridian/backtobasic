<?php

namespace PagosPayuBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PayuLog
 *
 * @ORM\Table(name="payu_log")
 * @ORM\Entity(repositoryClass="PagosPayuBundle\Repository\PayuLogRepository")
 */
class PayuLog
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="data", type="text", nullable=true)
     */
    private $data;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set data
     *
     * @param string $data
     *
     * @return PayuLog
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }
}

<?php

namespace PagosPayuBundle\Controller;

use CarroiridianBundle\Entity\Compra;
use CarroiridianBundle\Entity\Compraitem;
use CarroiridianBundle\Entity\Entrada;
use CarroiridianBundle\Entity\PagoLogger;
use CarroiridianBundle\Entity\PagoSuscripcion;
use PagosPayuBundle\Entity\PayuLog;
use PagosPayuBundle\Entity\RepuestaPago;
use PhpParser\Node\Scalar\MagicConst\Method;
use Proxies\__CG__\CarroiridianBundle\Entity\Inventario;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class DefaultController extends Controller
{
    /**
     * @Route("/pagar-payu",name="pagar_payu")
     */
    public function indexAction(Request $request)
    {
        $session = new Session();
        $carrito = $session->get('carrito', null);
	    $bonos = $session->get("bonos", null);
        if($carrito == null && $bonos == null){
            return $this->redirectToRoute('carrito');
        }
        $tipo_pago = $session->get('tipo_pago', "Online");
        $direccion_id = $session->get('direccion_id');
        $direccion = $this->getDoctrine()->getRepository('CarroiridianBundle:Envio')->find($direccion_id);


        $descuento = $session->get("descuento", null);
        $descuento_porc = $session->get("descuento_porc", null);

        $estado = $this->getDoctrine()->getRepository('CarroiridianBundle:EstadoCarrito')->findOneBy(array('ref'=>'INICIADA_EN_WEB'));
        $datos_payu = $this->getDoctrine()->getRepository('PagosPayuBundle:DatosPayu')->find(1);
        $ci = $this->get('ci');

        $user = $this->getUser();
        $compra = new Compra();
        $compra->setComprador($user);
        $compra->setDireccion($direccion);
        $compra->setEstado($estado);
        $compra->setPrueba($datos_payu->getTest());
        $compra->setTipoPago($tipo_pago);
        $em = $this->getDoctrine()->getManager();
        $repo_p = $this->getDoctrine()->getRepository('CarroiridianBundle:Producto');
        $repo_t = $this->getDoctrine()->getRepository('CarroiridianBundle:Talla');
        $repo_b = $this->getDoctrine()->getRepository('CarroiridianBundle:Bono');
        $repo_d = $this->getDoctrine()->getRepository('CarroiridianBundle:Descuento');
        $em->persist($compra);
        $em->flush();

        $ciudad = $this->getDoctrine()->getRepository('CarroiridianBundle:Ciudad')->find($compra->getDireccion()->getCiudad()->getId());
        $nombreCiudad = $ciudad->getNombre();

        $total = 0;
        $descripcion_text = '';
        $descripcion = '<table>';
        if($carrito > 0 && $carrito != null){
            foreach ($carrito as $id=>$tallas){
                foreach ($tallas as $id_talla=>$item){
                    if($item['cantidad'] > 0){
                        $producto = $repo_p->find($id);

                        $talla = $repo_t->find($id_talla);
                        //exit(dump($producto));
                        $compraitem = new Compraitem();
                        $compraitem->setCantidad($item['cantidad']);
                        $compraitem->setProducto($producto);
                        $compraitem->setTalla($talla);
                        $compraitem->setCompra($compra);
                        $em->persist($compraitem);
                        $em->flush();
                        $subtotal = $item['cantidad'] * $producto->getPrecio();
                        $total += $subtotal;
                        $descripcion .= '<tr>';
                        $descripcion .= '<td>'.$producto.'</td>';
                        $descripcion .= '<td>'.$talla.'</td>';
                        $descripcion .= '<td> $'.number_format($producto->getPrecio()).'</td>';
                        $descripcion .= '<td>'.$item['cantidad'].'</td>';
                        $descripcion .= '<td> $'.number_format($subtotal).'</td>';
                        $descripcion .= '</tr>';

                        $descripcion_text .= '|'.$producto.' - ';
                        $descripcion_text .= $talla.' - ';
                        $descripcion_text .= '$'.number_format($producto->getPrecio()).' - ';
                        $descripcion_text .= 'X'.$item['cantidad'].' - ';
                        $descripcion_text .= '$'.number_format($subtotal).' | ';
                    }
                }
            }
        }

        if(count($bonos) > 0 && $bonos != null){
            foreach ($bonos as $id=>$bono){
                $bono_src = $repo_b->find($bono['id']);
                $bono_src->setCompra($compra);
                $em->persist($bono_src);
                $em->flush();
                $subtotal = $bono['valor'];
                $total += $subtotal;
                $descripcion .= '<tr>';
                $descripcion .= '<td>'.$bono['de'].'</td>';
                $descripcion .= '<td>'.$bono['para'].'</td>';
                $descripcion .= '<td> $'.number_format($bono['valor']).'</td>';
                $descripcion .= '<td>1</td>';
                $descripcion .= '<td> $'.$bono['valor'].'</td>';
                $descripcion .= '</tr>';
                $descripcion_text .= '|Bono de regalo, de '.$bono["de"].' para ' . $bono["para"] . ' - ';
                $descripcion_text .= '$'.number_format($bono['valor']).' - ';
                $descripcion_text .= 'X1 - ';
                $descripcion_text .= '$'.number_format($bono['valor']).' | ';
            }
        }

        $descripcion .= '</table>';
        $compra->setDescripcion($descripcion);
        /**
         *    {% set discount_porc = (total * discount_porc) / 100 %}
        {% set total = total - discount_porc %}
        {% else %}
        {% set total = total - discount %}
         */
        $total = $total + $compra->getDireccion()->getCiudad()->getCosto();
        if(isset($descuento['id'])){
            $bono_temp = $repo_b->find($descuento['id']);
            $valor = $bono_temp->getValorbono()->getValor() - $bono_temp->getReclama();
            $total = $total - $valor;
            if($total <= 0){
                $valor = $total;
                $total = 0;
            }
            $bono_temp->setReclama($bono_temp->getReclama() + $valor);
            $em->persist($bono_temp);
            $em->flush();
            //unset($bono_temp);
        }
        if(isset($descuento_porc['id'])){
            $descuento_porc_temp = $repo_d->find($descuento_porc['id']);
            $descuento = ($total * $descuento_porc_temp->getPorcentaje()) / 100;
            $total = $total - $descuento;
            $this->getUser()->addDescuento($descuento_porc_temp);
            $em->persist($this->getUser());
            $em->flush();
            //unset($descuento_porc_temp);
        }
        $compra->setPrecio($total);
        $em->persist($compra);
        $em->flush();
        $descripcion_text .= 'Costo envio, $' . number_format($direccion->getCiudad()->getCosto()) . ' |';
        $descripcion_text .= 'TOTAL: $'.number_format($total);
        $tax = round($total*0.19/1.19,2);
        $taxReturnBase = round($total/1.19,2);
        $referenceCode = 'Test_BS_'.$compra->getId();
        $firma = md5($datos_payu->getApiKey().'~'.$datos_payu->getMerchantId().'~'.$referenceCode.'~'.$total.'~'.$datos_payu->getCurrency());
        //$responseUrl = $this->generateUrl('pagar_payu_respuesta',null,UrlGeneratorInterface::ABSOLUTE_URL);
        //$confirmationUrl = $this->generateUrl('pagar_payu_confirmacion',array(),UrlGeneratorInterface::ABSOLUTE_URL);

        $session->set('carrito', array());
        $session->set('bonos', array());
        $session->set('descuento', array());
        $session->set('descuento_porc', array());
        $session->set('tipo_pago', "Online");
        $session->set('direccion_id', null);
        if($total == 0){
            return $this->render('PagosPayuBundle:Default:gracias_compra.html.twig');
        }
        if($tipo_pago == "Online"){
            return $this->render('PagosPayuBundle:Default:index.html.twig',array('compra'=>$compra
            ,'tax'=>$tax
            ,'taxReturnBase'=>$taxReturnBase
            ,'firma'=>$firma
            ,'referenceCode'=>$referenceCode
            ,'descripcion_text'=>'Compra en Back To Basics'
            ,'datos_payu'=>$datos_payu
            ,'nombreCiudad'=>$nombreCiudad
            ));
        }else{
            return $this->render('PagosPayuBundle:Default:gracias_compra.html.twig');
        }

    }


    /**
     * @Route("/pagar-payu/respuesta",name="pagar_payu_respuesta")
     */
    public function respuestaAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $full_url = serialize($request->query->all());
        $logger = new PagoLogger();
        $logger->setRespuesta($full_url);
        $em->persist($logger);

        $datos_payu = $this->getDoctrine()->getRepository('PagosPayuBundle:DatosPayu')->find(1);
        $ApiKey = $datos_payu->getApiKey();
        $merchant_id = $datos_payu->getMerchantId();
        $referenceCode = $request->query->get('referenceCode');
        $TX_VALUE = $request->query->get('TX_VALUE');
        $New_value = number_format($TX_VALUE, 1, '.', '');
        $currency = $request->query->get('currency');
        $transactionState = $request->query->get('transactionState');

        $firma_cadena = "$ApiKey~$merchant_id~$referenceCode~$New_value~$currency~$transactionState";
        $firmacreada = md5($firma_cadena);
        $firma = $request->query->get('signature');

        $reference_pol = $request->query->get('reference_pol');
        $cus = $request->query->get('cus');
        $description = $request->query->get('description');
        $pseBank = $request->query->get('pseBank');
        $lapPaymentMethod = $request->query->get('lapPaymentMethod');
        $transactionId = $request->query->get('transactionId');
        $processingDate = $request->query->get('processingDate');
        $TX_ADMINISTRATIVE_FEE = $request->query->get('TX_ADMINISTRATIVE_FEE');

        $arr_ref = explode('_',$referenceCode);
        $id_compra = end($arr_ref);
        $compra = $this->getDoctrine()->getRepository('CarroiridianBundle:Compra')->find($id_compra);
        $respuesta = new RepuestaPago();
        $respuesta->setReferenceCode($referenceCode);
        $respuesta->setTXVALUE($TX_VALUE);
        $respuesta->setTransactionState($transactionState);
        $respuesta->setSignature($firma);
        $respuesta->setReferencePol($reference_pol);
        $respuesta->setCus($cus);
        $respuesta->setPseBank($pseBank);
        $respuesta->setLapPaymentMethod($lapPaymentMethod);
        $respuesta->setTransactionId($transactionId);
        $respuesta->setProcessingDate($processingDate);
        $respuesta->setTXADMINISTRATIVEFEE($TX_ADMINISTRATIVE_FEE);
        $respuesta->setDescription($description);
        $respuesta->setCompra($compra);
        $respuesta->setTipo('RESPUESTA');
        $em->persist($respuesta);
        $em->flush();



        if (strtoupper($firma) == strtoupper($firmacreada)) {

            if ($transactionState == 4 ) {
                $estadoTx = "APROBADA";
                $compra->setPago(true);
                $em->persist($compra);
                $em->flush();
            }

            else if ($transactionState == 6 ) {
                $estadoTx = "RECHZADA";
            }

            else if ($transactionState == 104 ) {
                $estadoTx = "PENDIENTE";
            }

            else if ($transactionState == 7 ) {
                $estadoTx = "ERROR";
            }

            else {
                $estadoTx=$_REQUEST['mensaje'];
            }

            $estado = $this->getDoctrine()->getRepository('CarroiridianBundle:EstadoCarrito')->findOneBy(array('ref'=>$estadoTx));
            $compra->setEstado($estado);
            $em->flush();

            return $this->render('PagosPayuBundle:Default:respuesta.html.twig',array(
                'estadoTx'=>$estadoTx,'transactionId'=>$transactionId,'reference_pol'=>$reference_pol,'referenceCode'=>$referenceCode,
                'pseBank'=>$pseBank,'cus'=>$cus,'TX_VALUE'=>$TX_VALUE,
                'currency'=>$currency,'lapPaymentMethod'=>$lapPaymentMethod));
        }else{
            return $this->render('HomeBundle:Default:index.html.twig');
        }
    }

    public function SendMail($subject, $from, $to, $custom, $template){

        $message =\Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom($from)
            ->setTo($to)
            ->setBody(
                $this->renderView(
                    $template,
                    $custom
                ),
                'text/html'
            );

        $this->get('mailer')->send($message);

    }

    /**
     * @Route("/pagar-payu/restar-inventario",name="restar_inventario", methods={"POST"})
     */
    public function restarInventarioAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $compra = $this->getDoctrine()->getRepository('CarroiridianBundle:Compra')->find($request->request->get('id_compra'));
        $compraitems = $compra->getCompraitems();
        $confirma_compra  = $this->getDoctrine()->getRepository('AppBundle:Mailing')->findBy(["llave" => "confirma_compra"]);
        $entrega_bono  = $this->getDoctrine()->getRepository('AppBundle:Mailing')->findBy(["llave" => "entrega_bono"]);
        $confirma_compra_bono  = $this->getDoctrine()->getRepository('AppBundle:Mailing')->findBy(["llave" => "confirma_compra_bono"]);
        $to = $compra->getEmail();
        $name = $compra->getUsuario();
        //$to = "jasson@iridian.co";
        if($request->request->get('email')){
            $to = $request->request->get('email');
        }
        foreach ($compraitems as $item){
            if(0){$item = new Compraitem();}

            $repo = $this->getDoctrine()->getRepository('CarroiridianBundle:Inventario');
            $inventario = $repo->findOneBy(array(
                'producto'=>$item->getProducto(),
                'talla'=>$item->getTalla()
            ));
            $salida = new Entrada();
            $salida->setCantidad(-1 * $item->getCantidad());
            $salida->setInventario($inventario);
            $salida->setFecha(new \DateTime());
            $em->persist($salida);
            $em->flush();

            $inventario->setCantidad($inventario->getCantidad() - $item->getCantidad());
            $em->persist($inventario);
            $em->flush();
            //public function SendMail($subject, $from, $to, $custom, $template)
        }
        $bonos  = $this->getDoctrine()->getRepository('CarroiridianBundle:Bono')->findBy(array('compra'=>$request->request->get('id_compra')));
        $admin  = $this->getDoctrine()->getRepository('AppBundle:Settings')->findBy(array('llave'=>"admin_mail"));
        $sender  = $this->getDoctrine()->getRepository('AppBundle:Settings')->findBy(array('llave'=>"sender_mail"));
        //exit(\Doctrine\Common\Util\Debug::dump());

        //$to = "jasson@iridian.co";
        $lc = $this->get('translator')->getLocale();
        if(count($bonos) > 0){
            $this->SendMail($confirma_compra_bono[0]->getAsunto()->getLocalName($lc), $sender[0]->getValor(), $to, array("name" => $name, "data" => $confirma_compra_bono[0]), "PagosPayuBundle:Default:confirma_compra_bono.html.twig");
            /*foreach ($bonos as $bono){
                $this->SendMail($entrega_bono[0]->getAsunto()->getLocalName($lc), $sender[0]->getValor(), $to, array("code" => $bonos[0]->getCodigo(), "from" => $bonos[0]->getDe(), "to" => $bonos[0]->getPara(), "data" => $entrega_bono[0]), "PagosPayuBundle:Default:entrega_bono.html.twig");
            }*/
        }
        $this->SendMail($confirma_compra[0]->getAsunto()->getLocalName($lc), $sender[0]->getValor(), $admin[0]->getValor(), array("summary" => $compraitems, "data" => $confirma_compra[0], "bonos" => $compra->getComprabonos()), "PagosPayuBundle:Default:confirma_compra.html.twig");
        $this->SendMail($confirma_compra[0]->getAsunto()->getLocalName($lc), $sender[0]->getValor(), $to, array("summary" => $compraitems, "data" => $confirma_compra[0], "bonos" => $compra->getComprabonos()), "PagosPayuBundle:Default:confirma_compra.html.twig");
        return new JsonResponse("1");
    }

    /**
     * @Route("/pagar-payu/forzar-email",name="pagar_payu_forzar", methods={"POST"})
     */
    public function forzarAction(Request $request)
    {
        //requiere
        //id_compra

        $compra = $this->getDoctrine()->getRepository('CarroiridianBundle:Compra')->find($request->request->get('id_compra'));
        $to = $compra->getEmail();
        //$to = "jasson@iridian.co";
        if($request->request->get('email')){
            $to = $request->request->get('email');
        }
        $name = $compra->getUsuario();
        $compraitems = $compra->getCompraitems();
        $confirma_compra  = $this->getDoctrine()->getRepository('AppBundle:Mailing')->findBy(["llave" => "confirma_compra"]);
        $entrega_bono  = $this->getDoctrine()->getRepository('AppBundle:Mailing')->findBy(["llave" => "entrega_bono"]);
        $confirma_compra_bono  = $this->getDoctrine()->getRepository('AppBundle:Mailing')->findBy(["llave" => "confirma_compra_bono"]);
        $bonos  = $this->getDoctrine()->getRepository('CarroiridianBundle:Bono')->findBy(array('compra'=>$request->request->get('id_compra')));
        $admin  = $this->getDoctrine()->getRepository('AppBundle:Settings')->findBy(array('llave'=>"admin_mail"));
        $sender  = $this->getDoctrine()->getRepository('AppBundle:Settings')->findBy(array('llave'=>"sender_mail"));
        //exit(\Doctrine\Common\Util\Debug::dump());

        //$to = "jasson@iridian.co";
        $lc = $this->get('translator')->getLocale();
        if(count($bonos) > 0){
            $this->SendMail($confirma_compra_bono[0]->getAsunto()->getLocalName($lc), $sender[0]->getValor(), $to, array("name" => $name, "data" => $confirma_compra_bono[0]), "PagosPayuBundle:Default:confirma_compra_bono.html.twig");
            /*foreach ($bonos as $bono){
                $this->SendMail($entrega_bono[0]->getAsunto()->getLocalName($lc), $sender[0]->getValor(), $to, array("code" => $bonos[0]->getCodigo(), "from" => $bonos[0]->getDe(), "to" => $bonos[0]->getPara(), "data" => $entrega_bono[0]), "PagosPayuBundle:Default:entrega_bono.html.twig");
            }*/
        }
        $this->SendMail($confirma_compra[0]->getAsunto()->getLocalName($lc), $sender[0]->getValor(), $admin[0]->getValor(), array("summary" => $compraitems, "data" => $confirma_compra[0], "bonos" => $compra->getComprabonos()), "PagosPayuBundle:Default:confirma_compra.html.twig");
        $this->SendMail($confirma_compra[0]->getAsunto()->getLocalName($lc), $sender[0]->getValor(), $to, array("summary" => $compraitems, "data" => $confirma_compra[0], "bonos" => $compra->getComprabonos()), "PagosPayuBundle:Default:confirma_compra.html.twig");
        return new JsonResponse("1");
    }


    /**
     * @Route("/pagar-payu/confirmacion",name="pagar_payu_confirmacion", methods={"POST"})
     */
    public function confirmacionAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $full_url = serialize($request->request->all());
        $logger = new PagoLogger();
        $logger->setRespuesta($full_url);
        $em->persist($logger);


        $datos_payu = $this->getDoctrine()->getRepository('PagosPayuBundle:DatosPayu')->find(1);
        $ApiKey = $datos_payu->getApiKey();
        $merchant_id = $datos_payu->getMerchantId();

        $reference_sale = $request->request->get('reference_sale');
        $value = $request->request->get('value');
        $new_value = number_format($value, 1, '.', '');
        $currency = $request->request->get('currency');
        $state_pol = $request->request->get('state_pol');
        $firma_cadena = "$ApiKey~$merchant_id~$reference_sale~$new_value~$currency~$state_pol";

        $firmacreada = md5($firma_cadena);
        $firma = $request->request->get('sign');
        $reference_pol = $request->query->get('reference_pol');
        $cus = $request->request->get('cus');
        $description = $request->query->get('description');
        $pseBank = $request->request->get('pse_bank');
        $lapPaymentMethod = $request->query->get('payment_method_name');
        $transactionId = $request->request->get('transaction_id');
        $processingDate = $request->request->get('transaction_date');
        $TX_ADMINISTRATIVE_FEE = $request->request->get('administrative_fee');
        $risk = $request->request->get('risk');

        $confirma_compra  = $this->getDoctrine()->getRepository('AppBundle:Mailing')->findBy(["llave" => "confirma_compra"]);
        $entrega_bono  = $this->getDoctrine()->getRepository('AppBundle:Mailing')->findBy(["llave" => "entrega_bono"]);
        $confirma_compra_bono  = $this->getDoctrine()->getRepository('AppBundle:Mailing')->findBy(["llave" => "confirma_compra_bono"]);


        $arr_ref = explode('_',$reference_sale);
        $id_compra = end($arr_ref);
        $compra = $this->getDoctrine()->getRepository('CarroiridianBundle:Compra')->find($id_compra);
        $compra->getId();
        $respuesta = new RepuestaPago();
        $respuesta->setReferenceCode($reference_sale);
        $respuesta->setTXVALUE($value);
        $respuesta->setTransactionState($state_pol);
        $respuesta->setSignature($firma);
        $respuesta->setReferencePol($reference_pol);
        $respuesta->setCus($cus);
        $respuesta->setPseBank($pseBank);
        $respuesta->setLapPaymentMethod($lapPaymentMethod);
        $respuesta->setTransactionId($transactionId);
        $respuesta->setProcessingDate($processingDate);
        $respuesta->setTXADMINISTRATIVEFEE($TX_ADMINISTRATIVE_FEE);
        $respuesta->setDescription($description);
        $respuesta->setCompra($compra);
        $respuesta->setRisk($risk);
        $respuesta->setTipo('CONFIRMACION');
        $em->persist($respuesta);
        $em->flush();

        if($state_pol == 4){
            $compraitems = $compra->getCompraitems();
            $to = $compra->getEmail();
            $name = $compra->getUsuario();
            foreach ($compraitems as $item){
                $repo = $this->getDoctrine()->getRepository('CarroiridianBundle:Inventario');
                $inventario = $repo->findOneBy(array(
                    'producto'=>$item->getProducto(),
                    'talla'=>$item->getTalla()
                ));
                $salida = new Entrada();
                $salida->setCantidad(-1 * $item->getCantidad());
                $salida->setInventario($inventario);
                $salida->setFecha(new \DateTime());
                $em->persist($salida);
                $em->flush();

                //$inventario = new Inventario();

                $inventario->setCantidad($inventario->getCantidad() - $item->getCantidad());
                $em->persist($inventario);
                $em->flush();
                //public function SendMail($subject, $from, $to, $custom, $template)
            }

            $bonos  = $this->getDoctrine()->getRepository('CarroiridianBundle:Bono')->findBy(array('compra'=>$id_compra));
            $admin  = $this->getDoctrine()->getRepository('AppBundle:Settings')->findBy(array('llave'=>"admin_mail"));
            $sender  = $this->getDoctrine()->getRepository('AppBundle:Settings')->findBy(array('llave'=>"sender_mail"));

            $lc = $this->get('translator')->getLocale();
            if(count($bonos) > 0){
                $this->SendMail($confirma_compra_bono[0]->getAsunto()->getLocalName($lc), $sender[0]->getValor(), $to, array("name" => $name, "data" => $confirma_compra_bono[0]), "PagosPayuBundle:Default:confirma_compra_bono.html.twig");
            }
            $this->SendMail($confirma_compra[0]->getAsunto()->getLocalName($lc), $sender[0]->getValor(), $admin[0]->getValor(), array("summary" => $compraitems, "data" => $confirma_compra[0], "bonos" => $compra->getComprabonos()), "PagosPayuBundle:Default:confirma_compra.html.twig");
            $this->SendMail($confirma_compra[0]->getAsunto()->getLocalName($lc), $sender[0]->getValor(), $to, array("summary" => $compraitems, "data" => $confirma_compra[0], "bonos" => $compra->getComprabonos()), "PagosPayuBundle:Default:confirma_compra.html.twig");

        }

        $em->flush();

       if (strtoupper($firma) == strtoupper($firmacreada)) {
            if ($state_pol == 4 ) {
                $estadoTx = "APROBADA";
                $compra->setPago(true);

            }

            else if ($state_pol == 6 ) {
                $estadoTx = "RECHZADA";
            }

            else if ($state_pol == 5 ) {
                $estadoTx = "EXPIRADA";
            }
            else {
                $estadoTx = "PENDIENTE";
            }
            $estado = $this->getDoctrine()->getRepository('CarroiridianBundle:EstadoCarrito')->findOneBy(array('ref'=>$estadoTx));
            $compra->setEstado($estado);
            $em->persist($compra);
            $em->flush();



            return new Response('Hello PAYU', Response::HTTP_OK);
        }else{
            return new Response('Hello PAYU', Response::HTTP_INTERNAL_SERVER_ERROR);
        }

    }


    /**
     * @Route("/webhook/payu", name="web_hook_payu")
     */
    public function index(Request $request)
    {
        $em=$this->getDoctrine()->getManager();
        $data = $request->request->all();
        $log = new PayuLog();
        $log->setData(json_encode($data));
        $em->persist($log);
        $em->flush();
        $reference_sale = $data['reference_sale'];
        $reference_recurring_payment = $data['reference_recurring_payment'];
        $susId = explode('_',$reference_recurring_payment)[0];
        $suscripcion = $this->getDoctrine()->getRepository("CarroiridianBundle:Suscripcion")->findOneBy(array('idPayu'=>$susId) );
        $to=$suscripcion->getUsuario()->getEmail();
        $state_pol = $data['state_pol'];
        $ex = explode('-',$reference_sale);
        $pagoId=[];
        for($i=0;$i< count($ex)-1;$i++ ){
            array_push($pagoId,$ex[$i]);
        }
        $pagoId= join('-',$pagoId);
        $pago = new PagoSuscripcion();
        $pago->setIdPayu($data['transaction_id']);
        $pago->setOrderId($data['reference_sale']);
        $pago->setMonto($data['value']);
        $pago->setMoneda($data['currency']);
        $pago->setCus($data['cus']);
        $pago->setReferencePol($data['reference_pol']);
        $fecha = new \DateTime($data['operation_date']);
        $pago->setFecha($fecha);
        $pago->setState($data['response_message_pol']);
        $pago->setSuscripcion($suscripcion);

        $em->persist($pago);
        $em->flush();
       // $this->qi->saveFire($pago);
        $activa = $state_pol == 4;
        if($activa){
            $lc = $this->get('translator')->getLocale();
            $confirma_compra  = $this->getDoctrine()->getRepository('AppBundle:Mailing')->findBy(["llave" => "confirma_compra_box"]);
            $admin  = $this->getDoctrine()->getRepository('AppBundle:Settings')->findBy(array('llave'=>"admin_mail"));
            $sender  = $this->getDoctrine()->getRepository('AppBundle:Settings')->findBy(array('llave'=>"sender_mail"));
            $valida = new \DateTime();
            $valida = new \DateTime(date('Y-m-d', strtotime("+2 days",strtotime("+".$suscripcion->getDuracion()." months", $valida->getTimestamp()) ) ) );
            //dd($valida);
            $suscripcion->setValidaHasta($valida);
            $suscripcion->setActiva(1);
            $em->persist($suscripcion);
            $em->flush();
            $this->SendMail($confirma_compra[0]->getAsunto()->getLocalName($lc), $sender[0]->getValor(), $admin[0]->getValor(), array("data" => $confirma_compra[0], "suscripcion" => $suscripcion), "PagosPayuBundle:Default:confirma_compra_box.html.twig.html.twig");
            $this->SendMail($confirma_compra[0]->getAsunto()->getLocalName($lc), $sender[0]->getValor(), $to, array("data" => $confirma_compra[0], "suscripcion" => $suscripcion), "PagosPayuBundle:Default:confirma_compra_box.html.twig.html.twig");
        }


        return new Response('Hello Payu', Response::HTTP_OK);
    }


}

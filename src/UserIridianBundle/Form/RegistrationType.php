<?php

namespace UserIridianBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nombre', TextType::class, array('required' => true));
        $builder->add('apellidos', TextType::class, array('required' => true));
        $builder->add('telefono', TextType::class, array('required' => true));
        $builder->add('fecha', BirthdayType::class, array(
            'required' => true,
            'widget' => 'choice',
            // this is actually the default format for single_text
            'format' => 'yyyy-MM-dd',
        ));
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';

        // Or for Symfony < 2.8
        // return 'fos_user_registration';
    }

    public function getBlockPrefix()
    {
        return 'app_user_registration';
    }

    // For Symfony 2.x
    public function getNombre()
    {
        return $this->getBlockPrefix();
    }
}
<?php

namespace UserIridianBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/useriridian")
     */
    public function indexAction()
    {
        return $this->render('UserIridianBundle:Default:index.html.twig');
    }
}

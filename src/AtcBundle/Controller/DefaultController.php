<?php

namespace AtcBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/atc", name="atc")
     */
    public function indexAction()
    {
        return $this->render('AtcBundle:Default:atc.html.twig');
    }
}
